/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:30
 */

package fr.nevax.grimmo.property.properties;


import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;
import io.reactivex.Flowable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link PropertiesPresenter}
 */
public class PropertiesPresenterTest {

    private static List<Property> PROPERTIES;

    @Mock
    private PropertiesRepository mPropertiesRepository;

    @Mock
    private PropertiesContract.View mPropertiesView;

    private BaseSchedulerProvider mSchedulerProvider;

    private PropertiesPresenter mPropertiesPresenter;

    @Before
    public void setupPropertiesPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Make the sure that all schedulers are immediate.
        mSchedulerProvider = new ImmediateSchedulerProvider();

        // Get a reference to the class under test
        mPropertiesPresenter = new PropertiesPresenter(mPropertiesRepository, mPropertiesView, mSchedulerProvider);

        // The presenter won't update the view unless it's active.
        when(mPropertiesView.isActive()).thenReturn(true);

        // We subscribe the properties to 3, with one active and two completed
        PROPERTIES = Lists.newArrayList(new Property("Title1", "Description1", 10f),
                new Property("Title2", "Description2", 10f, true), new Property("Title3", "Description3", 10f, true));
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mPropertiesPresenter = new PropertiesPresenter(mPropertiesRepository, mPropertiesView, mSchedulerProvider);

        // Then the presenter is set to the view
        verify(mPropertiesView).setPresenter(mPropertiesPresenter);
    }

    @Test
    public void loadAllPropertiesFromRepositoryAndLoadIntoView() {
        // Given an initialized PropertiesPresenter with initialized properties
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.just(PROPERTIES));
        // When loading of Properties is requested
        mPropertiesPresenter.setFiltering(PropertiesFilterType.ALL_PROPERTIES);
        mPropertiesPresenter.loadProperties(true);

        // Then progress indicator is shown
        verify(mPropertiesView).setLoadingIndicator(true);
        // Then progress indicator is hidden and all properties are shown in UI
        verify(mPropertiesView).setLoadingIndicator(false);
    }

    @Test
    public void loadActivePropertiesFromRepositoryAndLoadIntoView() {
        // Given an initialized PropertiesPresenter with initialized properties
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.just(PROPERTIES));
        // When loading of Properties is requested
        mPropertiesPresenter.setFiltering(PropertiesFilterType.ACTIVE_PROPERTIES);
        mPropertiesPresenter.loadProperties(true);

        // Then progress indicator is hidden and active properties are shown in UI
        verify(mPropertiesView).setLoadingIndicator(false);
    }

    @Test
    public void loadCompletedPropertiesFromRepositoryAndLoadIntoView() {
        // Given an initialized PropertiesPresenter with initialized properties
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.just(PROPERTIES));
        // When loading of Properties is requested
        mPropertiesPresenter.setFiltering(PropertiesFilterType.COMPLETED_PROPERTIES);
        mPropertiesPresenter.loadProperties(true);

        // Then progress indicator is hidden and completed properties are shown in UI
        verify(mPropertiesView).setLoadingIndicator(false);
    }

    @Test
    public void clickOnFab_ShowsAddPropertyUi() {
        // When adding a new property
        mPropertiesPresenter.addNewProperty();

        // Then add property UI is shown
        verify(mPropertiesView).showAddProperty();
    }

    @Test
    public void clickOnProperty_ShowsDetailUi() {
        // Given a stubbed active property
        Property requestedProperty = new Property("Details Requested", "For this property", 10f);

        // When open property details is requested
        mPropertiesPresenter.openPropertyDetails(requestedProperty);

        // Then property detail UI is shown
        verify(mPropertiesView).showPropertyDetailsUi(any(String.class));
    }

    @Test
    public void completeProperty_ShowsPropertyMarkedComplete() {
        // Given a stubbed property
        Property property = new Property("Details Requested", "For this property", 10f);
        // And no properties available in the repository
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.empty());

        // When property is marked as complete
        mPropertiesPresenter.completeProperty(property);

        // Then repository is called and property marked complete UI is shown
        verify(mPropertiesRepository).completeProperty(property);
        verify(mPropertiesView).showPropertyMarkedComplete();
    }

    @Test
    public void activateProperty_ShowsPropertyMarkedActive() {
        // Given a stubbed completed property
        Property property = new Property("Details Requested", "For this property", 10f, true);
        // And no properties available in the repository
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.empty());
        mPropertiesPresenter.loadProperties(true);

        // When property is marked as activated
        mPropertiesPresenter.activateProperty(property);

        // Then repository is called and property marked active UI is shown
        verify(mPropertiesRepository).activateProperty(property);
        verify(mPropertiesView).showPropertyMarkedActive();
    }

    @Test
    public void errorLoadingProperties_ShowsError() {
        // Given that no properties are available in the repository
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.error(new Exception()));

        // When properties are loaded
        mPropertiesPresenter.setFiltering(PropertiesFilterType.ALL_PROPERTIES);
        mPropertiesPresenter.loadProperties(true);

        // Then an error message is shown
        verify(mPropertiesView).showLoadingPropertiesError();
    }
}