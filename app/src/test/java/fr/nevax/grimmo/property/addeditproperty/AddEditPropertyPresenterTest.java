package fr.nevax.grimmo.property.addeditproperty;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.PropertyType;
import fr.nevax.grimmo.data.source.PicturesRepository;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.data.source.RoomsRepository;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionContract;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionPresenter;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapContract;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPictureContract;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomContract;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit tests for the implementation of {@link AddEditPropertyDescriptionPresenter}.
 */
public class AddEditPropertyPresenterTest {

    @Mock
    private PropertiesRepository mPropertiesRepository;

    @Mock
    private RoomsRepository mRoomsRepository;

    @Mock
    private PicturesRepository mPicturesRepository;

    @Mock
    private AddEditPropertyContract.View mAddEditPropertyView;

    @Mock
    private AddEditPropertyDescriptionContract.View mAddEditPropertyDescriptionView;

    @Mock
    private AddEditPropertyRoomContract.View mAddEditPropertyRoomView;

    @Mock
    private AddEditPropertyMapContract.View mAddEditPropertyMapView;

    @Mock
    private AddEditPropertyPictureContract.View mAddEditPropertyPictureView;

    private BaseSchedulerProvider mSchedulerProvider;

    private AddEditPropertyContract.Presenter mAddEditPropertyPresenter;
    private AddEditPropertyDescriptionContract.Presenter mAddEditPropertyDescriptionPresenter;
    private AddEditPropertyRoomContract.Presenter mAddEditPropertyRoomPresenter;
    private AddEditPropertyMapContract.Presenter mAddEditPropertyMapPresenter;
    private AddEditPropertyPictureContract.Presenter mAddEditPropertyPicturePresenter;

    @Before
    public void setupMocksAndView() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        mSchedulerProvider = new ImmediateSchedulerProvider();

        // The presenter wont't update the view unless it's active.
        when(mAddEditPropertyView.isActive()).thenReturn(true);
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mAddEditPropertyPresenter = new AddEditPropertyPresenter(null,
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);
        // Then the presenter is set to the view
        verify(mAddEditPropertyView).setPresenter(mAddEditPropertyPresenter);
    }

    @Test
    public void saveNewPropertyToRepository_showsSuccessMessageUi() {
        // Get a reference to the class under test
        mAddEditPropertyPresenter = new AddEditPropertyPresenter(null,
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // When the presenter is asked to save a property
        mAddEditPropertyPresenter.saveProperty("New Property Title", "Some Property Description", "10", "10", PropertyType.APARTMENT.ordinal());

        // Then a property is saved in the repository and the view updated
        verify(mPropertiesRepository).saveProperty(any(Property.class)); // saved to the model
        verify(mAddEditPropertyView).showPropertiesList(); // shown in the UI
    }

    @Test
    public void saveProperty_emptyPropertyShowsErrorUi() {
        // Get a reference to the class under test
        mAddEditPropertyPresenter = new AddEditPropertyPresenter(null,
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // When the presenter is asked to save an empty property
        mAddEditPropertyPresenter.saveProperty("", "", "", "", PropertyType.APARTMENT.ordinal());

        // Then an empty not error is shown in the UI
        verify(mAddEditPropertyView).showEmptyPropertyError();
    }

    @Test
    public void saveExistingPropertyToRepository_showsSuccessMessageUi() {
        // Get a reference to the class under test
        mAddEditPropertyPresenter = new AddEditPropertyPresenter(null,
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // When the presenter is asked to save an existing property
        mAddEditPropertyPresenter.saveProperty("Existing Property Title", "Some Property Description", "10", "10", PropertyType.APARTMENT.ordinal());

        // Then a property is saved in the repository and the view updated
        verify(mPropertiesRepository).saveProperty(any(Property.class)); // saved to the model
        verify(mAddEditPropertyView).showPropertiesList(); // shown in the UI
    }
}