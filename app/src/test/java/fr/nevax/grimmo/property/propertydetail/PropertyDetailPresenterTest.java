/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:30
 */

package fr.nevax.grimmo.property.propertydetail;


import com.google.common.base.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;
import io.reactivex.Flowable;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link PropertyDetailPresenter}
 */
public class PropertyDetailPresenterTest {

    public static final String TITLE_TEST = "title";

    public static final String DESCRIPTION_TEST = "description";

    public static final String INVALID_PROPERTY_ID = "";

    public static final Property ACTIVE_PROPERTY = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f);

    public static final Property COMPLETED_PROPERTY = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f, true);

    @Mock
    private PropertiesRepository mPropertiesRepository;

    @Mock
    private PropertyDetailContract.View mPropertyDetailView;

    private BaseSchedulerProvider mSchedulerProvider;

    private PropertyDetailPresenter mPropertyDetailPresenter;

    @Before
    public void setup() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Make the sure that all schedulers are immediate.
        mSchedulerProvider = new ImmediateSchedulerProvider();

        // The presenter won't update the view unless it's active.
        when(mPropertyDetailView.isActive()).thenReturn(true);
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                ACTIVE_PROPERTY.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);

        // Then the presenter is set to the view
        verify(mPropertyDetailView).setPresenter(mPropertyDetailPresenter);
    }

    @Test
    public void getActivePropertyFromRepositoryAndLoadIntoView() {
        // When properties presenter is asked to open a property
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                ACTIVE_PROPERTY.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        setPropertyAvailable(ACTIVE_PROPERTY);
        mPropertyDetailPresenter.subscribe();

        // Then property is loaded from model, callback is captured and progress indicator is shown
        verify(mPropertiesRepository).getProperty(eq(ACTIVE_PROPERTY.getId()));
        verify(mPropertyDetailView).setLoadingIndicator(true);

        // Then progress indicator is hidden and title, description and completion status are shown
        // in UI
        verify(mPropertyDetailView).setLoadingIndicator(false);
        verify(mPropertyDetailView).showTitle(TITLE_TEST);
        verify(mPropertyDetailView).showDescription(DESCRIPTION_TEST);
        verify(mPropertyDetailView).showCompletionStatus(false);
    }

    @Test
    public void getCompletedPropertyFromRepositoryAndLoadIntoView() {
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                COMPLETED_PROPERTY.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        setPropertyAvailable(COMPLETED_PROPERTY);
        mPropertyDetailPresenter.subscribe();

        // Then property is loaded from model, callback is captured and progress indicator is shown
        verify(mPropertiesRepository).getProperty(
                eq(COMPLETED_PROPERTY.getId()));
        verify(mPropertyDetailView).setLoadingIndicator(true);

        // Then progress indicator is hidden and title, description and completion status are shown
        // in UI
        verify(mPropertyDetailView).setLoadingIndicator(false);
        verify(mPropertyDetailView).showTitle(TITLE_TEST);
        verify(mPropertyDetailView).showDescription(DESCRIPTION_TEST);
        verify(mPropertyDetailView).showCompletionStatus(true);
    }

    @Test
    public void getUnknownPropertyFromRepositoryAndLoadIntoView() {
        // When loading of a property is requested with an invalid property ID.
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                INVALID_PROPERTY_ID, mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        mPropertyDetailPresenter.subscribe();
        verify(mPropertyDetailView).showMissingProperty();
    }

    @Test
    public void deleteProperty() {
        // Given an initialized PropertyDetailPresenter with stubbed property
        Property property = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f);

        // When the deletion of a property is requested
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                property.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        mPropertyDetailPresenter.deleteProperty();

        // Then the repository and the view are notified
        verify(mPropertiesRepository).deleteProperty(property.getId());
        verify(mPropertyDetailView).showPropertyDeleted();
    }

    @Test
    public void completeProperty() {
        // Given an initialized presenter with an active property
        Property property = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f);
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                property.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        setPropertyAvailable(property);
        mPropertyDetailPresenter.subscribe();

        // When the presenter is asked to complete the property
        mPropertyDetailPresenter.completeProperty();

        // Then a request is sent to the property repository and the UI is updated
        verify(mPropertiesRepository).completeProperty(property.getId());
        verify(mPropertyDetailView).showPropertyMarkedComplete();
    }

    @Test
    public void activateProperty() {
        // Given an initialized presenter with a completed property
        Property property = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f, true);
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                property.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        setPropertyAvailable(property);
        mPropertyDetailPresenter.subscribe();

        // When the presenter is asked to activate the property
        mPropertyDetailPresenter.activateProperty();

        // Then a request is sent to the property repository and the UI is updated
        verify(mPropertiesRepository).activateProperty(property.getId());
        verify(mPropertyDetailView).showPropertyMarkedActive();
    }

    @Test
    public void activePropertyIsShownWhenEditing() {
        // When the edit of an ACTIVE_PROPERTY is requested
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                ACTIVE_PROPERTY.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        mPropertyDetailPresenter.editProperty();

        // Then the view is notified
        verify(mPropertyDetailView).showEditProperty(ACTIVE_PROPERTY.getId());
    }

    @Test
    public void invalidPropertyIsNotShownWhenEditing() {
        // When the edit of an invalid property id is requested
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                INVALID_PROPERTY_ID, mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);
        mPropertyDetailPresenter.editProperty();

        // Then the edit mode is never started
        verify(mPropertyDetailView, never()).showEditProperty(INVALID_PROPERTY_ID);
        // instead, the error is shown.
        verify(mPropertyDetailView).showMissingProperty();
    }

    @Test
    public void invalidMapCoordinate() {
        // Given an initialized presenter without coordinate
        Property propertyWithoutCoordinate = new Property(TITLE_TEST, DESCRIPTION_TEST, 10f);
        mPropertyDetailPresenter = new PropertyDetailPresenter(
                propertyWithoutCoordinate.getId(), mPropertiesRepository, mPropertyDetailView, mSchedulerProvider);

        // When the presenter is asked to open show the property on map
        mPropertyDetailPresenter.showPropertyMap(propertyWithoutCoordinate);

        // Then a error is shown on the UI
        verify(mPropertyDetailView).showPropertyOnMapError();
    }

    private void setPropertyAvailable(Property property) {
        when(mPropertiesRepository.getProperty(eq(property.getId()))).thenReturn(Flowable.just(Optional.of(property)));
    }

}
