/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:30
 */

package fr.nevax.grimmo.property.propertystatistics;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;
import io.reactivex.Flowable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link PropertyStatisticsPresenter}
 */
public class PropertyStatisticsPresenterTest {

    private static List<Property> PROPERTIES;

    @Mock
    private PropertiesRepository mPropertiesRepository;

    @Mock
    private PropertyStatisticsContract.View mStatisticsView;

    private BaseSchedulerProvider mSchedulerProvider;

    private PropertyStatisticsPresenter mPropertyStatisticsPresenter;

    @Before
    public void setupPropertyStatisticsPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Make the sure that all schedulers are immediate.
        mSchedulerProvider = new ImmediateSchedulerProvider();

        // Get a reference to the class under test
        mPropertyStatisticsPresenter = new PropertyStatisticsPresenter(mPropertiesRepository, mStatisticsView,
                mSchedulerProvider);

        // The presenter won't update the view unless it's active.
        when(mStatisticsView.isActive()).thenReturn(true);

        // We subscribe the properties to 3, with one active and two completed
        PROPERTIES = Lists.newArrayList(
                new Property("Title1", "Description1", 10f),
                new Property("Title2", "Description2", 10f, true),
                new Property("Title3", "Description3", 10f, true)
        );
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mPropertyStatisticsPresenter = new PropertyStatisticsPresenter(mPropertiesRepository, mStatisticsView,
                mSchedulerProvider);

        // Then the presenter is set to the view
        verify(mStatisticsView).setPresenter(mPropertyStatisticsPresenter);
    }

    @Test
    public void loadEmptyPropertiesFromRepository_CallViewToDisplay() {
        // Given an initialized PropertyStatisticsPresenter with no properties
        PROPERTIES.clear();
        setPropertiesAvailable(PROPERTIES);

        // When loading of Properties is requested
        mPropertyStatisticsPresenter.subscribe();

        //Then progress indicator is shown
        verify(mStatisticsView).setProgressIndicator(true);

        // Callback is captured and invoked with stubbed properties
        verify(mPropertiesRepository).getProperties();

        // Then progress indicator is hidden and correct data is passed on to the view
        verify(mStatisticsView).setProgressIndicator(false);
        verify(mStatisticsView).showStatistics(0, 0);
    }

    @Test
    public void loadNonEmptyPropertiesFromRepository_CallViewToDisplay() {
        // Given an initialized PropertyStatisticsPresenter with 1 active and 2 completed properties
        setPropertiesAvailable(PROPERTIES);

        // When loading of Properties is requested
        mPropertyStatisticsPresenter.subscribe();

        //Then progress indicator is shown
        verify(mStatisticsView).setProgressIndicator(true);

        // Then progress indicator is hidden and correct data is passed on to the view
        verify(mStatisticsView).setProgressIndicator(false);
        verify(mStatisticsView).showStatistics(1, 2);
    }

    @Test
    public void loadStatisticsWhenPropertiesAreUnavailable_CallErrorToDisplay() {
        // Given that properties data isn't available
        setPropertiesNotAvailable();

        // When statistics are loaded
        mPropertyStatisticsPresenter.subscribe();

        // Then an error message is shown
        verify(mStatisticsView).showLoadingStatisticsError();
    }

    private void setPropertiesAvailable(List<Property> properties) {
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.just(properties));
    }

    private void setPropertiesNotAvailable() {
        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.error(new Exception()));
    }
}