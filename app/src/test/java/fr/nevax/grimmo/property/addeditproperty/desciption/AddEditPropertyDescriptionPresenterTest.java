/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:30
 */

package fr.nevax.grimmo.property.addeditproperty.desciption;

import com.google.common.base.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.PropertyType;
import fr.nevax.grimmo.data.source.PicturesRepository;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.data.source.RoomsRepository;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyContract;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyPresenter;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionContract;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionPresenter;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapContract;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPictureContract;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomContract;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;
import io.reactivex.Flowable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link AddEditPropertyDescriptionPresenter}.
 */
public class AddEditPropertyDescriptionPresenterTest {

    @Mock
    private PropertiesRepository mPropertiesRepository;

    @Mock
    private RoomsRepository mRoomsRepository;

    @Mock
    private PicturesRepository mPicturesRepository;

    @Mock
    private AddEditPropertyContract.View mAddEditPropertyView;

    @Mock
    private AddEditPropertyDescriptionContract.View mAddEditPropertyDescriptionView;

    @Mock
    private AddEditPropertyRoomContract.View mAddEditPropertyRoomView;

    @Mock
    private AddEditPropertyMapContract.View mAddEditPropertyMapView;

    @Mock
    private AddEditPropertyPictureContract.View mAddEditPropertyPictureView;

    private BaseSchedulerProvider mSchedulerProvider;

    private AddEditPropertyContract.Presenter mAddEditPropertyPresenter;
    private AddEditPropertyDescriptionContract.Presenter mAddEditPropertyDescriptionPresenter;
    private AddEditPropertyRoomContract.Presenter mAddEditPropertyRoomPresenter;
    private AddEditPropertyMapContract.Presenter mAddEditPropertyMapPresenter;
    private AddEditPropertyPictureContract.Presenter mAddEditPropertyPicturePresenter;

    @Before
    public void setupMocksAndView() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        mSchedulerProvider = new ImmediateSchedulerProvider();

        // The presenter wont't update the view unless it's active.
        when(mAddEditPropertyView.isActive()).thenReturn(true);

        mAddEditPropertyPresenter = new AddEditPropertyPresenter(
                null,
                mPropertiesRepository,
                mRoomsRepository,
                mPicturesRepository,
                mAddEditPropertyView,
                true,
                mSchedulerProvider);
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(
                null, mAddEditPropertyDescriptionView,
                true, mAddEditPropertyPresenter);

        // Then the presenter is set to the view
        verify(mAddEditPropertyDescriptionView).setPresenter(mAddEditPropertyDescriptionPresenter);
    }

    @Test
    public void saveNewPropertyToRepository_showsSuccessMessageUi() {
        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(
                null, mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to save a property
        mAddEditPropertyPresenter.saveProperty("New Property Title", "Some Property Description", "10", "10", PropertyType.APARTMENT.ordinal());

        // Then a property is saved in the repository and the view updated
        verify(mPropertiesRepository).saveProperty(any(Property.class)); // saved to the model
        verify(mAddEditPropertyView).showPropertiesList(); // shown in the UI
    }

    @Test
    public void saveProperty_emptyPropertyShowsErrorUi() {
        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(
                null, mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to save an empty property
        mAddEditPropertyPresenter.saveProperty("", "", "", "", PropertyType.APARTMENT.ordinal());

        // Then an empty not error is shown in the UI
        verify(mAddEditPropertyView).showEmptyPropertyError();
    }

    @Test
    public void saveExistingPropertyToRepository_showsSuccessMessageUi() {
        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(
                "1", mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to save an existing property
        mAddEditPropertyPresenter.saveProperty("Existing Property Title", "Some Property Description", "10", "10", PropertyType.APARTMENT.ordinal());

        // Then a property is saved in the repository and the view updated
        verify(mPropertiesRepository).saveProperty(any(Property.class)); // saved to the model
        verify(mAddEditPropertyView).showPropertiesList(); // shown in the UI
    }

    @Test
    public void populateProperty_callsRepoAndUpdatesViewOnSuccess() {
        Property testProperty = new Property("TITLE", "DESCRIPTION", 10f);
        Optional<Property> propertyOptional = Optional.of(testProperty);
        when(mPropertiesRepository.getProperty(testProperty.getId())).thenReturn(Flowable.just(propertyOptional));

        mAddEditPropertyPresenter = new AddEditPropertyPresenter(testProperty.getId(),
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(testProperty.getId(),
                mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to populate an existing property
        mAddEditPropertyPresenter.populateProperty();

        // Then the property repository is queried and the view updated
        verify(mPropertiesRepository).getProperty(eq(testProperty.getId()));

        verify(mAddEditPropertyDescriptionView).setTitle(testProperty.getTitle());
        verify(mAddEditPropertyDescriptionView).setDescription(testProperty.getDescription());
        verify(mAddEditPropertyDescriptionView).setPrice(testProperty.getPrice());
//        assertThat(mAddEditPropertyDescriptionPresenter.isDataMissing(), is(false));
    }

    @Test
    public void populateProperty_callsRepoAndUpdatesViewOnAbsentProperty() {
        Property testProperty = new Property("TITLE", "DESCRIPTION", 10f);
        when(mPropertiesRepository.getProperty(testProperty.getId())).thenReturn(Flowable.just(Optional.absent()));

        mAddEditPropertyPresenter = new AddEditPropertyPresenter(testProperty.getId(),
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(testProperty.getId(),
                mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to populate an existing property
        mAddEditPropertyPresenter.populateProperty();

        // Then the property repository is queried and the view updated
        verify(mPropertiesRepository).getProperty(eq(testProperty.getId()));

        verify(mAddEditPropertyView).showGetPropertyError();
        verify(mAddEditPropertyDescriptionView, never()).setTitle(testProperty.getTitle());
        verify(mAddEditPropertyDescriptionView, never()).setDescription(testProperty.getDescription());
    }

    @Test
    public void populateProperty_callsRepoAndUpdatesViewOnError() {
        Property testProperty = new Property("TITLE", "DESCRIPTION", 10f);
        when(mPropertiesRepository.getProperty(testProperty.getId())).thenReturn(Flowable.error(new Throwable("Some error")));

        mAddEditPropertyPresenter = new AddEditPropertyPresenter(testProperty.getId(),
                mPropertiesRepository, mRoomsRepository, mPicturesRepository, mAddEditPropertyView, true, mSchedulerProvider);

        // Get a reference to the class under test
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(testProperty.getId(),
                mAddEditPropertyDescriptionView, true, mAddEditPropertyPresenter);

        // When the presenter is asked to populate an existing property
        mAddEditPropertyPresenter.populateProperty();

        // Then the property repository is queried and the view updated
        verify(mPropertiesRepository).getProperty(eq(testProperty.getId()));

        verify(mAddEditPropertyView).showGetPropertyError();
        verify(mAddEditPropertyDescriptionView, never()).setTitle(testProperty.getTitle());
        verify(mAddEditPropertyDescriptionView, never()).setDescription(testProperty.getDescription());
    }
}