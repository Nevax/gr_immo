/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.content.Context;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import fr.nevax.grimmo.data.Property;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
public class PropertiesRepositoryTest {

    private final static String PROPERTY_TITLE = "title";

    private final static String PROPERTY_TITLE2 = "title2";

    private final static String PROPERTY_TITLE3 = "title3";

    private static List<Property> PROPERTIES = Lists.newArrayList(
            new Property("Title1", "Description1", 10f),
            new Property("Title2", "Description2", 10f)
    );

    private PropertiesRepository mPropertiesRepository;

    private TestSubscriber<List<Property>> mPropertiesTestSubscriber;

    @Mock
    private PropertiesDataSource mPropertiesRemoteDataSource;

    @Mock
    private PropertiesDataSource mPropertiesLocalDataSource;

    @Mock
    private Context mContext;


    @Before
    public void setupPropertiesRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mPropertiesRepository = PropertiesRepository.getInstance(
                mPropertiesRemoteDataSource, mPropertiesLocalDataSource);

        mPropertiesTestSubscriber = new TestSubscriber<>();
    }

    @After
    public void destroyRepositoryInstance() {
        PropertiesRepository.destroyInstance();
    }

    @Test
    public void getProperties_repositoryCachesAfterFirstSubscription_whenPropertiesAvailableInLocalStorage() {
        // Given that the local data source has data available
        setPropertiesAvailable(mPropertiesLocalDataSource, PROPERTIES);
        // And the remote data source does not have any data available
        setPropertiesNotAvailable(mPropertiesRemoteDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Property>> testSubscriber1 = new TestSubscriber<>();
        mPropertiesRepository.getProperties().subscribe(testSubscriber1);

        TestSubscriber<List<Property>> testSubscriber2 = new TestSubscriber<>();
        mPropertiesRepository.getProperties().subscribe(testSubscriber2);

        // Then properties were only requested once from remote and local sources
        verify(mPropertiesRemoteDataSource).getProperties();
        verify(mPropertiesLocalDataSource).getProperties();
        //
        assertFalse(mPropertiesRepository.mCacheIsDirty);
        testSubscriber1.assertValue(PROPERTIES);
        testSubscriber2.assertValue(PROPERTIES);
    }

    @Test
    public void getProperties_repositoryCachesAfterFirstSubscription_whenPropertiesAvailableInRemoteStorage() {
        // Given that the remote data source has data available
        setPropertiesAvailable(mPropertiesRemoteDataSource, PROPERTIES);
        // And the local data source does not have any data available
        setPropertiesNotAvailable(mPropertiesLocalDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Property>> testSubscriber1 = new TestSubscriber<>();
        mPropertiesRepository.getProperties().subscribe(testSubscriber1);

        TestSubscriber<List<Property>> testSubscriber2 = new TestSubscriber<>();
        mPropertiesRepository.getProperties().subscribe(testSubscriber2);

        // Then properties were only requested once from remote and local sources
        verify(mPropertiesRemoteDataSource).getProperties();
        verify(mPropertiesLocalDataSource).getProperties();
        assertFalse(mPropertiesRepository.mCacheIsDirty);
        testSubscriber1.assertValue(PROPERTIES);
        testSubscriber2.assertValue(PROPERTIES);
    }

    @Test
    public void getProperties_requestsAllPropertiesFromLocalDataSource() {
        // Given that the local data source has data available
        setPropertiesAvailable(mPropertiesLocalDataSource, PROPERTIES);
        // And the remote data source does not have any data available
        setPropertiesNotAvailable(mPropertiesRemoteDataSource);

        // When properties are requested from the properties repository
        mPropertiesRepository.getProperties().subscribe(mPropertiesTestSubscriber);

        // Then properties are loaded from the local data source
        verify(mPropertiesLocalDataSource).getProperties();
        mPropertiesTestSubscriber.assertValue(PROPERTIES);
    }

    @Test
    public void saveProperty_savesPropertyToServiceAPI() {
        // Given a stub property with title and description
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);

        // When a property is saved to the properties repository
        mPropertiesRepository.saveProperty(newProperty);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).saveProperty(newProperty);
        verify(mPropertiesLocalDataSource).saveProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
    }

    @Test
    public void completeProperty_completesPropertyToServiceAPIUpdatesCache() {
        // Given a stub active property with title and description added in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);

        // When a property is completed to the properties repository
        mPropertiesRepository.completeProperty(newProperty);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).completeProperty(newProperty);
        verify(mPropertiesLocalDataSource).completeProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
        assertThat(mPropertiesRepository.mCachedProperties.get(newProperty.getId()).isActive(), is(false));
    }

    @Test
    public void completePropertyId_completesPropertyToServiceAPIUpdatesCache() {
        // Given a stub active property with title and description added in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);

        // When a property is completed using its id to the properties repository
        mPropertiesRepository.completeProperty(newProperty.getId());

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).completeProperty(newProperty);
        verify(mPropertiesLocalDataSource).completeProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
        assertThat(mPropertiesRepository.mCachedProperties.get(newProperty.getId()).isActive(), is(false));
    }

    @Test
    public void activateProperty_activatesPropertyToServiceAPIUpdatesCache() {
        // Given a stub completed property with title and description in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);

        // When a completed property is activated to the properties repository
        mPropertiesRepository.activateProperty(newProperty);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).activateProperty(newProperty);
        verify(mPropertiesLocalDataSource).activateProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
        assertThat(mPropertiesRepository.mCachedProperties.get(newProperty.getId()).isActive(), is(true));
    }

    @Test
    public void activatePropertyId_activatesPropertyToServiceAPIUpdatesCache() {
        // Given a stub completed property with title and description in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);

        // When a completed property is activated with its id to the properties repository
        mPropertiesRepository.activateProperty(newProperty.getId());

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).activateProperty(newProperty);
        verify(mPropertiesLocalDataSource).activateProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
        assertThat(mPropertiesRepository.mCachedProperties.get(newProperty.getId()).isActive(), is(true));
    }

    @Test
    public void getProperty_requestsSinglePropertyFromLocalDataSource() {
        // Given a stub completed property with title and description in the local repository
        Property property = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        Optional<Property> propertyOptional = Optional.of(property);
        setPropertyAvailable(mPropertiesLocalDataSource, propertyOptional);
        // And the property not available in the remote repository
        setPropertyNotAvailable(mPropertiesRemoteDataSource, propertyOptional.get().getId());

        // When a property is requested from the properties repository
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mPropertiesRepository.getProperty(property.getId()).subscribe(testSubscriber);

        // Then the property is loaded from the database
        verify(mPropertiesLocalDataSource).getProperty(eq(property.getId()));
        testSubscriber.assertValue(propertyOptional);
    }

    @Test
    public void getProperty_whenDataNotLocal_fails() {
        // Given a stub completed property with title and description in the remote repository
        Property property = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        Optional<Property> propertyOptional = Optional.of(property);
        setPropertyAvailable(mPropertiesRemoteDataSource, propertyOptional);
        // And the property not available in the local repository
        setPropertyNotAvailable(mPropertiesLocalDataSource, property.getId());

        // When a property is requested from the properties repository
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mPropertiesRepository.getProperty(property.getId()).subscribe(testSubscriber);

        // then empty Optional is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void deleteCompletedProperties_deleteCompletedPropertiesToServiceAPIUpdatesCache() {
        // Given 2 stub completed properties and 1 stub active properties in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f, true);
        mPropertiesRepository.saveProperty(newProperty);
        Property newProperty2 = new Property(PROPERTY_TITLE2, "Some Property Description", 10f, false);
        mPropertiesRepository.saveProperty(newProperty2);
        Property newProperty3 = new Property(PROPERTY_TITLE3, "Some Property Description", 10f, true);
        mPropertiesRepository.saveProperty(newProperty3);

        // When a completed properties are cleared to the properties repository
        mPropertiesRepository.clearCompletedProperties();

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPropertiesRemoteDataSource).clearCompletedProperties();
        verify(mPropertiesLocalDataSource).clearCompletedProperties();

        assertThat(mPropertiesRepository.mCachedProperties.size(), is(1));
        assertTrue(mPropertiesRepository.mCachedProperties.get(newProperty2.getId()).isActive());
        assertThat(mPropertiesRepository.mCachedProperties.get(newProperty2.getId()).getTitle(), is(PROPERTY_TITLE2));
    }

    @Test
    public void deleteAllProperties_deletePropertiesToServiceAPIUpdatesCache() {
        // Given 2 stub completed properties and 1 stub active properties in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);
        Property newProperty2 = new Property(PROPERTY_TITLE2, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty2);
        Property newProperty3 = new Property(PROPERTY_TITLE3, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty3);

        // When all properties are deleted to the properties repository
        mPropertiesRepository.deleteAllProperties();

        // Verify the data sources were called
        verify(mPropertiesRemoteDataSource).deleteAllProperties();
        verify(mPropertiesLocalDataSource).deleteAllProperties();

        assertThat(mPropertiesRepository.mCachedProperties.size(), is(0));
    }

    @Test
    public void deleteProperty_deletePropertyToServiceAPIRemovedFromCache() {
        // Given a property in the repository
        Property newProperty = new Property(PROPERTY_TITLE, "Some Property Description", 10f);
        mPropertiesRepository.saveProperty(newProperty);
        assertThat(mPropertiesRepository.mCachedProperties.containsKey(newProperty.getId()), is(true));

        // When deleted
        mPropertiesRepository.deleteProperty(newProperty.getId());

        // Verify the data sources were called
        verify(mPropertiesRemoteDataSource).deleteProperty(newProperty.getId());
        verify(mPropertiesLocalDataSource).deleteProperty(newProperty.getId());

        // Verify it's removed from repository
        assertThat(mPropertiesRepository.mCachedProperties.containsKey(newProperty.getId()), is(false));
    }

    @Test
    public void getPropertiesWithDirtyCache_propertiesAreRetrievedFromRemote() {
        // Given that the remote data source has data available
        setPropertiesAvailable(mPropertiesRemoteDataSource, PROPERTIES);

        // When calling getProperties in the repository with dirty cache
        mPropertiesRepository.refreshProperties();
        mPropertiesRepository.getProperties().subscribe(mPropertiesTestSubscriber);

        // Verify the properties from the remote data source are returned, not the local
        verify(mPropertiesLocalDataSource, never()).getProperties();
        verify(mPropertiesRemoteDataSource).getProperties();
        mPropertiesTestSubscriber.assertValue(PROPERTIES);
    }

    @Test
    public void getPropertiesWithLocalDataSourceUnavailable_propertiesAreRetrievedFromRemote() {
        // Given that the local data source has no data available
        setPropertiesNotAvailable(mPropertiesLocalDataSource);
        // And the remote data source has data available
        setPropertiesAvailable(mPropertiesRemoteDataSource, PROPERTIES);

        // When calling getProperties in the repository
        mPropertiesRepository.getProperties().subscribe(mPropertiesTestSubscriber);

        // Verify the properties from the remote data source are returned
        verify(mPropertiesRemoteDataSource).getProperties();
        mPropertiesTestSubscriber.assertValue(PROPERTIES);
    }

    @Test
    public void getPropertiesWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given that the local data source has no data available
        setPropertiesNotAvailable(mPropertiesLocalDataSource);
        // And the remote data source has no data available
        setPropertiesNotAvailable(mPropertiesRemoteDataSource);

        // When calling getProperties in the repository
        mPropertiesRepository.getProperties().subscribe(mPropertiesTestSubscriber);

        // Verify no data is returned
        mPropertiesTestSubscriber.assertNoValues();
        // Verify that error is returned
        mPropertiesTestSubscriber.assertError(NoSuchElementException.class);
    }

    @Test
    public void getPropertyWithBothDataSourcesUnavailable_firesOnError() {
        // Given a property id
        final String propertyId = "123";
        // And the local data source has no data available
        setPropertyNotAvailable(mPropertiesLocalDataSource, propertyId);
        // And the remote data source has no data available
        setPropertyNotAvailable(mPropertiesRemoteDataSource, propertyId);

        // When calling getProperty in the repository
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mPropertiesRepository.getProperty(propertyId).subscribe(testSubscriber);

        // Verify that error is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void getProperties_refreshesLocalDataSource() {
        // Given that the remote data source has data available
        setPropertiesAvailable(mPropertiesRemoteDataSource, PROPERTIES);

        // Mark cache as dirty to force a reload of data from remote data source.
        mPropertiesRepository.refreshProperties();

        // When calling getProperties in the repository
        mPropertiesRepository.getProperties().subscribe(mPropertiesTestSubscriber);

        // Verify that the data fetched from the remote data source was saved in local.
        verify(mPropertiesLocalDataSource, times(PROPERTIES.size())).saveProperty(any(Property.class));
        mPropertiesTestSubscriber.assertValue(PROPERTIES);
    }

    private void setPropertiesNotAvailable(PropertiesDataSource dataSource) {
        when(dataSource.getProperties()).thenReturn(Flowable.just(Collections.emptyList()));
    }

    private void setPropertiesAvailable(PropertiesDataSource dataSource, List<Property> properties) {
        // don't allow the data sources to complete.
        when(dataSource.getProperties()).thenReturn(Flowable.just(properties).concatWith(Flowable.never()));
    }

    private void setPropertyNotAvailable(PropertiesDataSource dataSource, String propertyId) {
        when(dataSource.getProperty(eq(propertyId))).thenReturn(Flowable.just(Optional.absent()));
    }

    private void setPropertyAvailable(PropertiesDataSource dataSource, Optional<Property> propertyOptional) {
        when(dataSource.getProperty(eq(propertyOptional.get().getId()))).thenReturn(Flowable.just(propertyOptional).concatWith(Flowable.never()));
    }
}
