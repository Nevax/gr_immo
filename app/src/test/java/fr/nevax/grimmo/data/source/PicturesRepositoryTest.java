/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.content.Context;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import fr.nevax.grimmo.data.Picture;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
public class PicturesRepositoryTest {

    private final static String PICTURE_TITLE = "title";

    private final static String PICTURE_TITLE2 = "title2";

    private final static String PICTURE_TITLE3 = "title3";

    private static List<Picture> PICTURES = Lists.newArrayList(
            new Picture(null, "Title1", "Description1", ""),
            new Picture(null, "Title2", "Description2", "")
    );

    private PicturesRepository mPicturesRepository;

    private TestSubscriber<List<Picture>> mPicturesTestSubscriber;

    @Mock
    private PicturesDataSource mPicturesRemoteDataSource;

    @Mock
    private PicturesDataSource mPicturesLocalDataSource;

    @Mock
    private Context mContext;


    @Before
    public void setupPicturesRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mPicturesRepository = PicturesRepository.getInstance(
                mPicturesRemoteDataSource, mPicturesLocalDataSource);

        mPicturesTestSubscriber = new TestSubscriber<>();
    }

    @After
    public void destroyRepositoryInstance() {
        PicturesRepository.destroyInstance();
    }

    @Test
    public void getPictures_repositoryCachesAfterFirstSubscription_whenPicturesAvailableInLocalStorage() {
        // Given that the local data source has data available
        setPicturesAvailable(mPicturesLocalDataSource, PICTURES);
        // And the remote data source does not have any data available
        setPicturesNotAvailable(mPicturesRemoteDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Picture>> testSubscriber1 = new TestSubscriber<>();
        mPicturesRepository.getPictures().subscribe(testSubscriber1);

        TestSubscriber<List<Picture>> testSubscriber2 = new TestSubscriber<>();
        mPicturesRepository.getPictures().subscribe(testSubscriber2);

        // Then pictures were only requested once from remote and local sources
        verify(mPicturesRemoteDataSource).getPictures();
        verify(mPicturesLocalDataSource).getPictures();
        //
        assertFalse(mPicturesRepository.mCacheIsDirty);
        testSubscriber1.assertValue(PICTURES);
        testSubscriber2.assertValue(PICTURES);
    }

    @Test
    public void getPictures_repositoryCachesAfterFirstSubscription_whenPicturesAvailableInRemoteStorage() {
        // Given that the remote data source has data available
        setPicturesAvailable(mPicturesRemoteDataSource, PICTURES);
        // And the local data source does not have any data available
        setPicturesNotAvailable(mPicturesLocalDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Picture>> testSubscriber1 = new TestSubscriber<>();
        mPicturesRepository.getPictures().subscribe(testSubscriber1);

        TestSubscriber<List<Picture>> testSubscriber2 = new TestSubscriber<>();
        mPicturesRepository.getPictures().subscribe(testSubscriber2);

        // Then pictures were only requested once from remote and local sources
        verify(mPicturesRemoteDataSource).getPictures();
        verify(mPicturesLocalDataSource).getPictures();
        assertFalse(mPicturesRepository.mCacheIsDirty);
        testSubscriber1.assertValue(PICTURES);
        testSubscriber2.assertValue(PICTURES);
    }

    @Test
    public void getPictures_requestsAllPicturesFromLocalDataSource() {
        // Given that the local data source has data available
        setPicturesAvailable(mPicturesLocalDataSource, PICTURES);
        // And the remote data source does not have any data available
        setPicturesNotAvailable(mPicturesRemoteDataSource);

        // When pictures are requested from the pictures repository
        mPicturesRepository.getPictures().subscribe(mPicturesTestSubscriber);

        // Then pictures are loaded from the local data source
        verify(mPicturesLocalDataSource).getPictures();
        mPicturesTestSubscriber.assertValue(PICTURES);
    }

    @Test
    public void savePicture_savesPictureToServiceAPI() {
        // Given a stub picture with title and description
        Picture newPicture = new Picture(null, PICTURE_TITLE, "Some Picture Description", "");

        // When a picture is saved to the pictures repository
        mPicturesRepository.savePicture(newPicture);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mPicturesRemoteDataSource).savePicture(newPicture);
        verify(mPicturesLocalDataSource).savePicture(newPicture);
        assertThat(mPicturesRepository.mCachedPictures.size(), is(1));
    }

    @Test
    public void getPicture_requestsSinglePictureFromLocalDataSource() {
        // Given a stub completed picture with title and description in the local repository
        Picture picture = new Picture(null, PICTURE_TITLE, "Some Picture Description", "");
        Optional<Picture> pictureOptional = Optional.of(picture);
        setPictureAvailable(mPicturesLocalDataSource, pictureOptional);
        // And the picture not available in the remote repository
        setPictureNotAvailable(mPicturesRemoteDataSource, pictureOptional.get().getId());

        // When a picture is requested from the pictures repository
        TestSubscriber<Optional<Picture>> testSubscriber = new TestSubscriber<>();
        mPicturesRepository.getPicture(picture.getId()).subscribe(testSubscriber);

        // Then the picture is loaded from the database
        verify(mPicturesLocalDataSource).getPicture(eq(picture.getId()));
        testSubscriber.assertValue(pictureOptional);
    }

    @Test
    public void getPicture_whenDataNotLocal_fails() {
        // Given a stub completed picture with title and description in the remote repository
        Picture picture = new Picture(null, PICTURE_TITLE, "Some Picture Description", "");
        Optional<Picture> pictureOptional = Optional.of(picture);
        setPictureAvailable(mPicturesRemoteDataSource, pictureOptional);
        // And the picture not available in the local repository
        setPictureNotAvailable(mPicturesLocalDataSource, picture.getId());

        // When a picture is requested from the pictures repository
        TestSubscriber<Optional<Picture>> testSubscriber = new TestSubscriber<>();
        mPicturesRepository.getPicture(picture.getId()).subscribe(testSubscriber);

        // then empty Optional is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void deleteAllPictures_deletePicturesToServiceAPIUpdatesCache() {
        // Given 2 stub completed pictures and 1 stub active pictures in the repository
        Picture newPicture = new Picture(null, PICTURE_TITLE, "Some Picture Description", "");
        mPicturesRepository.savePicture(newPicture);
        Picture newPicture2 = new Picture(null, PICTURE_TITLE2, "Some Picture Description", "");
        mPicturesRepository.savePicture(newPicture2);
        Picture newPicture3 = new Picture(null, PICTURE_TITLE3, "Some Picture Description", "");
        mPicturesRepository.savePicture(newPicture3);

        // When all pictures are deleted to the pictures repository
        mPicturesRepository.deleteAllPictures();

        // Verify the data sources were called
        verify(mPicturesRemoteDataSource).deleteAllPictures();
        verify(mPicturesLocalDataSource).deleteAllPictures();

        assertThat(mPicturesRepository.mCachedPictures.size(), is(0));
    }

    @Test
    public void deletePicture_deletePictureToServiceAPIRemovedFromCache() {
        // Given a picture in the repository
        Picture newPicture = new Picture(null, PICTURE_TITLE, "Some Picture Description", "");
        mPicturesRepository.savePicture(newPicture);
        assertThat(mPicturesRepository.mCachedPictures.containsKey(newPicture.getId()), is(true));

        // When deleted
        mPicturesRepository.deletePicture(newPicture.getId());

        // Verify the data sources were called
        verify(mPicturesRemoteDataSource).deletePicture(newPicture.getId());
        verify(mPicturesLocalDataSource).deletePicture(newPicture.getId());

        // Verify it's removed from repository
        assertThat(mPicturesRepository.mCachedPictures.containsKey(newPicture.getId()), is(false));
    }

    @Test
    public void getPicturesWithDirtyCache_picturesAreRetrievedFromRemote() {
        // Given that the remote data source has data available
        setPicturesAvailable(mPicturesRemoteDataSource, PICTURES);

        // When calling getPictures in the repository with dirty cache
        mPicturesRepository.refreshPictures();
        mPicturesRepository.getPictures().subscribe(mPicturesTestSubscriber);

        // Verify the pictures from the remote data source are returned, not the local
        verify(mPicturesLocalDataSource, never()).getPictures();
        verify(mPicturesRemoteDataSource).getPictures();
        mPicturesTestSubscriber.assertValue(PICTURES);
    }

    @Test
    public void getPicturesWithLocalDataSourceUnavailable_picturesAreRetrievedFromRemote() {
        // Given that the local data source has no data available
        setPicturesNotAvailable(mPicturesLocalDataSource);
        // And the remote data source has data available
        setPicturesAvailable(mPicturesRemoteDataSource, PICTURES);

        // When calling getPictures in the repository
        mPicturesRepository.getPictures().subscribe(mPicturesTestSubscriber);

        // Verify the pictures from the remote data source are returned
        verify(mPicturesRemoteDataSource).getPictures();
        mPicturesTestSubscriber.assertValue(PICTURES);
    }

    @Test
    public void getPicturesWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given that the local data source has no data available
        setPicturesNotAvailable(mPicturesLocalDataSource);
        // And the remote data source has no data available
        setPicturesNotAvailable(mPicturesRemoteDataSource);

        // When calling getPictures in the repository
        mPicturesRepository.getPictures().subscribe(mPicturesTestSubscriber);

        // Verify no data is returned
        mPicturesTestSubscriber.assertNoValues();
        // Verify that error is returned
        mPicturesTestSubscriber.assertError(NoSuchElementException.class);
    }

    @Test
    public void getPictureWithBothDataSourcesUnavailable_firesOnError() {
        // Given a picture id
        final String pictureId = "123";
        // And the local data source has no data available
        setPictureNotAvailable(mPicturesLocalDataSource, pictureId);
        // And the remote data source has no data available
        setPictureNotAvailable(mPicturesRemoteDataSource, pictureId);

        // When calling getPicture in the repository
        TestSubscriber<Optional<Picture>> testSubscriber = new TestSubscriber<>();
        mPicturesRepository.getPicture(pictureId).subscribe(testSubscriber);

        // Verify that error is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void getPictures_refreshesLocalDataSource() {
        // Given that the remote data source has data available
        setPicturesAvailable(mPicturesRemoteDataSource, PICTURES);

        // Mark cache as dirty to force a reload of data from remote data source.
        mPicturesRepository.refreshPictures();

        // When calling getPictures in the repository
        mPicturesRepository.getPictures().subscribe(mPicturesTestSubscriber);

        // Verify that the data fetched from the remote data source was saved in local.
        verify(mPicturesLocalDataSource, times(PICTURES.size())).savePicture(any(Picture.class));
        mPicturesTestSubscriber.assertValue(PICTURES);
    }

    private void setPicturesNotAvailable(PicturesDataSource dataSource) {
        when(dataSource.getPictures()).thenReturn(Flowable.just(Collections.emptyList()));
    }

    private void setPicturesAvailable(PicturesDataSource dataSource, List<Picture> pictures) {
        // don't allow the data sources to complete.
        when(dataSource.getPictures()).thenReturn(Flowable.just(pictures).concatWith(Flowable.never()));
    }

    private void setPictureNotAvailable(PicturesDataSource dataSource, String pictureId) {
        when(dataSource.getPicture(eq(pictureId))).thenReturn(Flowable.just(Optional.absent()));
    }

    private void setPictureAvailable(PicturesDataSource dataSource, Optional<Picture> pictureOptional) {
        when(dataSource.getPicture(eq(pictureOptional.get().getId()))).thenReturn(Flowable.just(pictureOptional).concatWith(Flowable.never()));
    }
}
