/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.content.Context;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import fr.nevax.grimmo.data.Room;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
public class RoomsRepositoryTest {

    private final static String ROOM_TITLE = "title";

    private final static String ROOM_TITLE2 = "title2";

    private final static String ROOM_TITLE3 = "title3";

    private static List<Room> ROOMS = Lists.newArrayList(
            new Room(null, "Title1", "Description1", 10, null, null, null),
            new Room(null, "Title2", "Description2", 10, null, null, null)
    );

    private RoomsRepository mRoomsRepository;

    private TestSubscriber<List<Room>> mRoomsTestSubscriber;

    @Mock
    private RoomsDataSource mRoomsRemoteDataSource;

    @Mock
    private RoomsDataSource mRoomsLocalDataSource;

    @Mock
    private Context mContext;

    @Before
    public void setupRoomsRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mRoomsRepository = RoomsRepository.getInstance(
                mRoomsRemoteDataSource, mRoomsLocalDataSource);

        mRoomsTestSubscriber = new TestSubscriber<>();
    }

    @After
    public void destroyRepositoryInstance() {
        RoomsRepository.destroyInstance();
    }

    @Test
    public void getRooms_repositoryCachesAfterFirstSubscription_whenRoomsAvailableInLocalStorage() {
        // Given that the local data source has data available
        setRoomsAvailable(mRoomsLocalDataSource, ROOMS);
        // And the remote data source does not have any data available
        setRoomsNotAvailable(mRoomsRemoteDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Room>> testSubscriber1 = new TestSubscriber<>();
        mRoomsRepository.getRooms().subscribe(testSubscriber1);

        TestSubscriber<List<Room>> testSubscriber2 = new TestSubscriber<>();
        mRoomsRepository.getRooms().subscribe(testSubscriber2);

        // Then rooms were only requested once from remote and local sources
        verify(mRoomsRemoteDataSource).getRooms();
        verify(mRoomsLocalDataSource).getRooms();
        //
        assertFalse(mRoomsRepository.mCacheIsDirty);
        testSubscriber1.assertValue(ROOMS);
        testSubscriber2.assertValue(ROOMS);
    }

    @Test
    public void getRooms_repositoryCachesAfterFirstSubscription_whenRoomsAvailableInRemoteStorage() {
        // Given that the remote data source has data available
        setRoomsAvailable(mRoomsRemoteDataSource, ROOMS);
        // And the local data source does not have any data available
        setRoomsNotAvailable(mRoomsLocalDataSource);

        // When two subscriptions are set
        TestSubscriber<List<Room>> testSubscriber1 = new TestSubscriber<>();
        mRoomsRepository.getRooms().subscribe(testSubscriber1);

        TestSubscriber<List<Room>> testSubscriber2 = new TestSubscriber<>();
        mRoomsRepository.getRooms().subscribe(testSubscriber2);

        // Then rooms were only requested once from remote and local sources
        verify(mRoomsRemoteDataSource).getRooms();
        verify(mRoomsLocalDataSource).getRooms();
        assertFalse(mRoomsRepository.mCacheIsDirty);
        testSubscriber1.assertValue(ROOMS);
        testSubscriber2.assertValue(ROOMS);
    }

    @Test
    public void getRooms_requestsAllRoomsFromLocalDataSource() {
        // Given that the local data source has data available
        setRoomsAvailable(mRoomsLocalDataSource, ROOMS);
        // And the remote data source does not have any data available
        setRoomsNotAvailable(mRoomsRemoteDataSource);

        // When rooms are requested from the rooms repository
        mRoomsRepository.getRooms().subscribe(mRoomsTestSubscriber);

        // Then rooms are loaded from the local data source
        verify(mRoomsLocalDataSource).getRooms();
        mRoomsTestSubscriber.assertValue(ROOMS);
    }

    @Test
    public void saveRoom_savesRoomToServiceAPI() {
        // Given a stub room with title and description
        Room newRoom = ROOMS.get(0);

        // When a room is saved to the rooms repository
        mRoomsRepository.saveRoom(newRoom);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mRoomsRemoteDataSource).saveRoom(newRoom);
        verify(mRoomsLocalDataSource).saveRoom(newRoom);
        assertThat(mRoomsRepository.mCachedRooms.size(), is(1));
    }

    @Test
    public void getRoom_requestsSingleRoomFromLocalDataSource() {
        // Given a stub completed room with title and description in the local repository
        Room room = ROOMS.get(0);
        Optional<Room> roomOptional = Optional.of(room);
        setRoomAvailable(mRoomsLocalDataSource, roomOptional);
        // And the room not available in the remote repository
        setRoomNotAvailable(mRoomsRemoteDataSource, roomOptional.get().getId());

        // When a room is requested from the rooms repository
        TestSubscriber<Optional<Room>> testSubscriber = new TestSubscriber<>();
        mRoomsRepository.getRoom(room.getId()).subscribe(testSubscriber);

        // Then the room is loaded from the database
        verify(mRoomsLocalDataSource).getRoom(eq(room.getId()));
        testSubscriber.assertValue(roomOptional);
    }

    @Test
    public void getRoom_whenDataNotLocal_fails() {
        // Given a stub completed room with title and description in the remote repository
        Room room = ROOMS.get(0);
        Optional<Room> roomOptional = Optional.of(room);
        setRoomAvailable(mRoomsRemoteDataSource, roomOptional);
        // And the room not available in the local repository
        setRoomNotAvailable(mRoomsLocalDataSource, room.getId());

        // When a room is requested from the rooms repository
        TestSubscriber<Optional<Room>> testSubscriber = new TestSubscriber<>();
        mRoomsRepository.getRoom(room.getId()).subscribe(testSubscriber);

        // then empty Optional is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void deleteAllRooms_deleteRoomsToServiceAPIUpdatesCache() {
        // Given 2 stub completed rooms and 1 stub active rooms in the repository
        Room newRoom = new Room(null, ROOM_TITLE, "Some Room Description", 10, null, null, null);
        mRoomsRepository.saveRoom(newRoom);
        Room newRoom2 = new Room(null, ROOM_TITLE2, "Some Room Description", 10, null, null, null);
        mRoomsRepository.saveRoom(newRoom2);
        Room newRoom3 = new Room(null, ROOM_TITLE3, "Some Room Description", 10, null, null, null);
        mRoomsRepository.saveRoom(newRoom3);

        // When all rooms are deleted to the rooms repository
        mRoomsRepository.deleteAllRooms();

        // Verify the data sources were called
        verify(mRoomsRemoteDataSource).deleteAllRooms();
        verify(mRoomsLocalDataSource).deleteAllRooms();

        assertThat(mRoomsRepository.mCachedRooms.size(), is(0));
    }

    @Test
    public void deleteRoom_deleteRoomToServiceAPIRemovedFromCache() {
        // Given a room in the repository
        Room newRoom = new Room(null, ROOM_TITLE, "Some Room Description", 10, null, null, null);
        mRoomsRepository.saveRoom(newRoom);
        assertThat(mRoomsRepository.mCachedRooms.containsKey(newRoom.getId()), is(true));

        // When deleted
        mRoomsRepository.deleteRoom(newRoom.getId());

        // Verify the data sources were called
        verify(mRoomsRemoteDataSource).deleteRoom(newRoom.getId());
        verify(mRoomsLocalDataSource).deleteRoom(newRoom.getId());

        // Verify it's removed from repository
        assertThat(mRoomsRepository.mCachedRooms.containsKey(newRoom.getId()), is(false));
    }

    @Test
    public void getRoomsWithDirtyCache_roomsAreRetrievedFromRemote() {
        // Given that the remote data source has data available
        setRoomsAvailable(mRoomsRemoteDataSource, ROOMS);

        // When calling getRooms in the repository with dirty cache
        mRoomsRepository.refreshRooms();
        mRoomsRepository.getRooms().subscribe(mRoomsTestSubscriber);

        // Verify the rooms from the remote data source are returned, not the local
        verify(mRoomsLocalDataSource, never()).getRooms();
        verify(mRoomsRemoteDataSource).getRooms();
        mRoomsTestSubscriber.assertValue(ROOMS);
    }

    @Test
    public void getRoomsWithLocalDataSourceUnavailable_roomsAreRetrievedFromRemote() {
        // Given that the local data source has no data available
        setRoomsNotAvailable(mRoomsLocalDataSource);
        // And the remote data source has data available
        setRoomsAvailable(mRoomsRemoteDataSource, ROOMS);

        // When calling getRooms in the repository
        mRoomsRepository.getRooms().subscribe(mRoomsTestSubscriber);

        // Verify the rooms from the remote data source are returned
        verify(mRoomsRemoteDataSource).getRooms();
        mRoomsTestSubscriber.assertValue(ROOMS);
    }

    @Test
    public void getRoomsWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given that the local data source has no data available
        setRoomsNotAvailable(mRoomsLocalDataSource);
        // And the remote data source has no data available
        setRoomsNotAvailable(mRoomsRemoteDataSource);

        // When calling getRooms in the repository
        mRoomsRepository.getRooms().subscribe(mRoomsTestSubscriber);

        // Verify no data is returned
        mRoomsTestSubscriber.assertNoValues();
        // Verify that error is returned
        mRoomsTestSubscriber.assertError(NoSuchElementException.class);
    }

    @Test
    public void getRoomWithBothDataSourcesUnavailable_firesOnError() {
        // Given a room id
        final String roomId = "123";
        // And the local data source has no data available
        setRoomNotAvailable(mRoomsLocalDataSource, roomId);
        // And the remote data source has no data available
        setRoomNotAvailable(mRoomsRemoteDataSource, roomId);

        // When calling getRoom in the repository
        TestSubscriber<Optional<Room>> testSubscriber = new TestSubscriber<>();
        mRoomsRepository.getRoom(roomId).subscribe(testSubscriber);

        // Verify that error is returned
        testSubscriber.assertValue(Optional.absent());
    }

    @Test
    public void getRooms_refreshesLocalDataSource() {
        // Given that the remote data source has data available
        setRoomsAvailable(mRoomsRemoteDataSource, ROOMS);

        // Mark cache as dirty to force a reload of data from remote data source.
        mRoomsRepository.refreshRooms();

        // When calling getRooms in the repository
        mRoomsRepository.getRooms().subscribe(mRoomsTestSubscriber);

        // Verify that the data fetched from the remote data source was saved in local.
        verify(mRoomsLocalDataSource, times(ROOMS.size())).saveRoom(any(Room.class));
        mRoomsTestSubscriber.assertValue(ROOMS);
    }

    private void setRoomsNotAvailable(RoomsDataSource dataSource) {
        when(dataSource.getRooms()).thenReturn(Flowable.just(Collections.emptyList()));
    }

    private void setRoomsAvailable(RoomsDataSource dataSource, List<Room> rooms) {
        // don't allow the data sources to complete.
        when(dataSource.getRooms()).thenReturn(Flowable.just(rooms).concatWith(Flowable.never()));
    }

    private void setRoomNotAvailable(RoomsDataSource dataSource, String roomId) {
        when(dataSource.getRoom(eq(roomId))).thenReturn(Flowable.just(Optional.absent()));
    }

    private void setRoomAvailable(RoomsDataSource dataSource, Optional<Room> roomOptional) {
        when(dataSource.getRoom(eq(roomOptional.get().getId()))).thenReturn(Flowable.just(roomOptional).concatWith(Flowable.never()));
    }
}
