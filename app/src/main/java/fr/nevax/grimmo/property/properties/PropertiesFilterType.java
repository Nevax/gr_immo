/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.properties;

/**
 * Used with the filter spinner in the properties list.
 */
public enum PropertiesFilterType {
    /**
     * Do not filter properties.
     */
    ALL_PROPERTIES,

    /**
     * Filters only the active (not completed yet) properties.
     */
    ACTIVE_PROPERTIES,

    /**
     * Filters only the completed properties.
     */
    COMPLETED_PROPERTIES
}
