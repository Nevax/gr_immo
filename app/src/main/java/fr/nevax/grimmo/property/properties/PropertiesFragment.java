/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.properties;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyActivity;
import fr.nevax.grimmo.property.propertydetail.PropertyDetailActivity;
import fr.nevax.grimmo.property.propertystatistics.PropertyStatisticsActivity;
import fr.nevax.grimmo.setting.SettingsActivity;
import fr.nevax.grimmo.util.ScrollChildSwipeRefreshLayout;

import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.data.Property.NO_IMAGE;
import static fr.nevax.grimmo.util.ValueUtils.DECIMAL_FORMAT;
import static fr.nevax.grimmo.util.ValueUtils.formatPriceValue;

/**
 * Display a grid of {@link Property}s. User can choose to view all, active or completed properties.
 */
public class PropertiesFragment extends Fragment implements PropertiesContract.View {

    private PropertiesContract.Presenter mPresenter;
    /**
     * Listener for clicks on properties in the RecyclerView.
     */
    PropertyItemListener mItemListener = new PropertyItemListener() {
        @Override
        public void onPropertyClick(Property clickedProperty) {
            mPresenter.openPropertyDetails(clickedProperty);
        }

        @Override
        public void onCompletePropertyClick(Property completedProperty) {
            mPresenter.completeProperty(completedProperty);
        }

        @Override
        public void onActivatePropertyClick(Property activatedProperty) {
            mPresenter.activateProperty(activatedProperty);
        }
    };
    private PropertiesAdapter mListAdapter;
    private View mNoPropertiesView;
    private ImageView mNoPropertyIcon;
    private TextView mNoPropertyMainView;
    private TextView mNoPropertyAddView;
    private LinearLayout mPropertiesView;
    private TextView mFilteringLabelView;

    public PropertiesFragment() {
    }

    public static PropertiesFragment newInstance() {
        return new PropertiesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListAdapter = new PropertiesAdapter(new ArrayList<>(0), mItemListener, getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(@NonNull PropertiesContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.result(requestCode, resultCode);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.properties_frag, container, false);

        // Set up properties view
        RecyclerView recyclerView = root.findViewById(R.id.properties_list);
        recyclerView.setAdapter(mListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mFilteringLabelView = root.findViewById(R.id.filteringLabel);
        mPropertiesView = root.findViewById(R.id.propertiesLL);

        // Set up  no properties view
        mNoPropertiesView = root.findViewById(R.id.noProperties);
        mNoPropertyIcon = root.findViewById(R.id.noPropertiesIcon);
        mNoPropertyMainView = root.findViewById(R.id.noPropertiesMain);
        mNoPropertyAddView = root.findViewById(R.id.noPropertiesAdd);
        mNoPropertyAddView.setOnClickListener(__ -> showAddProperty());

        // Set up floating action button
        FloatingActionButton fab = getActivity().findViewById(R.id.fab_add_property);

        fab.setImageResource(R.drawable.ic_add);
        fab.setOnClickListener(__ -> mPresenter.addNewProperty());


        // Set up progress indicator
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                root.findViewById(R.id.refresh_layout_properties);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.loadProperties(false));

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                mPresenter.clearCompletedProperties();
                break;
            case R.id.menu_filter:
                showFilteringPopUpMenu();
                break;
            case R.id.menu_refresh:
                mPresenter.loadProperties(true);
                break;
            case R.id.menu_statistics:
                showStatistics();
                break;
            case R.id.menu_settings:
                showSettings();
                break;
        }
        return true;
    }

    @Override
    public void showStatistics() {
        startActivity(new Intent(getContext(), PropertyStatisticsActivity.class));
    }

    @Override
    public void showSettings() {
        startActivity(new Intent(getContext(), SettingsActivity.class));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.properties_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showFilteringPopUpMenu() {
        PopupMenu popup = new PopupMenu(getContext(), getActivity().findViewById(R.id.menu_filter));
        popup.getMenuInflater().inflate(R.menu.filter_properties, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.active:
                    mPresenter.setFiltering(PropertiesFilterType.ACTIVE_PROPERTIES);
                    break;
                case R.id.completed:
                    mPresenter.setFiltering(PropertiesFilterType.COMPLETED_PROPERTIES);
                    break;
                default:
                    mPresenter.setFiltering(PropertiesFilterType.ALL_PROPERTIES);
                    break;
            }
            mPresenter.loadProperties(false);
            return true;
        });

        popup.show();
    }

    @Override
    public void setLoadingIndicator(final boolean active) {

        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = getView().findViewById(R.id.refresh_layout_properties);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showProperties(List<Property> properties) {
        mListAdapter.replaceData(properties);

        mPropertiesView.setVisibility(View.VISIBLE);
        mNoPropertiesView.setVisibility(View.GONE);
    }

    @Override
    public void showNoActiveProperties() {
        showNoPropertiesViews(
                getResources().getString(R.string.no_properties_active),
                R.drawable.ic_check_circle_24dp,
                false
        );
    }

    @Override
    public void showNoProperties() {
        showNoPropertiesViews(
                getResources().getString(R.string.no_properties_all),
                R.drawable.ic_assignment_turned_in_24dp,
                false
        );
    }

    @Override
    public void showNoCompletedProperties() {
        showNoPropertiesViews(
                getResources().getString(R.string.no_properties_completed),
                R.drawable.ic_verified_user_24dp,
                false
        );
    }

    @Override
    public void showSuccessfullySavedMessage() {
        showMessage(getString(R.string.successfully_saved_property_message));
    }

    private void showNoPropertiesViews(String mainText, int iconRes, boolean showAddView) {
        mPropertiesView.setVisibility(View.GONE);
        mNoPropertiesView.setVisibility(View.VISIBLE);

        mNoPropertyMainView.setText(mainText);
        mNoPropertyIcon.setImageDrawable(getResources().getDrawable(iconRes));
        mNoPropertyAddView.setVisibility(showAddView ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showActiveFilterLabel() {
        mFilteringLabelView.setText(getResources().getString(R.string.label_active_properties));
    }

    @Override
    public void showCompletedFilterLabel() {
        mFilteringLabelView.setText(getResources().getString(R.string.label_completed_properties));
    }

    @Override
    public void showAllFilterLabel() {
        mFilteringLabelView.setText(getResources().getString(R.string.label_all_properties));
    }

    @Override
    public void showAddProperty() {
        Intent intent = new Intent(getContext(), AddEditPropertyActivity.class);
        startActivityForResult(intent, AddEditPropertyActivity.REQUEST_ADD_PROPERTY);
    }

    @Override
    public void showPropertyDetailsUi(String propertyId) {
        // in it's own Activity, since it makes more sense that way and it gives us the flexibility
        // to show some Intent stubbing.
        Intent intent = new Intent(getContext(), PropertyDetailActivity.class);
        intent.putExtra(PropertyDetailActivity.EXTRA_PROPERTY_ID, propertyId);
        startActivity(intent);
    }

    @Override
    public void showPropertyMarkedComplete() {
        showMessage(getString(R.string.property_marked_complete));
    }

    @Override
    public void showPropertyMarkedActive() {
        showMessage(getString(R.string.property_marked_active));
    }

    @Override
    public void showCompletedPropertiesCleared() {
        showMessage(getString(R.string.completed_properties_cleared));
    }

    @Override
    public void showLoadingPropertiesError() {
        showMessage(getString(R.string.loading_properties_error));
    }

    private void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    public interface PropertyItemListener {

        void onPropertyClick(Property clickedProperty);

        void onCompletePropertyClick(Property completedProperty);

        void onActivatePropertyClick(Property activatedProperty);
    }

    private static class PropertiesAdapter extends RecyclerView.Adapter<PropertiesAdapter.SimpleViewHolder> {

        private List<Property> mProperties;
        private PropertyItemListener mItemListener;
        private Context mContext;

        public PropertiesAdapter(List<Property> properties, PropertyItemListener itemListener, Context context) {
            setProperties(properties);
            mItemListener = itemListener;
            mContext = context;
        }

        public void replaceData(List<Property> properties) {
            setProperties(properties);
            notifyDataSetChanged();
        }

        private void setProperties(List<Property> properties) {
            mProperties = checkNotNull(properties);
        }

        @NonNull
        @Override
        public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_item, parent, false);
            return new SimpleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
            final Property property = getItem(position);

            String type = mContext.getString(property.getPropertyType().getStringResourceId());
            String size = property.getSize() + " " + mContext.getString(R.string.size_unit);
            String roomCount = property.getRooms().size() + " " + mContext.getString(R.string.rooms);
            String price = formatPriceValue(property.getPrice(), DECIMAL_FORMAT)
                    + mContext.getString(R.string.euro);

            if (!property.getPictures().isEmpty() ) {
                final File imageFile = property.getPictures().get(0).getImageFile();
                if (imageFile.exists()) {
                    Picasso.get()
                            .load(imageFile)
                            .fit()
                            .into(holder.backgroundImageView);
                } else {
                    holder.showNoPicture();
                }
            } else {
                holder.showNoPicture();
            }

            holder.cityTV.setText(property.getTitleForList());
            holder.typeTV.setText(type);
            holder.priceTV.setText(price);
            holder.roomCountTV.setText(roomCount);
            holder.sizeTV.setText(size);

            holder.propertyView.setOnClickListener(__ -> mItemListener.onPropertyClick(property));
        }

        @Override
        public int getItemCount() {
            return mProperties.size();
        }


        public Property getItem(int i) {
            return mProperties.get(i);
        }

        static class SimpleViewHolder extends RecyclerView.ViewHolder {
            public final View propertyView;
            public final ImageView backgroundImageView;
            public final ImageView favoriteImageView;
            public final TextView cityTV;
            public final TextView typeTV;
            public final TextView priceTV;
            public final TextView roomCountTV;
            public final TextView sizeTV;

            public SimpleViewHolder(View view) {
                super(view);
                propertyView = view.findViewById(R.id.property_holder);
                backgroundImageView = view.findViewById(R.id.property_image);
                favoriteImageView = view.findViewById(R.id.property_favorite);
                cityTV = view.findViewById(R.id.property_city);
                typeTV = view.findViewById(R.id.property_type);
                priceTV = view.findViewById(R.id.property_price);
                roomCountTV = view.findViewById(R.id.property_number_rooms);
                sizeTV = view.findViewById(R.id.property_size);
            }

            public void showNoPicture() {
                Picasso.get()
                        .load(NO_IMAGE)
                        .fit()
                        .into(backgroundImageView);
            }
        }
    }

}
