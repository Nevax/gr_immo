/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertystatistics;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.google.common.primitives.Ints;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.util.EspressoIdlingResource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link PropertyStatisticsFragment}), retrieves the data and updates
 * the UI as required.
 */
public class PropertyStatisticsPresenter implements PropertyStatisticsContract.Presenter {

    @NonNull
    private final PropertiesRepository mPropertiesRepository;

    @NonNull
    private final PropertyStatisticsContract.View mStatisticsView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    public PropertyStatisticsPresenter(@NonNull PropertiesRepository propertiesRepository,
                                       @NonNull PropertyStatisticsContract.View statisticsView,
                                       @NonNull BaseSchedulerProvider schedulerProvider) {
        mPropertiesRepository = checkNotNull(propertiesRepository, "propertiesRepository cannot be null");
        mStatisticsView = checkNotNull(statisticsView, "statisticsView cannot be null!");
        mSchedulerProvider = checkNotNull(schedulerProvider, "schedulerProvider cannot be null");

        mCompositeDisposable = new CompositeDisposable();
        mStatisticsView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        loadStatistics();
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    private void loadStatistics() {
        mStatisticsView.setProgressIndicator(true);

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment(); // App is busy until further notice

        Flowable<Property> properties = mPropertiesRepository
                .getProperties()
                .flatMap(Flowable::fromIterable);
        Flowable<Long> completedProperties = properties.filter(Property::isCompleted).count().toFlowable();
        Flowable<Long> activeProperties = properties.filter(Property::isActive).count().toFlowable();
        Disposable disposable = Flowable
                .zip(completedProperties, activeProperties, (completed, active) -> Pair.create(active, completed))
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .doFinally(() -> {
                    if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                        EspressoIdlingResource.decrement(); // Set app as idle.
                    }
                })
                .subscribe(
                        // onNext
                        stats -> mStatisticsView.showStatistics(Ints.saturatedCast(stats.first), Ints.saturatedCast(stats.second)),
                        // onError
                        throwable -> mStatisticsView.showLoadingStatisticsError(),
                        // onCompleted
                        () -> mStatisticsView.setProgressIndicator(false));
        mCompositeDisposable.add(disposable);
    }
}
