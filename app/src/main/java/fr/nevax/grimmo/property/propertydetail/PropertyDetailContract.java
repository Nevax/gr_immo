/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertydetail;


import android.content.Context;

import java.util.List;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;
import fr.nevax.grimmo.data.Picture;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface PropertyDetailContract {

    interface View extends BaseView<fr.nevax.grimmo.property.propertydetail.PropertyDetailContract.Presenter> {

        void setLoadingIndicator(boolean active);

        void showMissingProperty();

        void hideTitle();

        void showTitle(String title);

        void hideDescription();

        void showDescription(String description);

        void showCompletionStatus(boolean complete);

        void showEditProperty(String propertyId);

        void showPropertyDeleted();

        void showPropertyMarkedComplete();

        void showPropertyMarkedActive();

        void showPropertyOnMap(Double latitude, Double longitude);

        void showPropertyOnMapError();

        boolean isActive();

        void showNoPictures();

        void showPictures(List<Picture> pictures);

        Context getContext();
    }

    interface Presenter extends BasePresenter {

        void editProperty();

        void deleteProperty();

        void completeProperty();

        void activateProperty();

        void openPropertyOnMap();
    }
}