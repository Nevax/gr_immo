/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertydetail;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.R;
import fr.nevax.grimmo.util.ActivityUtils;


/**
 * Displays property details screen.
 */
public class PropertyDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PROPERTY_ID = "PROPERTY_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.propertydetail_act);

        // Set up the toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        // Get the requested property id
        String propertyId = getIntent().getStringExtra(EXTRA_PROPERTY_ID);

        PropertyDetailFragment propertyDetailFragment = (PropertyDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (propertyDetailFragment == null) {
            propertyDetailFragment = PropertyDetailFragment.newInstance(propertyId);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    propertyDetailFragment, R.id.contentFrame);
        }

        // Create the presenter
        new PropertyDetailPresenter(
                propertyId,
                Injection.providePropertiesRepository(getApplicationContext()),
                propertyDetailFragment,
                Injection.provideSchedulerProvider());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}