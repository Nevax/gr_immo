/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertystatistics;


import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface PropertyStatisticsContract {

    interface View extends BaseView<fr.nevax.grimmo.property.propertystatistics.PropertyStatisticsContract.Presenter> {

        void setProgressIndicator(boolean active);

        void showStatistics(int numberOfIncompleteProperties, int numberOfCompletedProperties);

        void showLoadingStatisticsError();

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

    }
}
