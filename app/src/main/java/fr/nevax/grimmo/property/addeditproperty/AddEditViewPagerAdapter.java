/*
 * Created by Antoine GRAVELOT on 20/02/18 23:34
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 20/02/18 23:34
 */

package fr.nevax.grimmo.property.addeditproperty;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class AddEditViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<Integer> mFragmentTitleList = new ArrayList<>();
    private Context context;

    public AddEditViewPagerAdapter(FragmentManager manager, Context nContext) {
        super(manager);
        this.context = nContext;
    }

    @Override
    @Nullable
    public Fragment getItem(int position) {
        if (mFragmentList.size() <= position)
            return null;
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, Integer titleResourceId) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(titleResourceId);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int resourceId = mFragmentTitleList.get(position);
        return context.getString(resourceId);
    }
}
