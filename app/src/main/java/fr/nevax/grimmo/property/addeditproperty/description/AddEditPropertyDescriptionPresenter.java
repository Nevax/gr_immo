/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.description;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyContract;
import fr.nevax.grimmo.util.ValueUtils;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link AddEditPropertyDescriptionFragment}), retrieves the data and updates
 * the UI as required.
 */
public class AddEditPropertyDescriptionPresenter implements AddEditPropertyDescriptionContract.Presenter {

    @NonNull
    private final AddEditPropertyDescriptionContract.View mAddPropertyView;

    @Nullable
    private String mPropertyId;

    private boolean mIsDataMissing;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    /**
     * Creates a presenter for the add/edit view.
     *
     * @param propertyId             ID of the property to edit or null for a new property
     * @param addPropertyView        the add/edit view
     * @param shouldLoadDataFromRepo whether data needs to be loaded or not (for config changes)
     */
    public AddEditPropertyDescriptionPresenter(@Nullable String propertyId,
                                               @NonNull AddEditPropertyDescriptionContract.View addPropertyView,
                                               boolean shouldLoadDataFromRepo,
                                               @NonNull AddEditPropertyContract.Presenter parentPresenter) {
        mPropertyId = propertyId;
        mAddPropertyView = checkNotNull(addPropertyView);
        mIsDataMissing = shouldLoadDataFromRepo;

        AddEditPropertyContract.Presenter mParentPresenter = checkNotNull(parentPresenter, "parent presenter cannot be null");
        mParentPresenter.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
        mAddPropertyView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (!isNewProperty() && mIsDataMissing) {
            // populateProperty();
        }
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public boolean isDataMissing() {
        return mIsDataMissing;
    }

    @Override
    public String getDescription() {
        return mAddPropertyView.getDescription();
    }

    @Override
    public void setDescription(String description) {
        mAddPropertyView.setDescription(description);
    }

    @Override
    public String getPrice() {
        return mAddPropertyView.getPrice();
    }

    @Override
    public void setPrice(Float price) {
        mAddPropertyView.setPrice(price);
    }

    @Override
    public int getSelectedPropertyType() {
        return mAddPropertyView.getSelectedPropertyType();
    }

    @Override
    public void setSelectedPropertyType(int ordinal) {
        mAddPropertyView.setSelectedPropertyType(ordinal);
    }

    @Override
    public Integer getSize() {
        return ValueUtils.parseIntegerOrZero(mAddPropertyView.getSize());
    }

    @Override
    public void setSize(Integer size) {
        mAddPropertyView.setSize(size.toString());
    }

    @Override
    public String getTitle() {
        return mAddPropertyView.getTitle();
    }

    @Override
    public void setTitle(String title) {
        mAddPropertyView.setTitle(title);
    }

    private boolean isNewProperty() {
        return mPropertyId == null;
    }
}
