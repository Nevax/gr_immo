/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.room;

import java.util.List;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;
import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.data.RoomType;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface AddEditPropertyRoomContract {

    interface View extends BaseView<Presenter> {

        boolean isActive();

        void showRoomDialog();

        void showRooms();

        void showNoRooms();
    }

    interface Presenter extends BasePresenter {

        boolean isDataMissing();

        void addRoom(String title, String description, int size, RoomType roomType, List<Picture> pictures, List<Room> nearbyRooms);

        void setAdapter(AddEditPropertyRoomContract.Adapter adapter);

        List<Room> getRooms();

        void addRooms(List<Room> rooms);
    }

    interface Adapter {

        void add(Room room);

        void add(Room room, int position);

        void remove(int position);

        List<Room> getRooms();
    }
}
