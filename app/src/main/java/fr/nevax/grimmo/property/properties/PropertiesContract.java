/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.properties;

import android.support.annotation.NonNull;

import java.util.List;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;
import fr.nevax.grimmo.data.Property;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface PropertiesContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showProperties(List<Property> properties);

        void showAddProperty();

        void showPropertyDetailsUi(String propertyId);

        void showPropertyMarkedComplete();

        void showPropertyMarkedActive();

        void showCompletedPropertiesCleared();

        void showLoadingPropertiesError();

        void showNoProperties();

        void showActiveFilterLabel();

        void showCompletedFilterLabel();

        void showAllFilterLabel();

        void showNoActiveProperties();

        void showNoCompletedProperties();

        void showSuccessfullySavedMessage();

        boolean isActive();

        void showFilteringPopUpMenu();

        void showStatistics();

        void showSettings();
    }

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void loadProperties(boolean forceUpdate);

        void addNewProperty();

        void openPropertyDetails(@NonNull Property requestedProperty);

        void completeProperty(@NonNull Property completedProperty);

        void activateProperty(@NonNull Property activeProperty);

        void clearCompletedProperties();

        PropertiesFilterType getFiltering();

        void setFiltering(PropertiesFilterType requestType);
    }
}
