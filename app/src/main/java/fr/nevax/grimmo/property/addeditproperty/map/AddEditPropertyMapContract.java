/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.map;

import com.google.android.gms.maps.model.LatLng;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface AddEditPropertyMapContract {

    interface View extends BaseView<Presenter> {

        boolean isActive();

        LatLng getLocation();

        void setLocation(LatLng position);
    }

    interface Presenter extends BasePresenter {

        boolean isDataMissing();

        void setLocation(Double latitude, Double longitude);

        LatLng getLocation();
    }
}
