/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.room;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.data.RoomType;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyContract;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link AddEditPropertyRoomFragment}), retrieves the data and updates
 * the UI as required.
 */
public class AddEditPropertyRoomPresenter implements AddEditPropertyRoomContract.Presenter {

    private final String TAG = getClass().getSimpleName();

    @NonNull
    private final AddEditPropertyRoomContract.View mAddPropertyView;

    private AddEditPropertyRoomContract.Adapter mAdapter;

    @Nullable
    private String mPropertyId;

    private boolean mIsDataMissing;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    /**
     * Creates a presenter for the add/edit view.
     *
     * @param propertyId             ID of the property to edit or null for a new property
     * @param addPropertyView        the add/edit view
     * @param shouldLoadDataFromRepo whether data needs to be loaded or not (for config changes)
     */
    public AddEditPropertyRoomPresenter(@Nullable String propertyId,
                                        @NonNull AddEditPropertyRoomContract.View addPropertyView, boolean shouldLoadDataFromRepo,
                                        @NonNull AddEditPropertyContract.Presenter parentPresenter) {
        mPropertyId = propertyId;
        mAddPropertyView = checkNotNull(addPropertyView);
        mIsDataMissing = shouldLoadDataFromRepo;

        AddEditPropertyContract.Presenter mParentPresenter = checkNotNull(parentPresenter, "parent presenter cannot be null");
        mParentPresenter.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
        mAddPropertyView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (!isNewProperty() && mIsDataMissing) {
//            populateRoom();
        }
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public boolean isDataMissing() {
        return mIsDataMissing;
    }

    @Override
    public void addRoom(String title, String description, int size, RoomType roomType, List<Picture> pictures, List<Room> nearbyRooms) {
        mAdapter.add(new Room(null, title, description, size, null, null, null));
    }

    private boolean isNewProperty() {
        return mPropertyId == null;
    }

    public void setAdapter(AddEditPropertyRoomContract.Adapter adapter) {
        this.mAdapter = adapter;
    }

    private void processRooms(@NonNull List<Room> rooms) {
        if (rooms.isEmpty()) {
            // Show a message indicating there are no rooms.
            mAddPropertyView.showNoRooms();
        } else {
            // Show the list of rooms
            mAddPropertyView.showRooms();
        }
    }

    @Override
    public List<Room> getRooms() {
        return mAdapter.getRooms();
    }

    @Override
    public void addRooms(List<Room> rooms) {
        if (!rooms.isEmpty()) {
            for (Room room : rooms) {
                this.addRoom(room.getTitle(), room.getDescription(), room.getSize(), null, null, null);
            }
            processRooms(mAdapter.getRooms());
        }
    }
}
