/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.description;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface AddEditPropertyDescriptionContract {

    interface View extends BaseView<Presenter> {

        void showEmptyPropertyError();

        void showPropertiesList();

        void setSize(Integer size);

        void setPropertyType(String[] propertyTypeList);

        String getTitle();

        void setTitle(String title);

        String getDescription();

        void setDescription(String description);

        String getPrice();

        void setPrice(Float price);

        int getSelectedPropertyType();

        void setSelectedPropertyType(Integer selectedId);

        boolean isActive();

        String getSize();

        void setSize(String size);
    }

    interface Presenter extends BasePresenter {

        boolean isDataMissing();

        String getTitle();

        void setTitle(String title);

        String getDescription();

        void setDescription(String description);

        String getPrice();

        void setPrice(Float price);

        int getSelectedPropertyType();

        void setSelectedPropertyType(int ordinal);

        Integer getSize();

        void setSize(Integer size);
    }
}
