/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.room;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.util.ValueUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Main UI for the add property screen. Users can enter a property title and description.
 */
public class AddEditPropertyRoomFragment extends Fragment implements AddEditPropertyRoomContract.View {

    public static final String ARGUMENT_EDIT_PROPERTY_ID = "EDIT_PROPERTY_ID";
    public final int FRAGMENT_NAME_RESOURCE_ID = R.string.rooms;
    private AddEditPropertyRoomContract.Presenter mPresenter;
    private AddEditPropertyRoomContract.Adapter mAdapter;
    private RecyclerView mRoomsView;
    private View mNoRoomsView;
    private LinearLayoutManager mLayoutManager;
    private String TAG = getClass().getSimpleName();
    private ImageView mNoPropertyIcon;
    private TextView mNoPropertyMainView;
    private TextView mNoPropertyAddView;

    public static AddEditPropertyRoomFragment newInstance() {
        return new AddEditPropertyRoomFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(@NonNull AddEditPropertyRoomContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ImageButton addRoomButton = getActivity().findViewById(R.id.add_new_room);
        addRoomButton.setOnClickListener(v -> showRoomDialog());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.addproperty_room_frag, container, false);

        // Set up  no properties view
        mNoRoomsView = root.findViewById(R.id.noRooms);
        mNoPropertyIcon = root.findViewById(R.id.noRoomsIcon);
        mNoPropertyMainView = root.findViewById(R.id.noRoomsMain);

        mRoomsView = root.findViewById(R.id.rooms_recyclerView);

        mAdapter = new AddEditPropertyRoomAdapter(getContext());
        mLayoutManager = new LinearLayoutManager(getContext());

        mPresenter.setAdapter(mAdapter);
        mRoomsView.setAdapter((RecyclerView.Adapter) mAdapter);
        mRoomsView.setLayoutManager(mLayoutManager);

        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showRoomDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptsView = layoutInflater.inflate(R.layout.dialog_addedit_room, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

        // set prompts.xml to alert dialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText titleInput = promptsView.findViewById(R.id.addedit_new_room_name);
        final EditText descriptionInput = promptsView.findViewById(R.id.addedit_new_room_description);
        final EditText sizeInput = promptsView.findViewById(R.id.addedit_new_room_size);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getString(R.string.add),
                        (dialog, id) -> {
                            final int size = ValueUtils.parseIntegerOrZero(sizeInput.getText().toString());
                            mPresenter.addRooms(Arrays.asList(new Room(null, titleInput.getText().toString(), descriptionInput.getText().toString(), size, null, null, null)));
                        })
                .setNegativeButton(getString(R.string.cancel),
                        (dialog, id) -> dialog.cancel());

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void showRooms() {
        // Hide rooms recyclerview
        mRoomsView.setVisibility(View.VISIBLE);
        // Show no rooms aivailalble
        mNoRoomsView.setVisibility(View.GONE);
    }

    @Override
    public void showNoRooms() {
        showNoPropertiesViews(
                getResources().getString(R.string.no_properties_all),
                R.drawable.ic_assignment_turned_in_24dp,
                false
        );
    }

    private void showNoPropertiesViews(String mainText, int iconRes, boolean showAddView) {
        mRoomsView.setVisibility(View.GONE);
        mNoRoomsView.setVisibility(View.VISIBLE);

        mNoPropertyMainView.setText(mainText);
        mNoPropertyIcon.setImageDrawable(getResources().getDrawable(iconRes));
        mNoPropertyAddView.setVisibility(showAddView ? View.VISIBLE : View.GONE);
    }

    private void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }
}
