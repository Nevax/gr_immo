/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.picture;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fr.nevax.grimmo.BuildConfig;
import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyContract;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link AddEditPropertyPictureFragment}), retrieves the data and updates
 * the UI as required.
 */
public class AddEditPropertyPicturePresenter implements AddEditPropertyPictureContract.Presenter {

    static final int REQUEST_CAMERA = 1;
    static final int SELECT_FILE = 2;

    private final String TAG = getClass().getSimpleName();

    @NonNull
    private final AddEditPropertyPictureContract.View mAddPropertyView;

    @Nullable
    private String mCurrentPhotoPath;

    @Nullable
    private String mOriginalPhotoPath;

    //TODO Set adapter
    private AddEditPropertyPictureContract.Adapter mAdapter;

    @Nullable
    private String mPropertyId;

    private boolean mIsDataMissing;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    /**
     * Creates a presenter for the add/edit view.
     *
     * @param propertyId                ID of the property to edit or null for a new property
     * @param addPropertyView           the add/edit view
     * @param shouldLoadDataFromRepo    whether data needs to be loaded or not (for config changes)
     * @param mAddEditPropertyPresenter parent presenter
     */
    public AddEditPropertyPicturePresenter(@Nullable String propertyId,
                                           @NonNull AddEditPropertyPictureContract.View addPropertyView,
                                           boolean shouldLoadDataFromRepo,
                                           AddEditPropertyContract.Presenter mAddEditPropertyPresenter) {
        mPropertyId = propertyId;
        mAddPropertyView = checkNotNull(addPropertyView);
        mIsDataMissing = shouldLoadDataFromRepo;

        AddEditPropertyContract.Presenter mParentPresenter = checkNotNull(mAddEditPropertyPresenter, "parent presenter cannot be null");
        mParentPresenter.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
        mAddPropertyView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (!isNewProperty() && mIsDataMissing) {
            //populateRooms();
        }
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }


    @Override
    public boolean isDataMissing() {
        return mIsDataMissing;
    }

    private boolean isNewProperty() {
        return mPropertyId == null;
    }

    @Override
    public void dispatchSelectPictureIntent() {
//        InputStream inputStream;
//        try {
//            inputStream = getContext().getContentResolver().openInputStream(data.getData());
//        } catch (FileNotFoundException e) {
//            showCanceledToast();
//            e.printStackTrace();
//        }
//        File image = new File(Environment.getExternalStorageDirectory() + data.getData().getPath());
////            File image = new File(mCurrentPhotoPath);
//        //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
//        Toast.makeText(getContext(), "Image get!!!! " + image.getAbsolutePath(), Toast.LENGTH_LONG).show();
//
//        Log.d("src is ", image.toString() + " exist =  " + image.exists());

        // Create the File where the photo should go

        Intent selectPictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        selectPictureIntent.setType("image/*");
//        startActivityForResult(selectPictureIntent, SELECT_FILE);

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }

        File originalFile = new File(mOriginalPhotoPath);

        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(mAddPropertyView.getFragmentContext(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            selectPictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            mAddPropertyView.startActivityForResult(selectPictureIntent, SELECT_FILE);
        }
    }

    @Override
    public void addPicture(String title, String description) {
        this.addPicture(title, description, mCurrentPhotoPath);
        processPictures(mAdapter.getPictures());
    }

    @Override
    public void addPicture(String title, String description, String filePath) {
        this.addPicture(null, title, description, new File(filePath));
    }

    @Override
    public void addPicture(@Nullable String id, String title, String description, File file) {
//        checkNotNull(file, "file cannot be null");
        if (file != null) {
            if (!file.exists()) {
                Log.e(TAG, "file doesn't exist");
            }
            mAdapter.add(new Picture(id, description, description, file));
        } else
            Log.e(TAG, "photo file is null");
        //TODO Show toast Error
    }

    @Override
    public void addPicture(List<Picture> pictures) {
        if (!pictures.isEmpty()) {
            for (Picture picture : pictures) {
                this.addPicture(picture.getId(), picture.getDescription(), picture.getDescription(), picture.getImageFile());
            }
            processPictures(mAdapter.getPictures());
        }
    }

    @Override
    public void setAdapter(AddEditPropertyPictureContract.Adapter adapter) {
        mAdapter = checkNotNull(adapter);
    }

    @Override
    public void setOriginalPhotoPath(String path) {
        mOriginalPhotoPath = path;
    }

    @Override
    public List<Picture> getPictures() {
        return mAdapter.getPictures();
    }

    @Override
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mAddPropertyView.getFragmentContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mAddPropertyView.getFragmentContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                mAddPropertyView.startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.FRANCE).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mAddPropertyView.getFragmentContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void processPictures(@NonNull List<Picture> pictures) {
        if (pictures.isEmpty()) {
            // Show a message indicating there are no rooms.
            mAddPropertyView.showNoPictures();
        } else {
            // Show the list of rooms
            mAddPropertyView.showPictures();
        }
    }
}
