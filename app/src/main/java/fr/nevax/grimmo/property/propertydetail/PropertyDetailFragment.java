/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertydetail;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Preconditions;
import com.squareup.picasso.Picasso;

import java.util.List;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyActivity;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionFragment;

import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.data.Property.NO_IMAGE;

/**
 * Main UI for the property detail screen.
 */
public class PropertyDetailFragment extends Fragment implements PropertyDetailContract.View {

    @NonNull
    private static final String ARGUMENT_PROPERTY_ID = "PROPERTY_ID";

    @NonNull
    private static final int REQUEST_EDIT_PROPERTY = 1;

    private PropertyDetailContract.Presenter mPresenter;

    private TextView mDetailTitle;

    private TextView mDetailDescription;

    private CheckBox mDetailCompleteStatus;

    private ImageView mPropertyImageCollapsing;

    public static PropertyDetailFragment newInstance(@Nullable String propertyId) {
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_PROPERTY_ID, propertyId);
        PropertyDetailFragment fragment = new PropertyDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.propertydetail_frag, container, false);
        setHasOptionsMenu(true);
        mDetailTitle = root.findViewById(R.id.property_detail_title);
        mDetailDescription = root.findViewById(R.id.property_detail_description);
        mDetailCompleteStatus = root.findViewById(R.id.property_detail_complete);
        mPropertyImageCollapsing = root.findViewById(R.id.property_image_collapsing);

        // Set up floating action button
        FloatingActionButton fab = getActivity().findViewById(R.id.fab_edit_property);
        FloatingActionButton mapFab = root.findViewById(R.id.map_fab);

        fab.setOnClickListener(__ -> mPresenter.editProperty());
        mapFab.setOnClickListener(__ -> mPresenter.openPropertyOnMap());

        return root;
    }

    @Override
    public void setPresenter(@NonNull PropertyDetailContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                mPresenter.deleteProperty();
                return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.propertydetail_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mDetailTitle.setText("");
            mDetailDescription.setText(getString(R.string.loading));
        }
    }

    @Override
    public void hideDescription() {
        mDetailDescription.setVisibility(View.GONE);
    }

    @Override
    public void hideTitle() {
        mDetailTitle.setVisibility(View.GONE);
    }

    @Override
    public void showDescription(@NonNull String description) {
        mDetailDescription.setVisibility(View.VISIBLE);
        mDetailDescription.setText(description);
    }

    @Override
    public void showCompletionStatus(final boolean complete) {
        Preconditions.checkNotNull(mDetailCompleteStatus);

        mDetailCompleteStatus.setChecked(complete);
        mDetailCompleteStatus.setOnCheckedChangeListener(
                (buttonView, isChecked) -> {
                    if (isChecked) {
                        mPresenter.completeProperty();
                    } else {
                        mPresenter.activateProperty();
                    }
                });
    }

    @Override
    public void showEditProperty(@NonNull String propertyId) {
        Intent intent = new Intent(getContext(), AddEditPropertyActivity.class);
        intent.putExtra(AddEditPropertyDescriptionFragment.ARGUMENT_EDIT_PROPERTY_ID, propertyId);
        startActivityForResult(intent, REQUEST_EDIT_PROPERTY);
    }

    @Override
    public void showPropertyDeleted() {
        getActivity().finish();
    }

    public void showPropertyMarkedComplete() {
        Snackbar.make(getView(), getString(R.string.property_marked_complete), Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showPropertyMarkedActive() {
        Snackbar.make(getView(), getString(R.string.property_marked_active), Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showPropertyOnMap(Double latitude, Double longitude) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com").appendPath("maps").appendPath("dir").appendPath("").appendQueryParameter("api", "1")
                .appendQueryParameter("destination", latitude + "," + longitude);
        String url = builder.build().toString();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void showPropertyOnMapError() {
        Snackbar.make(getView(), getString(R.string.property_coordinate_undefined), Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EDIT_PROPERTY) {
            // If the property was edited successfully, go back to the list.
            if (resultCode == Activity.RESULT_OK) {
                getActivity().finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showTitle(@NonNull String title) {
        mDetailTitle.setVisibility(View.VISIBLE);
        mDetailTitle.setText(title);
    }

    @Override
    public void showMissingProperty() {
        mDetailTitle.setText("");
        mDetailDescription.setText(getString(R.string.no_data));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showNoPictures() {
        Picasso.get()
                .load(NO_IMAGE)
                .into(mPropertyImageCollapsing);
    }

    @Override
    public void showPictures(List<Picture> pictures) {
        Picasso.get()
                .load(pictures.get(0).getImageFile())
                .resize(500, 500) // width, height
                .centerCrop()
                .error(R.drawable.ic_photo_camera_black_24dp)
                .into(mPropertyImageCollapsing);
    }
}
