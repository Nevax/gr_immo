package fr.nevax.grimmo.property.addeditproperty;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.PropertyType;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.data.source.PicturesDataSource;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import fr.nevax.grimmo.data.source.RoomsDataSource;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionContract;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionFragment;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapContract;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPictureContract;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomContract;
import fr.nevax.grimmo.util.ValueUtils;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link AddEditPropertyDescriptionFragment}), retrieves the data and updates
 * the UI as required.
 */
public class AddEditPropertyPresenter implements AddEditPropertyContract.Presenter {

    @NonNull
    private final PropertiesDataSource mPropertiesRepository;

    @NonNull
    private final RoomsDataSource mRoomsRepository;

    @NonNull
    private final PicturesDataSource mPicturesRepository;

    @NonNull
    private final AddEditPropertyContract.View mAddPropertyView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;
    private final String TAG = getClass().getSimpleName();
    private AddEditPropertyDescriptionContract.Presenter mAddEditPropertyDescriptionPresenter;
    private AddEditPropertyRoomContract.Presenter mAddEditPropertyRoomPresenter;
    private AddEditPropertyMapContract.Presenter mAddEditPropertyMapPresenter;
    private AddEditPropertyPictureContract.Presenter mAddEditPropertyPicturePresenter;
    @Nullable
    private String mPropertyId;
    private boolean mIsDataMissing;
    @NonNull
    private CompositeDisposable mCompositeDisposable;

    /**
     * Creates a presenter for the add/edit view.
     *
     * @param propertyId             ID of the property to edit or null for a new property
     * @param propertiesRepository   a repository of data for properties
     * @param roomsRepository
     * @param picturesRepository
     * @param addPropertyView        the add/edit view
     * @param shouldLoadDataFromRepo whether data needs to be loaded or not (for config changes)
     */
    public AddEditPropertyPresenter(@Nullable String propertyId, @NonNull PropertiesDataSource propertiesRepository,
                                    @NonNull RoomsDataSource roomsRepository, @NonNull PicturesDataSource picturesRepository, @NonNull AddEditPropertyContract.View addPropertyView, boolean shouldLoadDataFromRepo,
                                    @NonNull BaseSchedulerProvider schedulerProvider) {
        mPropertyId = propertyId;
        mPropertiesRepository = checkNotNull(propertiesRepository);
        mAddPropertyView = checkNotNull(addPropertyView);
        mIsDataMissing = shouldLoadDataFromRepo;

        mRoomsRepository = checkNotNull(roomsRepository);
        mPicturesRepository = checkNotNull(picturesRepository);

        mSchedulerProvider = checkNotNull(schedulerProvider, "schedulerProvider cannot be null!");

        mCompositeDisposable = new CompositeDisposable();
        mAddPropertyView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (!isNewProperty() && mIsDataMissing) {
            populateProperty();
        }
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void saveProperty() {
        Double latitude = null;
        Double longitude = null;

        String title = mAddEditPropertyDescriptionPresenter.getTitle();
        String description = mAddEditPropertyDescriptionPresenter.getDescription();
        String price = mAddEditPropertyDescriptionPresenter.getPrice();
        Integer size = mAddEditPropertyDescriptionPresenter.getSize();
        Integer propertyTypeIndex = mAddEditPropertyDescriptionPresenter.getSelectedPropertyType();

        if (mAddEditPropertyMapPresenter.getLocation() != null) {
            latitude = mAddEditPropertyMapPresenter.getLocation().latitude;
            longitude = mAddEditPropertyMapPresenter.getLocation().longitude;
        }

        Float priceFloat = ValueUtils.parseFloatOrZero(price);

        List<Room> rooms = mAddEditPropertyRoomPresenter.getRooms();
        List<Picture> pictures = mAddEditPropertyPicturePresenter.getPictures();

        if (isNewProperty()) {
            createProperty(title, description, size, priceFloat, propertyTypeIndex, latitude, longitude, rooms, pictures);
        } else {
            updateProperty(title, description, size, priceFloat, propertyTypeIndex, latitude, longitude, rooms, pictures);
        }
    }

    //TODO Only used during tests
    @Override
    public void saveProperty(String title, String description, String size, String price,
                             int propertyTypeIndex) {
        Integer sizeInteger = ValueUtils.parseIntegerOrZero(size);
        Float priceFloat = ValueUtils.parseFloatOrZero(price);
        Double latitudeFloat = ValueUtils.parseDoubleOrZero("0");
        Double longitudeFloat = ValueUtils.parseDoubleOrZero("0");

        if (isNewProperty()) {
            createProperty(title, description, sizeInteger, priceFloat, propertyTypeIndex, latitudeFloat, longitudeFloat, null, null);
        } else {
            updateProperty(title, description, sizeInteger, priceFloat, propertyTypeIndex, latitudeFloat, longitudeFloat, null, null);
        }
    }

    @Override
    public void populateProperty() {
        if (isNewProperty()) {
            throw new RuntimeException("populateProperty() was called but property is new.");
        }
        mCompositeDisposable.add(mPropertiesRepository
                .getProperty(mPropertyId)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        propertyOptional -> {
                            if (propertyOptional.isPresent()) {
                                Property property = propertyOptional.get();
                                if (mAddPropertyView.isActive()) {

                                    List<Room> rooms = property.getRooms();
                                    List<Picture> pictures = property.getPictures();

                                    mAddEditPropertyDescriptionPresenter.setTitle(property.getTitle());
                                    mAddEditPropertyDescriptionPresenter.setDescription(property.getDescription());
                                    mAddEditPropertyDescriptionPresenter.setPrice(property.getPrice());
                                    mAddEditPropertyDescriptionPresenter.setSize(property.getSize());
                                    PropertyType selectedPropertyType = PropertyType.valueOf(property.getPropertyType().name());
                                    mAddEditPropertyDescriptionPresenter.setSelectedPropertyType(selectedPropertyType.ordinal());

                                    mAddEditPropertyRoomPresenter.addRooms(rooms);

                                    if (property.getLatitude() != null || property.getLongitude() != null)
                                        mAddEditPropertyMapPresenter.setLocation(property.getLatitude(), property.getLongitude());

                                    mAddEditPropertyPicturePresenter.addPicture(pictures);

                                    mIsDataMissing = false;
                                }
                            } else {
                                if (mAddPropertyView.isActive()) {
                                    mAddPropertyView.showGetPropertyError();
                                }
                            }
                        },
                        // onError
                        throwable -> {
                            if (mAddPropertyView.isActive()) {
                                mAddPropertyView.showGetPropertyError();
                            }
                        }));
    }

    @Override
    public boolean isDataMissing() {
        return mIsDataMissing;
    }

    @Override
    public void setPresenter(AddEditPropertyDescriptionContract.Presenter addEditPropertyDescriptionPresenter) {
        mAddEditPropertyDescriptionPresenter = addEditPropertyDescriptionPresenter;
    }

    @Override
    public void setPresenter(AddEditPropertyRoomContract.Presenter addEditPropertyRoomPresenter) {
        mAddEditPropertyRoomPresenter = addEditPropertyRoomPresenter;
    }

    @Override
    public void setPresenter(AddEditPropertyMapContract.Presenter addEditPropertyMapPresenter) {
        mAddEditPropertyMapPresenter = addEditPropertyMapPresenter;
    }

    @Override
    public void setPresenter(AddEditPropertyPictureContract.Presenter addEditPropertyPicturePresenter) {
        mAddEditPropertyPicturePresenter = addEditPropertyPicturePresenter;
    }

    private boolean isNewProperty() {
        return mPropertyId == null;
    }

    private void createProperty(String title, String description, Integer size, Float price,
                                Integer propertyTypeIndex, Double latitude, Double longitude, List<Room> rooms, List<Picture> pictures) {
        Property newProperty = new Property(title, description, price);
        newProperty.setSize(size);
        newProperty.setPropertyType(PropertyType.getPropertyTypeFromIndex(propertyTypeIndex));
        newProperty.setLatitude(latitude);
        newProperty.setLongitude(longitude);
        newProperty.setRooms(rooms);
        newProperty.setPictures(pictures);
        if (newProperty.isEmpty()) {
            mAddPropertyView.showEmptyPropertyError();
        } else {
            mPropertiesRepository.saveProperty(newProperty);
            mRoomsRepository.saveRoom(newProperty.getRooms());
            mPicturesRepository.savePicture(newProperty.getPictures());
            mAddPropertyView.showPropertiesList();
        }
    }

    private void updateProperty(String title, String description, Integer size, Float price,
                                Integer propertyTypeIndex, Double latitude, Double longitude, List<Room> rooms, List<Picture> pictures) {
        if (isNewProperty()) {
            throw new RuntimeException("updateProperty() was called but property is new.");
        }
        Property property = new Property(title, description, price, mPropertyId);
        property.setSize(size);
        property.setPropertyType(PropertyType.getPropertyTypeFromIndex(propertyTypeIndex));
        property.setLatitude(latitude);
        property.setLongitude(longitude);
        property.setRooms(rooms);
        property.setPictures(pictures);
        mPropertiesRepository.saveProperty(property);
        mAddPropertyView.showPropertiesList(); // After an edit, go back to the list.
    }
}
