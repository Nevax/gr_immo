/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.picture;

import android.content.Context;
import android.content.Intent;

import java.io.File;
import java.util.List;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;
import fr.nevax.grimmo.data.Picture;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface AddEditPropertyPictureContract {

    interface View extends BaseView<Presenter> {

        boolean isActive();

        void showCanceledToast();

        Context getFragmentContext();

        void startActivityForResult(Intent intent, int requestCode);

        void showPictures();

        void showNoPictures();
    }

    interface Presenter extends BasePresenter {

        boolean isDataMissing();

        void dispatchTakePictureIntent();

        void dispatchSelectPictureIntent();

        void addPicture(String title, String description);

        void addPicture(String title, String description, String filePath);

        void addPicture(String id, String title, String description, File file);

        void addPicture(List<Picture> pictures);

        void setAdapter(Adapter adapter);

        void setOriginalPhotoPath(String path);

        List<Picture> getPictures();
    }

    interface Adapter {

        void add(Picture picture);

        void add(Picture property, int position);

        void remove(int position);

        List<Picture> getPictures();
    }
}
