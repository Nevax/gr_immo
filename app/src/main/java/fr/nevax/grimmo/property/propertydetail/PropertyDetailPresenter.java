/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertydetail;


import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link PropertyDetailFragment}), retrieves the data and updates
 * the UI as required.
 */
public class PropertyDetailPresenter implements PropertyDetailContract.Presenter {

    @NonNull
    private final PropertiesRepository mPropertiesRepository;

    @NonNull
    private final PropertyDetailContract.View mPropertyDetailView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @Nullable
    private String mPropertyId;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    public PropertyDetailPresenter(@Nullable String propertyId,
                                   @NonNull PropertiesRepository propertiesRepository,
                                   @NonNull PropertyDetailContract.View propertyDetailView,
                                   @NonNull BaseSchedulerProvider schedulerProvider) {
        this.mPropertyId = propertyId;
        mPropertiesRepository = checkNotNull(propertiesRepository, "propertiesRepository cannot be null!");
        mPropertyDetailView = checkNotNull(propertyDetailView, "propertyDetailView cannot be null!");
        mSchedulerProvider = checkNotNull(schedulerProvider, "schedulerProvider cannot be null");

        mCompositeDisposable = new CompositeDisposable();
        mPropertyDetailView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        openProperty();
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    private void openProperty() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }

        mPropertyDetailView.setLoadingIndicator(true);
        mCompositeDisposable.add(mPropertiesRepository
                .getProperty(mPropertyId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        this::showProperty,
                        // onError
                        throwable -> {
                        },
                        // onCompleted
                        () -> mPropertyDetailView.setLoadingIndicator(false)));
    }

    @Override
    public void openPropertyOnMap() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }

        mPropertyDetailView.setLoadingIndicator(true);
        mCompositeDisposable.add(mPropertiesRepository
                .getProperty(mPropertyId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        this::showPropertyMap,
                        // onError
                        throwable -> {
                        },
                        // onCompleted
                        () -> mPropertyDetailView.setLoadingIndicator(false)));
    }

    @Override
    public void editProperty() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }
        mPropertyDetailView.showEditProperty(mPropertyId);
    }

    @Override
    public void deleteProperty() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }
        mPropertiesRepository.deleteProperty(mPropertyId);
        mPropertyDetailView.showPropertyDeleted();
    }

    @Override
    public void completeProperty() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }
        mPropertiesRepository.completeProperty(mPropertyId);
        mPropertyDetailView.showPropertyMarkedComplete();
    }

    @Override
    public void activateProperty() {
        if (Strings.isNullOrEmpty(mPropertyId)) {
            mPropertyDetailView.showMissingProperty();
            return;
        }
        mPropertiesRepository.activateProperty(mPropertyId);
        mPropertyDetailView.showPropertyMarkedActive();
    }

    private void showProperty(@NonNull Property property) {
        String title = property.getTitle();
        String description = property.getDescription();
        List<Picture> pictures = property.getPictures();

        Double latitude = property.getLatitude();
        Double longitude = property.getLongitude();

        if (latitude != null && longitude != null) {
            Geocoder geocoder = new Geocoder(mPropertyDetailView.getContext(), Locale.getDefault());
            try {
                Address addresses = geocoder.getFromLocation(latitude, longitude, 1).get(0);
                String address = addresses.getAddressLine(0);

                description += "\n" + address;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (Strings.isNullOrEmpty(title)) {
            mPropertyDetailView.hideTitle();
        } else {
            mPropertyDetailView.showTitle(title);
        }

        if (Strings.isNullOrEmpty(description)) {
            mPropertyDetailView.hideDescription();
        } else {
            mPropertyDetailView.showDescription(description);
        }

        if (pictures.isEmpty()) {
            mPropertyDetailView.showNoPictures();
        } else {
            mPropertyDetailView.showPictures(pictures);
        }

        mPropertyDetailView.showCompletionStatus(property.isCompleted());
    }

    protected void showPropertyMap(@NonNull Property property) {
        if (property.getLatitude() != null || property.getLongitude() != null) {
            Double latitude = property.getLatitude();
            Double longitude = property.getLongitude();

            if (latitude == 0d && longitude == 0d)
                mPropertyDetailView.showPropertyOnMapError();
            else
                mPropertyDetailView.showPropertyOnMap(latitude, longitude);
        } else {
            mPropertyDetailView.showPropertyOnMapError();
        }
    }
}
