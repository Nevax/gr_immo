package fr.nevax.grimmo.property.addeditproperty.picture;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.Picture;

public class AddEditPropertyPictureAdapter extends RecyclerView.Adapter<AddEditPropertyPictureAdapter.SimpleViewHolder> implements AddEditPropertyPictureContract.Adapter {

    private final String TAG = getClass().getSimpleName();

    @NonNull
    private final Context mContext;

    @NonNull
    private List<Picture> mData;

    public AddEditPropertyPictureAdapter(Context context) {
        this(context, null);
    }

    public AddEditPropertyPictureAdapter(Context context, Picture[] data) {
        mContext = context;
        if (data != null)
            mData = new ArrayList<>(Arrays.asList(data));
        else
            mData = new ArrayList<>();
    }

    @Override
    public void add(Picture picture) {
        this.add(picture, -1);
    }

    @Override
    public void add(Picture picture, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, picture);
        notifyItemInserted(position);
    }

    @Override
    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public List<Picture> getPictures() {
        return mData;
    }

    @NonNull
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.picture_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, final int position) {
        final Picture picture = mData.get(position);

        Picasso.get()
                .load(picture.getImageFile())
                .fit()
                .into(holder.imageView);

//        holder.titleTextView.setText(picture.getDescription());
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        //        public final TextView titleTextView;
        public final ImageView imageView;

        public SimpleViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.property_picture);
//            titleTextView = view.findViewById(R.id.image_title);
        }
    }
}