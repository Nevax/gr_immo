/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.picture;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import fr.nevax.grimmo.R;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPicturePresenter.REQUEST_CAMERA;
import static fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPicturePresenter.SELECT_FILE;

/**
 * Main UI for the add property screen. Users can enter a property title and description.
 */
public class AddEditPropertyPictureFragment extends Fragment implements AddEditPropertyPictureContract.View {

    public static final String ARGUMENT_EDIT_PROPERTY_ID = "EDIT_PROPERTY_ID";

    public final int FRAGMENT_NAME_RESOURCE_ID = R.string.pictures;
    private final String TAG = getClass().getSimpleName();

    private ImageView imageView;
    private AddEditPropertyPictureContract.Presenter mPresenter;
    private AddEditPropertyPictureContract.Adapter mAdapter;
    private RecyclerView mPicturesView;
    private LinearLayoutManager mLayoutManager;
    private View mNoPicturesView;
    private ImageView mNoPictureIcon;
    private TextView mNoPictureMainView;
    private TextView mNoPropertyAddView;

    public static AddEditPropertyPictureFragment newInstance() {
        return new AddEditPropertyPictureFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(@NonNull AddEditPropertyPictureContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.addproperty_picture_frag, container, false);

        ImageButton addPictureButton = root.findViewById(R.id.add_new_picture);
        mPicturesView = root.findViewById(R.id.pictures_recyclerView);
        mPicturesView.setHasFixedSize(true);
        mPicturesView.setItemViewCacheSize(20);
        mPicturesView.setDrawingCacheEnabled(true);
        mPicturesView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // Set up  no properties view
        mNoPicturesView = root.findViewById(R.id.noPictures);
        mNoPictureIcon = root.findViewById(R.id.noPicturesIcon);
        mNoPictureMainView = root.findViewById(R.id.noPicturesMain);


        mAdapter = new AddEditPropertyPictureAdapter(getContext());
        mPresenter.setAdapter(mAdapter);

        mLayoutManager = new GridLayoutManager(getContext(), 2);

        mPicturesView.setAdapter((RecyclerView.Adapter) mAdapter);
        mPicturesView.setLayoutManager(mLayoutManager);

        addPictureButton.setOnClickListener(v -> pickImageFromSelectedSource());

        setHasOptionsMenu(true);
        return root;
    }

    public void pickImageFromSelectedSource() {
        final CharSequence[] items = {getString(R.string.take_picture), getString(R.string.cancel)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(getString(R.string.take_picture))) {
                mPresenter.dispatchTakePictureIntent();
            } else if (items[item].equals(getString(R.string.select_galery))) {
                mPresenter.dispatchSelectPictureIntent();
            } else if (items[item].equals(getString(R.string.select_galery))) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            showCanceledToast();
        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            // Picture selected from galery
            if (data == null)
                showCanceledToast();
            else
                mPresenter.addPicture("", data.getData().getPath());

        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            // Picture selected from camera
            mPresenter.addPicture("", "");
        }
    }

    @Override
    public void showCanceledToast() {
        Toast.makeText(getContext(), "CANCELED ", Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getFragmentContext() {
        return getContext();
    }

    @Override
    public void showPictures() {
        // Hide rooms recycler view
        mPicturesView.setVisibility(View.VISIBLE);
        // Show no rooms available
        mNoPicturesView.setVisibility(View.GONE);
    }

    @Override
    public void showNoPictures() {
        showNoPicturesViews(
                getResources().getString(R.string.no_pictures_all),
                R.drawable.ic_assignment_turned_in_24dp,
                false
        );
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    private void showNoPicturesViews(String mainText, int iconRes, boolean showAddView) {
        mPicturesView.setVisibility(View.GONE);
        mNoPicturesView.setVisibility(View.VISIBLE);

        mNoPictureMainView.setText(mainText);
        mNoPictureIcon.setImageDrawable(getResources().getDrawable(iconRes));
        mNoPropertyAddView.setVisibility(showAddView ? View.VISIBLE : View.GONE);
    }
}
