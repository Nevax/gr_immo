package fr.nevax.grimmo.property.addeditproperty.room;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.Room;

public class AddEditPropertyRoomAdapter extends RecyclerView.Adapter<AddEditPropertyRoomAdapter.SimpleViewHolder> implements AddEditPropertyRoomContract.Adapter {

    private final String TAG = getClass().getSimpleName();
    private final Context mContext;
    private List<Room> mData;

    public AddEditPropertyRoomAdapter(Context context) {
        this(context, null);
    }

    public AddEditPropertyRoomAdapter(Context context, Room[] data) {
        mContext = context;
        if (data != null)
            mData = new ArrayList<>(Arrays.asList(data));
        else
            mData = new ArrayList<>();
    }

    @Override
    public void add(Room room) {
        this.add(room, -1);
    }

    @Override
    public void add(Room room, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, room);
        notifyItemInserted(position);
    }

    @Override
    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public List<Room> getRooms() {
        return mData;
    }

    @NonNull
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.room_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, final int position) {
        final Room room = mData.get(position);

        holder.title.setText(room.getTitle());
        holder.description.setText(room.getDescription());
        holder.size.setText(room.getSize() + " m2");
        holder.title.setOnClickListener(view -> Toast.makeText(mContext, "Position =" + position, Toast.LENGTH_SHORT).show());
    }

    public void replaceData(List<Room> rooms) {
        mData = rooms;
        notifyDataSetChanged();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView description;
        public final TextView size;

        public SimpleViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            size = view.findViewById(R.id.room_size);
        }
    }
}