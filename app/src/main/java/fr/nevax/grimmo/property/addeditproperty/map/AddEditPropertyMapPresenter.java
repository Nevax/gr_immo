/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.map;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyContract;
import io.reactivex.disposables.CompositeDisposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link AddEditPropertyMapPresenter}), retrieves the data and updates
 * the UI as required.
 */
public class AddEditPropertyMapPresenter implements AddEditPropertyMapContract.Presenter {

    @NonNull
    private final AddEditPropertyMapContract.View mAddPropertyView;

    @Nullable
    private String mPropertyId;

    private boolean mIsDataMissing;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    /**
     * Creates a presenter for the add/edit view.
     *
     * @param propertyId                ID of the property to edit or null for a new property
     * @param addPropertyView           the add/edit view
     * @param shouldLoadDataFromRepo    whether data needs to be loaded or not (for config changes)
     * @param mAddEditPropertyPresenter parent presenter
     */
    public AddEditPropertyMapPresenter(@Nullable String propertyId,
                                       @NonNull AddEditPropertyMapContract.View addPropertyView,
                                       boolean shouldLoadDataFromRepo,
                                       AddEditPropertyContract.Presenter mAddEditPropertyPresenter) {
        mPropertyId = propertyId;
        mAddPropertyView = checkNotNull(addPropertyView);
        mIsDataMissing = shouldLoadDataFromRepo;

        AddEditPropertyContract.Presenter mParentPresenter = checkNotNull(mAddEditPropertyPresenter, "parent presenter cannot be null");
        mParentPresenter.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
        mAddPropertyView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (!isNewProperty() && mIsDataMissing) {
//            populateProperty();
        }
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }


    @Override
    public boolean isDataMissing() {
        return mIsDataMissing;
    }

    @Override
    public void setLocation(Double latitude, Double longitude) {
        mAddPropertyView.setLocation(new LatLng(latitude, longitude));
    }

    @Override
    public LatLng getLocation() {
        return mAddPropertyView.getLocation();
    }

    private boolean isNewProperty() {
        return mPropertyId == null;
    }

}
