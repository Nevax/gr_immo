/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import fr.nevax.grimmo.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Main UI for the add property screen. Users can enter a property title and description.
 */
public class AddEditPropertyMapFragment extends Fragment implements AddEditPropertyMapContract.View, OnMapReadyCallback {

    public static final String ARGUMENT_EDIT_PROPERTY_ID = "EDIT_PROPERTY_ID";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public final int FRAGMENT_NAME_RESOURCE_ID = R.string.map;
    private AddEditPropertyMapContract.Presenter mPresenter;
    private GoogleMap googleMap;

    private LatLng position;
    private String TAG = getClass().getSimpleName();

    public static AddEditPropertyMapFragment newInstance() {
        return new AddEditPropertyMapFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(@NonNull AddEditPropertyMapContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.addproperty_map_frag, container, false);

        MapView mMapView = root.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(mMap -> {
            googleMap = mMap;

            // For showing a move to my location button
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                return;
            }
            googleMap.setMyLocationEnabled(true);

            if (position != null)
                addMarkerToMap(position);

            googleMap.setOnMapClickListener(point -> {
                this.position = point;
                addMarkerToMap(point);
            });
        });

        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public LatLng getLocation() {
        return position;
    }

    @Override
    public void setLocation(LatLng position) {
        this.position = position;
    }

    private void addMarkerToMap(@NonNull LatLng position) {
        checkNotNull(position, "position cannot be null");
        googleMap.clear();
        // For zooming automatically to the location of the marker
        googleMap.addMarker(new MarkerOptions().position(position).title("Position du bien").snippet("Position du bien"));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom((float) 5.5).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap = googleMap;
    }
}
