/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.properties;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.IdlingResource;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.R;
import fr.nevax.grimmo.task.tasks.TasksActivity;
import fr.nevax.grimmo.util.ActivityUtils;
import fr.nevax.grimmo.util.EspressoIdlingResource;

public class PropertiesActivity extends AppCompatActivity {

    private static final String CURRENT_FILTERING_KEY = "CURRENT_FILTERING_KEY";

    private DrawerLayout mDrawerLayout;

    private PropertiesPresenter mPropertiesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.properties_act);

        // Set up the toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        // Set up the navigation drawer.
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        NavigationView navigationView = findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        PropertiesFragment propertiesFragment =
                (PropertiesFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (propertiesFragment == null) {
            // Create the fragment
            propertiesFragment = PropertiesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), propertiesFragment, R.id.contentFrame);
        }

        // Create the presenter
        mPropertiesPresenter = new PropertiesPresenter(
                Injection.providePropertiesRepository(getApplicationContext()),
                propertiesFragment,
                Injection.provideSchedulerProvider());

        // Load previously saved state, if available.
        if (savedInstanceState != null) {
            PropertiesFilterType currentFiltering =
                    (PropertiesFilterType) savedInstanceState.getSerializable(CURRENT_FILTERING_KEY);
            mPropertiesPresenter.setFiltering(currentFiltering);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(CURRENT_FILTERING_KEY, mPropertiesPresenter.getFiltering());

        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Open the navigation drawer when the home icon is selected from the toolbar.
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.property_navigation_menu_item:
                            // Do nothing because we are already here
                            break;
                        case R.id.task_navigation_menu_item:
                            Intent intentToProperties =
                                    new Intent(PropertiesActivity.this, TasksActivity.class);
                            startActivity(intentToProperties);
                            break;
                        default:
                            break;
                    }
                    // Close the navigation drawer when an item is selected.
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                });
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
