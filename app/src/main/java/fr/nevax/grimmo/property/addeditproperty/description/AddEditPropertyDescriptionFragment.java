/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty.description;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import fr.nevax.grimmo.R;
import fr.nevax.grimmo.data.PropertyType;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Main UI for the add property screen. Users can enter a property title and description.
 */
public class AddEditPropertyDescriptionFragment extends Fragment implements AddEditPropertyDescriptionContract.View {

    public static final String ARGUMENT_EDIT_PROPERTY_ID = "EDIT_PROPERTY_ID";
    public final int FRAGMENT_NAME_RESOURCE_ID = R.string.description_tab_name;
    private AddEditPropertyDescriptionContract.Presenter mPresenter;

    private TextInputLayout mTitle;

    private TextInputLayout mPrice;

    private TextInputLayout mSize;

    private Spinner mPropertyType;

    private TextInputLayout mDescription;

    public static AddEditPropertyDescriptionFragment newInstance() {
        return new AddEditPropertyDescriptionFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(@NonNull AddEditPropertyDescriptionContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.addproperty_description_frag, container, false);
        mTitle = root.findViewById(R.id.add_property_title);
        mPrice = root.findViewById(R.id.add_property_price);
        mSize = root.findViewById(R.id.add_property_size);
        mPropertyType = root.findViewById(R.id.add_property_type);
        mDescription = root.findViewById(R.id.add_property_description);

        // Set content of the spinner
        setPropertyType(PropertyType.getStringList(getContext()));

        // Reset errors
        mTitle.setError(null);
        mPrice.setError(null);
        mSize.setError(null);
        mDescription.setError(null);

        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void showEmptyPropertyError() {
        mTitle.setError(getString(R.string.empty_property_message));
        Snackbar.make(mTitle, getString(R.string.empty_property_message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPropertiesList() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public String getDescription() {
        return mDescription.getEditText().getText().toString();
    }

    @Override
    public void setDescription(String description) {
        mDescription.getEditText().setText(description);
    }

    @Override
    public String getPrice() {
        return mPrice.getEditText().getText().toString();
    }

    @Override
    public void setPrice(Float price) {
        mPrice.getEditText().setText(String.format("%.2f", price));
    }

    @Override
    public int getSelectedPropertyType() {
        return mPropertyType.getSelectedItemPosition();
    }

    @Override
    public void setSelectedPropertyType(Integer selectedId) {
        mPropertyType.setSelection(selectedId);
    }

    @Override
    public void setSize(Integer size) {
        mSize.getEditText().setText(String.valueOf(size));
    }

    @Override
    public void setPropertyType(String[] propertyTypeList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, propertyTypeList);
        mPropertyType.setAdapter(adapter);
    }

    @Override
    public String getTitle() {
        return mTitle.getEditText().getText().toString();
    }

    @Override
    public void setTitle(String title) {
        mTitle.getEditText().setText(title);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public String getSize() {
        return mSize.getEditText().getText().toString();
    }

    @Override
    public void setSize(String size) {
        mSize.getEditText().setText(size);
    }
}
