/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.addeditproperty;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.List;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.R;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionFragment;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionPresenter;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapFragment;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapPresenter;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPictureFragment;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPicturePresenter;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomFragment;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomPresenter;
import fr.nevax.grimmo.util.EspressoIdlingResource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Displays an add or edit property screen.
 */
public class AddEditPropertyActivity extends AppCompatActivity implements AddEditPropertyContract.View {

    public static final int REQUEST_ADD_PROPERTY = 1;

    public static final String SHOULD_LOAD_DATA_FROM_REPO_KEY = "SHOULD_LOAD_DATA_FROM_REPO_KEY";
    private static final String MAP_LATITUDE_MARKER = "MAP_LATITUDE_MARKER";
    private static final String MAP_LONGITUDE_MARKER = "MAP_LATITUDE_MARKER";
    private static final String MAP_MARKER_SET = "MAP_MARKER_SET";
    private final String TAG = getClass().getSimpleName();

    private ActionBar mActionBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AddEditViewPagerAdapter viewPagerAdapter;

    private AddEditPropertyContract.Presenter mAddEditPropertyPresenter;
    private AddEditPropertyDescriptionPresenter mAddEditPropertyDescriptionPresenter;
    private AddEditPropertyRoomPresenter mAddEditPropertyRoomPresenter;
    private AddEditPropertyMapPresenter mAddEditPropertyMapPresenter;
    private AddEditPropertyPicturePresenter mAddEditPropertyPicturePresenter;

    private AddEditPropertyDescriptionFragment mAddEditPropertyDescriptionFragment;
    private AddEditPropertyRoomFragment mAddEditPropertyRoomFragment;
    private AddEditPropertyMapFragment mAddEditPropertyMapFragment;
    private AddEditPropertyPictureFragment mAddEditPropertyPictureFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addproperty_act);

        // Set up the toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);

        String propertyId = getIntent().getStringExtra(AddEditPropertyDescriptionFragment.ARGUMENT_EDIT_PROPERTY_ID);

        setToolbarTitle(propertyId);

        List<Fragment> fragments = getSupportFragmentManager().getFragments();

        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    if (fragment instanceof AddEditPropertyDescriptionFragment) {
                        mAddEditPropertyDescriptionFragment = (AddEditPropertyDescriptionFragment) fragment;
                    } else if (fragment instanceof AddEditPropertyRoomFragment) {
                        mAddEditPropertyRoomFragment = (AddEditPropertyRoomFragment) fragment;
                    } else if (fragment instanceof AddEditPropertyMapFragment) {
                        mAddEditPropertyMapFragment = (AddEditPropertyMapFragment) fragment;
                    } else if (fragment instanceof AddEditPropertyPictureFragment) {
                        mAddEditPropertyPictureFragment = (AddEditPropertyPictureFragment) fragment;
                    }
                }
            }
        }

        if (mAddEditPropertyDescriptionFragment == null)
            mAddEditPropertyDescriptionFragment = AddEditPropertyDescriptionFragment.newInstance();

        if (mAddEditPropertyRoomFragment == null)
            mAddEditPropertyRoomFragment = AddEditPropertyRoomFragment.newInstance();

        if (mAddEditPropertyMapFragment == null)
            mAddEditPropertyMapFragment = AddEditPropertyMapFragment.newInstance();

        if (mAddEditPropertyPictureFragment == null)
            mAddEditPropertyPictureFragment = AddEditPropertyPictureFragment.newInstance();


        // Set up view pager with tab layout
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        viewPagerAdapter = new AddEditViewPagerAdapter(getSupportFragmentManager(), AddEditPropertyActivity.this);
        viewPagerAdapter.addFragment(mAddEditPropertyDescriptionFragment, mAddEditPropertyDescriptionFragment.FRAGMENT_NAME_RESOURCE_ID);
        viewPagerAdapter.addFragment(mAddEditPropertyRoomFragment, mAddEditPropertyRoomFragment.FRAGMENT_NAME_RESOURCE_ID);
        viewPagerAdapter.addFragment(mAddEditPropertyMapFragment, mAddEditPropertyMapFragment.FRAGMENT_NAME_RESOURCE_ID);
        viewPagerAdapter.addFragment(mAddEditPropertyPictureFragment, mAddEditPropertyPictureFragment.FRAGMENT_NAME_RESOURCE_ID);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(4);

        // This is required to avoid a black flash when the map is loaded.  The flash is due
        // to the use of a SurfaceView as the underlying view of the map.
        viewPager.requestTransparentRegion(viewPager);

        boolean shouldLoadDataFromRepo = true;

        // Prevent the presenter from loading data from the repository if this is a config change.
        if (savedInstanceState != null) {
            // Data might not have loaded when the config change happen, so we saved the state.
            shouldLoadDataFromRepo = savedInstanceState.getBoolean(SHOULD_LOAD_DATA_FROM_REPO_KEY);
        }

        // Create the presenter
        mAddEditPropertyPresenter = new AddEditPropertyPresenter(
                propertyId,
                Injection.providePropertiesRepository(getApplicationContext()),
                Injection.provideRoomsRepository(getApplicationContext()),
                Injection.providePicturesRepository(getApplicationContext()),
                AddEditPropertyActivity.this,
                shouldLoadDataFromRepo,
                Injection.provideSchedulerProvider());

        // Create the presenter
        mAddEditPropertyDescriptionPresenter = new AddEditPropertyDescriptionPresenter(
                propertyId,
                mAddEditPropertyDescriptionFragment,
                shouldLoadDataFromRepo,
                mAddEditPropertyPresenter);

        // Create the presenter
        mAddEditPropertyRoomPresenter = new AddEditPropertyRoomPresenter(
                propertyId,
                mAddEditPropertyRoomFragment,
                shouldLoadDataFromRepo,
                mAddEditPropertyPresenter);

        // Create the presenter
        mAddEditPropertyMapPresenter = new AddEditPropertyMapPresenter(
                propertyId,
                mAddEditPropertyMapFragment,
                shouldLoadDataFromRepo,
                mAddEditPropertyPresenter);

        // Create the presenter
        mAddEditPropertyPicturePresenter = new AddEditPropertyPicturePresenter(
                propertyId,
                mAddEditPropertyPictureFragment,
                shouldLoadDataFromRepo,
                mAddEditPropertyPresenter);

        if (savedInstanceState != null) {
            // If LATITUDE and LONGITUDE are set in bundle, set them
            boolean mapMarkerSet = savedInstanceState.getBoolean(MAP_MARKER_SET);
            if (mapMarkerSet) {
                Double latitude = savedInstanceState.getDouble(MAP_LATITUDE_MARKER);
                Double longitude = savedInstanceState.getDouble(MAP_LONGITUDE_MARKER);
                mAddEditPropertyMapPresenter.setLocation(latitude, longitude);
            }
        }

        FloatingActionButton fab = findViewById(R.id.fab_edit_property_done);
        fab.setImageResource(R.drawable.ic_done);
        fab.setOnClickListener(v -> mAddEditPropertyPresenter.saveProperty());
    }

    private void setToolbarTitle(@Nullable String propertyId) {
        if (propertyId == null) {
            mActionBar.setTitle(R.string.add_property);
        } else {
            mActionBar.setTitle(R.string.edit_property);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the state so that next time we know if we need to refresh data.
        outState.putBoolean(SHOULD_LOAD_DATA_FROM_REPO_KEY, mAddEditPropertyDescriptionPresenter.isDataMissing());
        outState.putBoolean(SHOULD_LOAD_DATA_FROM_REPO_KEY, mAddEditPropertyRoomPresenter.isDataMissing());
        outState.putBoolean(SHOULD_LOAD_DATA_FROM_REPO_KEY, mAddEditPropertyMapPresenter.isDataMissing());
        outState.putBoolean(SHOULD_LOAD_DATA_FROM_REPO_KEY, mAddEditPropertyPicturePresenter.isDataMissing());
        if (mAddEditPropertyMapPresenter.getLocation() != null) {
            outState.putBoolean(MAP_MARKER_SET, true);
            outState.putDouble(MAP_LATITUDE_MARKER, mAddEditPropertyMapPresenter.getLocation().latitude);
            outState.putDouble(MAP_LONGITUDE_MARKER, mAddEditPropertyMapPresenter.getLocation().longitude);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void setPresenter(AddEditPropertyContract.Presenter presenter) {
        mAddEditPropertyPresenter = checkNotNull(presenter);
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void showPropertiesList() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void showEmptyPropertyError() {
        viewPager.setCurrentItem(0);
        mAddEditPropertyDescriptionFragment.showEmptyPropertyError();
    }

    @Override
    public void showGetPropertyError() {
        Snackbar.make(getCurrentFocus(), R.string.error_get_property_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAddEditPropertyPresenter.subscribe();
        mAddEditPropertyDescriptionPresenter.subscribe();
        mAddEditPropertyRoomPresenter.subscribe();
        mAddEditPropertyMapPresenter.subscribe();
        mAddEditPropertyPicturePresenter.subscribe();

    }

    @Override
    public void onPause() {
        super.onPause();
        mAddEditPropertyPresenter.unsubscribe();
        mAddEditPropertyDescriptionPresenter.unsubscribe();
        mAddEditPropertyRoomPresenter.unsubscribe();
        mAddEditPropertyMapPresenter.unsubscribe();
        mAddEditPropertyPicturePresenter.unsubscribe();
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

}
