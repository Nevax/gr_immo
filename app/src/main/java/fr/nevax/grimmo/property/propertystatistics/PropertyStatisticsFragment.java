/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.propertystatistics;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fr.nevax.grimmo.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Main UI for the statistics screen.
 */
public class PropertyStatisticsFragment extends Fragment implements PropertyStatisticsContract.View {

    private TextView mStatisticsTV;

    private PropertyStatisticsContract.Presenter mPresenter;

    public static fr.nevax.grimmo.property.propertystatistics.PropertyStatisticsFragment newInstance() {
        return new fr.nevax.grimmo.property.propertystatistics.PropertyStatisticsFragment();
    }

    @Override
    public void setPresenter(@NonNull PropertyStatisticsContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.property_statistics_frag, container, false);
        mStatisticsTV = root.findViewById(R.id.property_statistics);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (active) {
            mStatisticsTV.setText(getString(R.string.loading));
        }
    }

    @Override
    public void showStatistics(int numberOfIncompleteProperties, int numberOfCompletedProperties) {
        if (numberOfCompletedProperties == 0 && numberOfIncompleteProperties == 0) {
            mStatisticsTV.setText(getResources().getString(R.string.property_statistics_no_properties));
        } else {
            String displayString = getResources().getString(R.string.property_statistics_active_properties) + " "
                    + numberOfIncompleteProperties + "\n" + getResources().getString(
                    R.string.property_statistics_completed_properties) + " " + numberOfCompletedProperties;
            mStatisticsTV.setText(displayString);
        }
    }

    @Override
    public void showLoadingStatisticsError() {
        mStatisticsTV.setText(getResources().getString(R.string.property_statistics_error));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }
}