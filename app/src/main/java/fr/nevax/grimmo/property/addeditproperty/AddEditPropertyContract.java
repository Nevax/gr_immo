package fr.nevax.grimmo.property.addeditproperty;

import fr.nevax.grimmo.BasePresenter;
import fr.nevax.grimmo.BaseView;
import fr.nevax.grimmo.property.addeditproperty.description.AddEditPropertyDescriptionContract;
import fr.nevax.grimmo.property.addeditproperty.map.AddEditPropertyMapContract;
import fr.nevax.grimmo.property.addeditproperty.picture.AddEditPropertyPictureContract;
import fr.nevax.grimmo.property.addeditproperty.room.AddEditPropertyRoomContract;

public interface AddEditPropertyContract {

    interface Presenter extends BasePresenter {

        void saveProperty();

        void saveProperty(String title, String description, String size, String price, int propertyTypeIndex);

        void populateProperty();

        boolean isDataMissing();

        void setPresenter(AddEditPropertyDescriptionContract.Presenter addEditPropertyDescriptionPresenter);

        void setPresenter(AddEditPropertyRoomContract.Presenter addEditPropertyDescriptionPresenter);

        void setPresenter(AddEditPropertyMapContract.Presenter addEditPropertyDescriptionPresenter);

        void setPresenter(AddEditPropertyPictureContract.Presenter addEditPropertyDescriptionPresenter);
    }

    interface View extends BaseView<Presenter> {

        boolean isActive();

        void showPropertiesList();

        void showEmptyPropertyError();

        void showGetPropertyError();
    }
}
