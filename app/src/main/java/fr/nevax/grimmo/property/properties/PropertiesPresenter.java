/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.property.properties;

import android.app.Activity;
import android.support.annotation.NonNull;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.property.addeditproperty.AddEditPropertyActivity;
import fr.nevax.grimmo.util.EspressoIdlingResource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link PropertiesFragment}), retrieves the data and updates the
 * UI as required.
 */
public class PropertiesPresenter implements PropertiesContract.Presenter {

    @NonNull
    private final PropertiesRepository mPropertiesRepository;

    @NonNull
    private final PropertiesContract.View mPropertiesView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private PropertiesFilterType mCurrentFiltering = PropertiesFilterType.ALL_PROPERTIES;

    private boolean mFirstLoad = true;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    public PropertiesPresenter(@NonNull PropertiesRepository propertiesRepository,
                               @NonNull PropertiesContract.View propertiesView,
                               @NonNull BaseSchedulerProvider schedulerProvider) {
        mPropertiesRepository = checkNotNull(propertiesRepository, "propertiesRepository cannot be null");
        mPropertiesView = checkNotNull(propertiesView, "propertiesView cannot be null!");
        mSchedulerProvider = checkNotNull(schedulerProvider, "schedulerProvider cannot be null");

        mCompositeDisposable = new CompositeDisposable();
        mPropertiesView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        loadProperties(false);
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void result(int requestCode, int resultCode) {
        // If a property was successfully added, show snackbar
        if (AddEditPropertyActivity.REQUEST_ADD_PROPERTY == requestCode && Activity.RESULT_OK == resultCode) {
            mPropertiesView.showSuccessfullySavedMessage();
        }
    }

    @Override
    public void loadProperties(boolean forceUpdate) {
        // Simplification for sample: a network reload will be forced on first load.
        loadProperties(forceUpdate || mFirstLoad, true);
        mFirstLoad = false;
    }

    /**
     * @param forceUpdate   Pass in true to refresh the data in the {@link PropertiesDataSource}
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    private void loadProperties(final boolean forceUpdate, final boolean showLoadingUI) {
        if (showLoadingUI) {
            mPropertiesView.setLoadingIndicator(true);
        }
        if (forceUpdate) {
            mPropertiesRepository.refreshProperties();
        }

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment(); // App is busy until further notice

        mCompositeDisposable.clear();
        Disposable disposable = mPropertiesRepository
                .getProperties()
                .flatMap(Flowable::fromIterable)
                .filter(property -> {
                    switch (mCurrentFiltering) {
                        case ACTIVE_PROPERTIES:
                            return property.isActive();
                        case COMPLETED_PROPERTIES:
                            return property.isCompleted();
                        case ALL_PROPERTIES:
                        default:
                            return true;
                    }
                })
                .toList()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doFinally(() -> {
                    if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                        EspressoIdlingResource.decrement(); // Set app as idle.
                    }
                })
                .subscribe(
                        // onNext
                        properties -> {
                            processProperties(properties);
                            mPropertiesView.setLoadingIndicator(false);
                        },
                        // onError
                        throwable -> mPropertiesView.showLoadingPropertiesError());

        mCompositeDisposable.add(disposable);
    }

    private void processProperties(@NonNull List<Property> properties) {
        if (properties.isEmpty()) {
            // Show a message indicating there are no properties for that filter type.
            processEmptyProperties();
        } else {
            // Show the list of properties
            mPropertiesView.showProperties(properties);
            // Set the filter label's text.
            showFilterLabel();
        }
    }

    private void showFilterLabel() {
        switch (mCurrentFiltering) {
            case ACTIVE_PROPERTIES:
                mPropertiesView.showActiveFilterLabel();
                break;
            case COMPLETED_PROPERTIES:
                mPropertiesView.showCompletedFilterLabel();
                break;
            default:
                mPropertiesView.showAllFilterLabel();
                break;
        }
    }

    private void processEmptyProperties() {
        switch (mCurrentFiltering) {
            case ACTIVE_PROPERTIES:
                mPropertiesView.showNoActiveProperties();
                break;
            case COMPLETED_PROPERTIES:
                mPropertiesView.showNoCompletedProperties();
                break;
            default:
                mPropertiesView.showNoProperties();
                break;
        }
    }

    @Override
    public void addNewProperty() {
        mPropertiesView.showAddProperty();
    }

    @Override
    public void openPropertyDetails(@NonNull Property requestedProperty) {
        checkNotNull(requestedProperty, "requestedProperty cannot be null!");
        mPropertiesView.showPropertyDetailsUi(requestedProperty.getId());
    }

    @Override
    public void completeProperty(@NonNull Property completedProperty) {
        checkNotNull(completedProperty, "completedProperty cannot be null!");
        mPropertiesRepository.completeProperty(completedProperty);
        mPropertiesView.showPropertyMarkedComplete();
        loadProperties(false, false);
    }

    @Override
    public void activateProperty(@NonNull Property activeProperty) {
        checkNotNull(activeProperty, "activeProperty cannot be null!");
        mPropertiesRepository.activateProperty(activeProperty);
        mPropertiesView.showPropertyMarkedActive();
        loadProperties(false, false);
    }

    @Override
    public void clearCompletedProperties() {
        mPropertiesRepository.clearCompletedProperties();
        mPropertiesView.showCompletedPropertiesCleared();
        loadProperties(false, false);
    }

    @Override
    public PropertiesFilterType getFiltering() {
        return mCurrentFiltering;
    }

    /**
     * Sets the current property filtering type.
     *
     * @param requestType Can be {@link PropertiesFilterType#ALL_PROPERTIES},
     *                    {@link PropertiesFilterType#COMPLETED_PROPERTIES}, or
     *                    {@link PropertiesFilterType#ACTIVE_PROPERTIES}
     */
    @Override
    public void setFiltering(@NonNull PropertiesFilterType requestType) {
        mCurrentFiltering = requestType;
    }

}
