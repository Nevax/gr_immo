/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import fr.nevax.grimmo.data.Task;
import io.reactivex.Flowable;


/**
 * Main entry point for accessing tasks data.
 * <p>
 */
public interface TasksDataSource {

    Flowable<List<Task>> getTasks();

    Flowable<Optional<Task>> getTask(@NonNull String taskId);

    void saveTask(@NonNull Task task);

    void completeTask(@NonNull Task task);

    void completeTask(@NonNull String taskId);

    void activateTask(@NonNull Task task);

    void activateTask(@NonNull String taskId);

    void clearCompletedTasks();

    void refreshTasks();

    void deleteAllTasks();

    void deleteTask(@NonNull String taskId);
}
