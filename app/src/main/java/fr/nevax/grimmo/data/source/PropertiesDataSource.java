/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import io.reactivex.Flowable;


/**
 * Main entry point for accessing properties data.
 * <p>
 */
public interface PropertiesDataSource {

    Flowable<List<Property>> getProperties();

    Flowable<Optional<Property>> getProperty(@NonNull String propertyId);

    void saveProperty(@NonNull Property property);

    void completeProperty(@NonNull Property property);

    void completeProperty(@NonNull String propertyId);

    void activateProperty(@NonNull Property property);

    void activateProperty(@NonNull String propertyId);

    void clearCompletedProperties();

    void refreshProperties();

    void deleteAllProperties();

    void deleteProperty(@NonNull String propertyId);
}
