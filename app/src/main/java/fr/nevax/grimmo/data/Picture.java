package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Strings;

import java.io.File;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

public class Picture {

    @NonNull
    private String mId;

    @NonNull
    private String mTitle;

    @Nullable
    private Property mProperty;

    @NonNull
    private String mDescription;

    @NonNull
    private File mImageFile;

    public Picture(@Nullable String itemId, @NonNull String title, @NonNull String description, @NonNull String imageFilename) {
        this(itemId, title, description, new File(imageFilename));
    }

    public Picture(@Nullable String id, @NonNull String title, @NonNull String description, @NonNull File imageFile) {

        if (id == null)
            id = UUID.randomUUID().toString();

        //TODO Fix check if file exist
//        checkArgument(imageFile.exists(), "image doesn't exist");

        this.mId = checkNotNull(id, "id cannot be null");
        this.mTitle = checkNotNull(title, "title cannot be null");
        this.mDescription = checkNotNull(description, "description cannot be null");
        this.mImageFile = checkNotNull(imageFile, "image file cannot be null");
    }

    @NonNull
    public String getId() {
        return mId;
    }

    public void setId(@NonNull String mId) {
        this.mId = mId;
    }

    @NonNull
    public String getDescription() {
        return mDescription;
    }

    public void setDescription(@NonNull String mDescription) {
        this.mDescription = mDescription;
    }

    @NonNull
    public File getImageFile() {
        return mImageFile;
    }

    public void setImageFile(@NonNull File mImageBitmap) {
        this.mImageFile = mImageBitmap;
    }

    public boolean isEmpty() {
        return Strings.isNullOrEmpty(mTitle) &&
                Strings.isNullOrEmpty(mDescription);
    }

    @NonNull
    public Property getProperty() {
        return mProperty;
    }

    public void setProperty(@NonNull Property mProperty) {
        this.mProperty = mProperty;
    }
}
