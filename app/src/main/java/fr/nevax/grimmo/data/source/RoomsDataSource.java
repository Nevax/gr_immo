/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import fr.nevax.grimmo.data.Room;
import io.reactivex.Flowable;


/**
 * Main entry point for accessing rooms data.
 * <p>
 */
public interface RoomsDataSource {

    Flowable<List<Room>> getRooms();

    Flowable<Optional<Room>> getRoom(@NonNull String roomId);

    void saveRoom(@NonNull Room room);

    void refreshRooms();

    void deleteAllRooms();

    void deleteRoom(@NonNull String roomId);

    void saveRoom(List<Room> rooms);
}
