/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data;

/**
 * Created by GRAVELOT Antoine on 09/02/18.
 */

public abstract class User {

    protected Long mId;
    protected String mFirstName;
    protected String mLastName;
    protected String mPhoneNumber;
    protected String mAdress;
    protected String mZipCode;
    protected String mCtiy;

}
