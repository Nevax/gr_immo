/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.Picture;
import io.reactivex.Flowable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Concrete implementation to load pictures from the data sources into a cache.
 * <p/>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
public class PicturesRepository implements PicturesDataSource {

    @Nullable
    private static PicturesRepository INSTANCE = null;

    @NonNull
    private final PicturesDataSource mPicturesRemoteDataSource;

    @NonNull
    private final PicturesDataSource mPicturesLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    @Nullable
    Map<String, Picture> mCachedPictures;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    boolean mCacheIsDirty = false;

    // Prevent direct instantiation.
    private PicturesRepository(@NonNull PicturesDataSource picturesRemoteDataSource,
                               @NonNull PicturesDataSource picturesLocalDataSource) {
        mPicturesRemoteDataSource = checkNotNull(picturesRemoteDataSource);
        mPicturesLocalDataSource = checkNotNull(picturesLocalDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param picturesRemoteDataSource the backend data source
     * @param picturesLocalDataSource  the device storage data source
     * @return the {@link PicturesRepository} instance
     */
    public static PicturesRepository getInstance(@NonNull PicturesDataSource picturesRemoteDataSource,
                                                 @NonNull PicturesDataSource picturesLocalDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new PicturesRepository(picturesRemoteDataSource, picturesLocalDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(PicturesDataSource, PicturesDataSource)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     * Gets pictures from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     */
    @Override
    public Flowable<List<Picture>> getPictures() {
        // Respond immediately with cache if available and not dirty
        if (mCachedPictures != null && !mCacheIsDirty) {
            return Flowable.fromIterable(mCachedPictures.values()).toList().toFlowable();
        } else if (mCachedPictures == null) {
            mCachedPictures = new LinkedHashMap<>();
        }

        Flowable<List<Picture>> remotePictures = getAndSaveRemotePictures();

        if (mCacheIsDirty) {
            return remotePictures;
        } else {
            // Query the local storage if available. If not, query the network.
            Flowable<List<Picture>> localPictures = getAndCacheLocalPictures();
            return Flowable.concat(localPictures, remotePictures)
                    .filter(pictures -> !pictures.isEmpty())
                    .firstOrError()
                    .toFlowable();
        }
    }

    private Flowable<List<Picture>> getAndCacheLocalPictures() {
        return mPicturesLocalDataSource.getPictures()
                .flatMap(pictures -> Flowable.fromIterable(pictures)
                        .doOnNext(picture -> mCachedPictures.put(picture.getId(), picture))
                        .toList()
                        .toFlowable());
    }

    private Flowable<List<Picture>> getAndSaveRemotePictures() {
        return mPicturesRemoteDataSource
                .getPictures()
                .flatMap(pictures -> Flowable.fromIterable(pictures).doOnNext(picture -> {
                    mPicturesLocalDataSource.savePicture(picture);
                    mCachedPictures.put(picture.getId(), picture);
                }).toList().toFlowable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

    @Override
    public void savePicture(@NonNull Picture picture) {
        checkNotNull(picture);
        mPicturesRemoteDataSource.savePicture(picture);
        mPicturesLocalDataSource.savePicture(picture);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedPictures == null) {
            mCachedPictures = new LinkedHashMap<>();
        }
        mCachedPictures.put(picture.getId(), picture);
    }

    /**
     * Gets pictures from local data source (sqlite) unless the table is new or empty. In that case it
     * uses the network data source. This is done to simplify the sample.
     */
    @Override
    public Flowable<Optional<Picture>> getPicture(@NonNull final String pictureId) {
        checkNotNull(pictureId);

        final Picture cachedPicture = getPictureWithId(pictureId);

        // Respond immediately with cache if available
        if (cachedPicture != null) {
            return Flowable.just(Optional.of(cachedPicture));
        }

        // Load from server/persisted if needed.

        // Do in memory cache update to keep the app UI up to date
        if (mCachedPictures == null) {
            mCachedPictures = new LinkedHashMap<>();
        }

        // Is the picture in the local data source? If not, query the network.
        Flowable<Optional<Picture>> localPicture = getPictureWithIdFromLocalRepository(pictureId);
        Flowable<Optional<Picture>> remotePicture = mPicturesRemoteDataSource
                .getPicture(pictureId)
                .doOnNext(pictureOptional -> {
                    if (pictureOptional.isPresent()) {
                        Picture picture = pictureOptional.get();
                        mPicturesLocalDataSource.savePicture(picture);
                        mCachedPictures.put(picture.getId(), picture);
                    }
                });

        return Flowable.concat(localPicture, remotePicture)
                .firstElement()
                .toFlowable();
    }

    @Override
    public void refreshPictures() {
        mCacheIsDirty = true;
    }

    @Override
    public void deleteAllPictures() {
        mPicturesRemoteDataSource.deleteAllPictures();
        mPicturesLocalDataSource.deleteAllPictures();

        if (mCachedPictures == null) {
            mCachedPictures = new LinkedHashMap<>();
        }
        mCachedPictures.clear();
    }

    @Override
    public void deletePicture(@NonNull String pictureId) {
        mPicturesRemoteDataSource.deletePicture(checkNotNull(pictureId));
        mPicturesLocalDataSource.deletePicture(checkNotNull(pictureId));

        mCachedPictures.remove(pictureId);
    }

    @Override
    public void savePicture(List<Picture> pictures) {
        for (Picture picture : pictures) {
            this.savePicture(picture);
        }
    }

    @Nullable
    private Picture getPictureWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedPictures == null || mCachedPictures.isEmpty()) {
            return null;
        } else {
            return mCachedPictures.get(id);
        }
    }

    @NonNull
    Flowable<Optional<Picture>> getPictureWithIdFromLocalRepository(@NonNull final String pictureId) {
        return mPicturesLocalDataSource
                .getPicture(pictureId)
                .doOnNext(pictureOptional -> {
                    if (pictureOptional.isPresent()) {
                        mCachedPictures.put(pictureId, pictureOptional.get());
                    }
                })
                .firstElement().toFlowable();
    }
}
