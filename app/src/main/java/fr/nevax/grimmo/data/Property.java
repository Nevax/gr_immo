/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by GRAVELOT Antoine on 09/02/18.
 */

public class Property {

    public static final String NO_IMAGE = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Pas_d%27image_disponible.svg/240px-Pas_d%27image_disponible.svg.png";

    @NonNull
    private final String mId;

    @Nullable
    private String mTitle;

    @Nullable
    private String mDescription;

    private Boolean mCompleted;

    private int mSize;

    private Double mLatitude;
    private Double mLongitude;

    private Float mPrice;

    private PropertyType mPropertyType;

    private Date mCreated;
    private Date mUpdated;

    private Client mOwner;

    private List<Room> mRooms = new ArrayList<>();
    private List<Picture> mPictures = new ArrayList<>();

    /**
     * Use this constructor to create a new active Property.
     *
     * @param title       title of the property
     * @param description description of the property
     */
    public Property(@Nullable String title, @Nullable String description, @Nullable Float price) {
        this(title, description, price, UUID.randomUUID().toString(), false);
    }

    /**
     * Use this constructor to create an active Property if the Property already has an id (copy of another
     * Property).
     *
     * @param title       title of the property
     * @param description description of the property
     * @param id          id of the property
     */
    public Property(@Nullable String title, @Nullable String description, @Nullable Float price, @NonNull String id) {
        this(title, description, price, id, false);
    }

    /**
     * Use this constructor to create a new completed Property.
     *
     * @param title       title of the property
     * @param description description of the property
     * @param completed   true if the property is completed, false if it's active
     */
    public Property(@Nullable String title, @Nullable String description, @Nullable Float price, boolean completed) {
        this(title, description, price, UUID.randomUUID().toString(), completed);
    }

    /**
     * Use this constructor to specify a completed Property if the Property already has an id (copy of
     * another Property).
     *
     * @param title       title of the property
     * @param description description of the property
     * @param id          id of the property
     * @param completed   true if the property is completed, false if it's active
     */
    public Property(@Nullable String title, @Nullable String description, @Nullable Float price,
                    @NonNull String id, boolean completed) {
        mId = id;
        mTitle = title;
        mDescription = description;
        mPrice = price;
        mCompleted = completed;
        mPropertyType = PropertyType.UNKNOWN;
    }

    @NonNull
    public String getId() {
        return mId;
    }

    @Nullable
    public String getTitle() {
        return mTitle;
    }

    @Nullable
    public String getTitleForList() {
        if (!Strings.isNullOrEmpty(mTitle)) {
            return mTitle;
        } else {
            return mDescription;
        }
    }

    @Nullable
    public String getDescription() {
        return mDescription;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public boolean isActive() {
        return !mCompleted;
    }

    public boolean isEmpty() {
        return Strings.isNullOrEmpty(mTitle) &&
                Strings.isNullOrEmpty(mDescription);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return Objects.equal(mId, property.mId) &&
                Objects.equal(mTitle, property.mTitle) &&
                Objects.equal(mDescription, property.mDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mId, mTitle, mDescription);
    }

    @Override
    public String toString() {
        return "Property with title " + mTitle;
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int mSize) {
        this.mSize = mSize;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public Float getPrice() {
        return mPrice;
    }

    public void setPrice(Float mPrice) {
        this.mPrice = mPrice;
    }

    public PropertyType getPropertyType() {
        return mPropertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.mPropertyType = propertyType;
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(Date mCreated) {
        this.mCreated = mCreated;
    }

    public Date getUpdated() {
        return mUpdated;
    }

    public void setUpdated(Date mUpdated) {
        this.mUpdated = mUpdated;
    }

    public Client getOwner() {
        return mOwner;
    }

    public void setOwner(Client mOwner) {
        this.mOwner = mOwner;
    }

    public List<Room> getRooms() {
        return mRooms;
    }

    public void setRooms(Room room) {
        this.mRooms.add(room);
    }

    public void setRooms(List<Room> rooms) {
        this.mRooms = rooms;
    }

    public List<Picture> getPictures() {
        return mPictures;
    }

    public void setPictures(List<Picture> pictures) {
        if (pictures != null) {
            Picture[] roomsArray = pictures.toArray(new Picture[pictures.size()]);
            for (Picture picture : roomsArray) {
                this.addPictures(picture);
            }
        }
    }

    public void addPictures(Picture picture) {
        picture.setProperty(this);
        mPictures.add(picture);
    }
}

