/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.remote;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PicturesDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of the data source that adds a latency simulating network.
 */
public class PicturesRemoteDataSource implements PicturesDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 5000;
    private final static Map<String, Property> TASKS_SERVICE_DATA;
    private static PicturesRemoteDataSource INSTANCE;

    static {
        TASKS_SERVICE_DATA = new LinkedHashMap<>(2);
        addProperty("Build tower in Pisa", "Ground looks good, no foundation work required.", 1000.22f);
        addProperty("Finish bridge in Tacoma", "Found awesome girders at half the cost!", 50000.52f);
    }

    // Prevent direct instantiation.
    private PicturesRemoteDataSource() {
    }

    public static PicturesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PicturesRemoteDataSource();
        }
        return INSTANCE;
    }

    private static void addProperty(String title, String description, Float price) {
        Property newProperty = new Property(title, description, price);
        TASKS_SERVICE_DATA.put(newProperty.getId(), newProperty);
    }

    @Override
    public Flowable<List<Picture>> getPictures() {
        return null;
    }

    @Override
    public Flowable<Optional<Picture>> getPicture(@NonNull String pictureId) {
        return null;
    }

    @Override
    public void savePicture(@NonNull Picture picture) {

    }

    @Override
    public void refreshPictures() {

    }

    @Override
    public void deleteAllPictures() {

    }

    @Override
    public void deletePicture(@NonNull String pictureId) {

    }

    @Override
    public void savePicture(List<Picture> pictures) {

    }
}
