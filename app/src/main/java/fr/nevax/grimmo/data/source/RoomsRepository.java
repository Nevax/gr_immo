/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.Room;
import io.reactivex.Flowable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Concrete implementation to load rooms from the data sources into a cache.
 * <p/>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
public class RoomsRepository implements RoomsDataSource {

    @Nullable
    private static RoomsRepository INSTANCE = null;

    @NonNull
    private final RoomsDataSource mRoomsRemoteDataSource;

    @NonNull
    private final RoomsDataSource mRoomsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    @Nullable
    Map<String, Room> mCachedRooms;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    boolean mCacheIsDirty = false;

    // Prevent direct instantiation.
    private RoomsRepository(@NonNull RoomsDataSource roomsRemoteDataSource,
                            @NonNull RoomsDataSource roomsLocalDataSource) {
        mRoomsRemoteDataSource = checkNotNull(roomsRemoteDataSource);
        mRoomsLocalDataSource = checkNotNull(roomsLocalDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param roomsRemoteDataSource the backend data source
     * @param roomsLocalDataSource  the device storage data source
     * @return the {@link RoomsRepository} instance
     */
    public static RoomsRepository getInstance(@NonNull RoomsDataSource roomsRemoteDataSource,
                                              @NonNull RoomsDataSource roomsLocalDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new RoomsRepository(roomsRemoteDataSource, roomsLocalDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(RoomsDataSource, RoomsDataSource)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     * Gets rooms from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     */
    @Override
    public Flowable<List<Room>> getRooms() {
        // Respond immediately with cache if available and not dirty
        if (mCachedRooms != null && !mCacheIsDirty) {
            return Flowable.fromIterable(mCachedRooms.values()).toList().toFlowable();
        } else if (mCachedRooms == null) {
            mCachedRooms = new LinkedHashMap<>();
        }

        Flowable<List<Room>> remoteRooms = getAndSaveRemoteRooms();

        if (mCacheIsDirty) {
            return remoteRooms;
        } else {
            // Query the local storage if available. If not, query the network.
            Flowable<List<Room>> localRooms = getAndCacheLocalRooms();
            return Flowable.concat(localRooms, remoteRooms)
                    .filter(rooms -> !rooms.isEmpty())
                    .firstOrError()
                    .toFlowable();
        }
    }

    private Flowable<List<Room>> getAndCacheLocalRooms() {
        return mRoomsLocalDataSource.getRooms()
                .flatMap(rooms -> Flowable.fromIterable(rooms)
                        .doOnNext(room -> mCachedRooms.put(room.getId(), room))
                        .toList()
                        .toFlowable());
    }

    private Flowable<List<Room>> getAndSaveRemoteRooms() {
        return mRoomsRemoteDataSource
                .getRooms()
                .flatMap(rooms -> Flowable.fromIterable(rooms).doOnNext(room -> {
                    mRoomsLocalDataSource.saveRoom(room);
                    mCachedRooms.put(room.getId(), room);
                }).toList().toFlowable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

    @Override
    public void saveRoom(@NonNull Room room) {
        checkNotNull(room);
        mRoomsRemoteDataSource.saveRoom(room);
        mRoomsLocalDataSource.saveRoom(room);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedRooms == null) {
            mCachedRooms = new LinkedHashMap<>();
        }
        mCachedRooms.put(room.getId(), room);
    }

    /**
     * Gets rooms from local data source (sqlite) unless the table is new or empty. In that case it
     * uses the network data source. This is done to simplify the sample.
     */
    @Override
    public Flowable<Optional<Room>> getRoom(@NonNull final String roomId) {
        checkNotNull(roomId);

        final Room cachedRoom = getRoomWithId(roomId);

        // Respond immediately with cache if available
        if (cachedRoom != null) {
            return Flowable.just(Optional.of(cachedRoom));
        }

        // Load from server/persisted if needed.

        // Do in memory cache update to keep the app UI up to date
        if (mCachedRooms == null) {
            mCachedRooms = new LinkedHashMap<>();
        }

        // Is the room in the local data source? If not, query the network.
        Flowable<Optional<Room>> localRoom = getRoomWithIdFromLocalRepository(roomId);
        Flowable<Optional<Room>> remoteRoom = mRoomsRemoteDataSource
                .getRoom(roomId)
                .doOnNext(roomOptional -> {
                    if (roomOptional.isPresent()) {
                        Room room = roomOptional.get();
                        mRoomsLocalDataSource.saveRoom(room);
                        mCachedRooms.put(room.getId(), room);
                    }
                });

        return Flowable.concat(localRoom, remoteRoom)
                .firstElement()
                .toFlowable();
    }

    @Override
    public void refreshRooms() {
        mCacheIsDirty = true;
    }

    @Override
    public void deleteAllRooms() {
        mRoomsRemoteDataSource.deleteAllRooms();
        mRoomsLocalDataSource.deleteAllRooms();

        if (mCachedRooms == null) {
            mCachedRooms = new LinkedHashMap<>();
        }
        mCachedRooms.clear();
    }

    @Override
    public void deleteRoom(@NonNull String roomId) {
        mRoomsRemoteDataSource.deleteRoom(checkNotNull(roomId));
        mRoomsLocalDataSource.deleteRoom(checkNotNull(roomId));

        mCachedRooms.remove(roomId);
    }

    @Override
    public void saveRoom(List<Room> rooms) {
        Room[] roomsArray = rooms.toArray(new Room[rooms.size()]);
        for (Room room : roomsArray) {
            this.saveRoom(room);
        }
    }

    @Nullable
    private Room getRoomWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedRooms == null || mCachedRooms.isEmpty()) {
            return null;
        } else {
            return mCachedRooms.get(id);
        }
    }

    @NonNull
    Flowable<Optional<Room>> getRoomWithIdFromLocalRepository(@NonNull final String roomId) {
        return mRoomsLocalDataSource
                .getRoom(roomId)
                .doOnNext(roomOptional -> {
                    if (roomOptional.isPresent()) {
                        mCachedRooms.put(roomId, roomOptional.get());
                    }
                })
                .firstElement().toFlowable();
    }
}
