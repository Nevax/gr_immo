/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data;

import android.content.Context;

import fr.nevax.grimmo.R;

public enum PropertyType {

    HOUSE("house", R.string.house),
    APARTMENT("apartment", R.string.apartment),
    GARAGE("garage", R.string.garage),
    OTHER("other", R.string.other),
    UNKNOWN("unknown", R.string.unknown);

    private String id;
    private Integer stringResourceId;

    PropertyType(String id, Integer stringResourceId) {
        this.id = id;
        this.stringResourceId = stringResourceId;
    }

    public static String getEnumByString(String id) {
        for (PropertyType e : PropertyType.values()) {
            if (id.equals(e.id)) return e.name();
        }
        return null;
    }

    public static String[] getStringList(Context context) {
        int index = 0;
        String[] propertyList = new String[PropertyType.values().length];
        for (PropertyType propertyType : PropertyType.values()) {
            propertyList[index] = context.getString(propertyType.stringResourceId);
            index++;
        }
        return propertyList;
    }

    public static PropertyType getPropertyTypeFromIndex(int index) {
        //TODO Check if index is ok
        return PropertyType.values()[index];
    }

    public String getId() {
        return id;
    }

    public Integer getStringResourceId() {
        return stringResourceId;
    }
}
