/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.Property;
import io.reactivex.Flowable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Concrete implementation to load properties from the data sources into a cache.
 * <p/>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
public class PropertiesRepository implements PropertiesDataSource {

    @Nullable
    private static PropertiesRepository INSTANCE = null;

    @NonNull
    private final PropertiesDataSource mPropertiesRemoteDataSource;

    @NonNull
    private final PropertiesDataSource mPropertiesLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    @Nullable
    Map<String, Property> mCachedProperties;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    @VisibleForTesting
    boolean mCacheIsDirty = false;

    // Prevent direct instantiation.
    private PropertiesRepository(@NonNull PropertiesDataSource propertiesRemoteDataSource,
                                 @NonNull PropertiesDataSource propertiesLocalDataSource) {
        mPropertiesRemoteDataSource = checkNotNull(propertiesRemoteDataSource);
        mPropertiesLocalDataSource = checkNotNull(propertiesLocalDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param propertiesRemoteDataSource the backend data source
     * @param propertiesLocalDataSource  the device storage data source
     * @return the {@link PropertiesRepository} instance
     */
    public static PropertiesRepository getInstance(@NonNull PropertiesDataSource propertiesRemoteDataSource,
                                                   @NonNull PropertiesDataSource propertiesLocalDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new PropertiesRepository(propertiesRemoteDataSource, propertiesLocalDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(PropertiesDataSource, PropertiesDataSource)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     * Gets properties from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     */
    @Override
    public Flowable<List<Property>> getProperties() {
        // Respond immediately with cache if available and not dirty
        if (mCachedProperties != null && !mCacheIsDirty) {
            return Flowable.fromIterable(mCachedProperties.values()).toList().toFlowable();
        } else if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }

        Flowable<List<Property>> remoteProperties = getAndSaveRemoteProperties();

        if (mCacheIsDirty) {
            return remoteProperties;
        } else {
            // Query the local storage if available. If not, query the network.
            Flowable<List<Property>> localProperties = getAndCacheLocalProperties();
            return Flowable.concat(localProperties, remoteProperties)
                    .filter(properties -> !properties.isEmpty())
                    .firstOrError()
                    .toFlowable();
        }
    }

    private Flowable<List<Property>> getAndCacheLocalProperties() {
        return mPropertiesLocalDataSource.getProperties()
                .flatMap(properties -> Flowable.fromIterable(properties)
                        .doOnNext(property -> mCachedProperties.put(property.getId(), property))
                        .toList()
                        .toFlowable());
    }

    private Flowable<List<Property>> getAndSaveRemoteProperties() {
        return mPropertiesRemoteDataSource
                .getProperties()
                .flatMap(properties -> Flowable.fromIterable(properties).doOnNext(property -> {
                    mPropertiesLocalDataSource.saveProperty(property);
                    mCachedProperties.put(property.getId(), property);
                }).toList().toFlowable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

    @Override
    public void saveProperty(@NonNull Property property) {
        checkNotNull(property);
        mPropertiesRemoteDataSource.saveProperty(property);
        mPropertiesLocalDataSource.saveProperty(property);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        mCachedProperties.put(property.getId(), property);
    }

    @Override
    public void completeProperty(@NonNull Property property) {
        checkNotNull(property);
        mPropertiesRemoteDataSource.completeProperty(property);
        mPropertiesLocalDataSource.completeProperty(property);

        Property completedProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId(), true);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        mCachedProperties.put(property.getId(), completedProperty);
    }

    @Override
    public void completeProperty(@NonNull String propertyId) {
        checkNotNull(propertyId);
        Property propertyWithId = getPropertyWithId(propertyId);
        if (propertyWithId != null) {
            completeProperty(propertyWithId);
        }
    }

    @Override
    public void activateProperty(@NonNull Property property) {
        checkNotNull(property);
        mPropertiesRemoteDataSource.activateProperty(property);
        mPropertiesLocalDataSource.activateProperty(property);

        Property activeProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId());

        // Do in memory cache update to keep the app UI up to date
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        mCachedProperties.put(property.getId(), activeProperty);
    }

    @Override
    public void activateProperty(@NonNull String propertyId) {
        checkNotNull(propertyId);
        Property propertyWithId = getPropertyWithId(propertyId);
        if (propertyWithId != null) {
            activateProperty(propertyWithId);
        }
    }

    @Override
    public void clearCompletedProperties() {
        mPropertiesRemoteDataSource.clearCompletedProperties();
        mPropertiesLocalDataSource.clearCompletedProperties();

        // Do in memory cache update to keep the app UI up to date
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        Iterator<Map.Entry<String, Property>> it = mCachedProperties.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Property> entry = it.next();
            if (entry.getValue().isCompleted()) {
                it.remove();
            }
        }
    }

    /**
     * Gets properties from local data source (sqlite) unless the table is new or empty. In that case it
     * uses the network data source. This is done to simplify the sample.
     */
    @Override
    public Flowable<Optional<Property>> getProperty(@NonNull final String propertyId) {
        checkNotNull(propertyId);

        final Property cachedProperty = getPropertyWithId(propertyId);

        // Respond immediately with cache if available
        if (cachedProperty != null) {
            return Flowable.just(Optional.of(cachedProperty));
        }

        // Load from server/persisted if needed.

        // Do in memory cache update to keep the app UI up to date
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }

        // Is the property in the local data source? If not, query the network.
        Flowable<Optional<Property>> localProperty = getPropertyWithIdFromLocalRepository(propertyId);
        Flowable<Optional<Property>> remoteProperty = mPropertiesRemoteDataSource
                .getProperty(propertyId)
                .doOnNext(propertyOptional -> {
                    if (propertyOptional.isPresent()) {
                        Property property = propertyOptional.get();
                        mPropertiesLocalDataSource.saveProperty(property);
                        mCachedProperties.put(property.getId(), property);
                    }
                });

        return Flowable.concat(localProperty, remoteProperty)
                .firstElement()
                .toFlowable();
    }

    @Override
    public void refreshProperties() {
        mCacheIsDirty = true;
    }

    @Override
    public void deleteAllProperties() {
        mPropertiesRemoteDataSource.deleteAllProperties();
        mPropertiesLocalDataSource.deleteAllProperties();

        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        mCachedProperties.clear();
    }

    @Override
    public void deleteProperty(@NonNull String propertyId) {
        mPropertiesRemoteDataSource.deleteProperty(checkNotNull(propertyId));
        mPropertiesLocalDataSource.deleteProperty(checkNotNull(propertyId));

        mCachedProperties.remove(propertyId);
    }

    @Nullable
    private Property getPropertyWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedProperties == null || mCachedProperties.isEmpty()) {
            return null;
        } else {
            return mCachedProperties.get(id);
        }
    }

    @NonNull
    Flowable<Optional<Property>> getPropertyWithIdFromLocalRepository(@NonNull final String propertyId) {
        return mPropertiesLocalDataSource
                .getProperty(propertyId)
                .doOnNext(propertyOptional -> {
                    if (propertyOptional.isPresent()) {
                        mCachedProperties.put(propertyId, propertyOptional.get());
                    }
                })
                .firstElement().toFlowable();
    }
}
