/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import fr.nevax.grimmo.data.Picture;
import io.reactivex.Flowable;


/**
 * Main entry point for accessing properties data.
 * <p>
 */
public interface PicturesDataSource {

    Flowable<List<Picture>> getPictures();

    Flowable<Optional<Picture>> getPicture(@NonNull String pictureId);

    void savePicture(@NonNull Picture picture);

    void refreshPictures();

    void deleteAllPictures();

    void deletePicture(@NonNull String pictureId);

    void savePicture(List<Picture> pictures);
}
