/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.common.base.Optional;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.PropertyType;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.PropertyEntry;


/**
 * Concrete implementation of a data source as a db.
 */
public class PropertiesLocalDataSource implements PropertiesDataSource {

    @Nullable
    private static PropertiesLocalDataSource INSTANCE;

    @NonNull
    private final BriteDatabase mDatabaseHelper;

    @NonNull
    private Function<Cursor, Property> mPropertyMapperFunction;

    // Prevent direct instantiation.
    private PropertiesLocalDataSource(@NonNull Context context,
                                      @NonNull BaseSchedulerProvider schedulerProvider) {
        checkNotNull(context, "context cannot be null");
        checkNotNull(schedulerProvider, "scheduleProvider cannot be null");
        BaseDbHelper dbHelper = new BaseDbHelper(context);
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, schedulerProvider.io());
        mPropertyMapperFunction = this::getProperty;
    }

    public static PropertiesLocalDataSource getInstance(
            @NonNull Context context,
            @NonNull BaseSchedulerProvider schedulerProvider) {
        if (INSTANCE == null) {
            INSTANCE = new PropertiesLocalDataSource(context, schedulerProvider);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @NonNull
    private Property getProperty(@NonNull Cursor c) {
        String itemId = c.getString(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_ENTRY_ID));
        String title = c.getString(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_TITLE));
        String description = c.getString(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_DESCRIPTION));
        Float price = c.getFloat(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_PRICE));
        Integer size = c.getInt(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_SIZE));
        boolean completed = c.getInt(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_COMPLETED)) == 1;
        PropertyType propertyType = PropertyType.valueOf(c.getString(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_TYPE)));
        Double latitude = c.getDouble(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_LATITUDE));
        Double longitude = c.getDouble(c.getColumnIndexOrThrow(PropertyEntry.COLUMN_NAME_LONGITUDE));

        Property property = new Property(title, description, price, itemId, completed);
        property.setPropertyType(propertyType);
        property.setLatitude(latitude);
        property.setLongitude(longitude);
        property.setSize(size);

        return property;
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        String[] projection = {
                PropertyEntry.COLUMN_NAME_ENTRY_ID,
                PropertyEntry.COLUMN_NAME_TITLE,
                PropertyEntry.COLUMN_NAME_DESCRIPTION,
                PropertyEntry.COLUMN_NAME_PRICE,
                PropertyEntry.COLUMN_NAME_SIZE,
                PropertyEntry.COLUMN_NAME_TYPE,
                PropertyEntry.COLUMN_NAME_LATITUDE,
                PropertyEntry.COLUMN_NAME_LONGITUDE,
                PropertyEntry.COLUMN_NAME_COMPLETED
        };
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), PropertyEntry.TABLE_NAME);
        return mDatabaseHelper.createQuery(PropertyEntry.TABLE_NAME, sql)
                .mapToList(mPropertyMapperFunction)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public Flowable<Optional<Property>> getProperty(@NonNull String propertyId) {
        String[] projection = {
                PropertyEntry.COLUMN_NAME_ENTRY_ID,
                PropertyEntry.COLUMN_NAME_TITLE,
                PropertyEntry.COLUMN_NAME_DESCRIPTION,
                PropertyEntry.COLUMN_NAME_PRICE,
                PropertyEntry.COLUMN_NAME_SIZE,
                PropertyEntry.COLUMN_NAME_TYPE,
                PropertyEntry.COLUMN_NAME_LATITUDE,
                PropertyEntry.COLUMN_NAME_LONGITUDE,
                PropertyEntry.COLUMN_NAME_COMPLETED
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), PropertyEntry.TABLE_NAME, PropertyEntry.COLUMN_NAME_ENTRY_ID);
        return mDatabaseHelper.createQuery(PropertyEntry.TABLE_NAME, sql, propertyId)
                .mapToOneOrDefault(cursor -> Optional.of(mPropertyMapperFunction.apply(cursor)), Optional.<Property>absent())
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public void saveProperty(@NonNull Property property) {
        checkNotNull(property);
        ContentValues values = new ContentValues();
        values.put(PropertyEntry.COLUMN_NAME_ENTRY_ID, property.getId());
        values.put(PropertyEntry.COLUMN_NAME_TITLE, property.getTitle());
        values.put(PropertyEntry.COLUMN_NAME_DESCRIPTION, property.getDescription());
        values.put(PropertyEntry.COLUMN_NAME_COMPLETED, property.isCompleted());
        values.put(PropertyEntry.COLUMN_NAME_PRICE, property.getPrice());
        values.put(PropertyEntry.COLUMN_NAME_SIZE, property.getSize());
        values.put(PropertyEntry.COLUMN_NAME_TYPE, property.getPropertyType().name());
        values.put(PropertyEntry.COLUMN_NAME_LATITUDE, property.getLatitude());
        values.put(PropertyEntry.COLUMN_NAME_LONGITUDE, property.getLongitude());
        values.put(PropertyEntry.COLUMN_NAME_COMPLETED, property.isCompleted());

        mDatabaseHelper.insert(PropertyEntry.TABLE_NAME, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void completeProperty(@NonNull Property property) {
        completeProperty(property.getId());
    }

    @Override
    public void completeProperty(@NonNull String propertyId) {
        ContentValues values = new ContentValues();
        values.put(PropertyEntry.COLUMN_NAME_COMPLETED, true);

        String selection = PropertyEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {propertyId};
        mDatabaseHelper.update(PropertyEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    @Override
    public void activateProperty(@NonNull Property property) {
        activateProperty(property.getId());
    }

    @Override
    public void activateProperty(@NonNull String propertyId) {
        ContentValues values = new ContentValues();
        values.put(PropertyEntry.COLUMN_NAME_COMPLETED, false);

        String selection = PropertyEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {propertyId};
        mDatabaseHelper.update(PropertyEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    @Override
    public void clearCompletedProperties() {
        String selection = PropertyEntry.COLUMN_NAME_COMPLETED + " LIKE ?";
        String[] selectionArgs = {"1"};
        mDatabaseHelper.delete(PropertyEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public void refreshProperties() {
        // Not required because the {@link PropertiesRepository} handles the logic of refreshing the
        // properties from all the available data sources.
    }

    @Override
    public void deleteAllProperties() {
        mDatabaseHelper.delete(PropertyEntry.TABLE_NAME, null);
    }

    @Override
    public void deleteProperty(@NonNull String propertyId) {
        String selection = PropertyEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {propertyId};
        mDatabaseHelper.delete(PropertyEntry.TABLE_NAME, selection, selectionArgs);
    }
}
