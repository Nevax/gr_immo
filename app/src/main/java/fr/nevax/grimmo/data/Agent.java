/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data;

/**
 * Created by GRAVELOT Antoine on 09/02/18.
 */

public class Agent extends User {

    private String mUsername;
    private String mPassword;
    private Agency mAgency;

    public Agent() {
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public Agency getmAgency() {
        return mAgency;
    }

    public void setmAgency(Agency mAgency) {
        this.mAgency = mAgency;
    }
}
