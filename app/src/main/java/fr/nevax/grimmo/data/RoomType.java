package fr.nevax.grimmo.data;

import fr.nevax.grimmo.R;

public enum RoomType {

    BATHROOM("bathroom", R.string.bathroom),
    UNKNOWN("unknown", R.string.unknown);

    private String id;
    private Integer stringResourceId;

    RoomType(String id, Integer stringResourceId) {
        this.id = id;
        this.stringResourceId = stringResourceId;
    }

    public static String getEnumByString(String id) {
        for (RoomType e : RoomType.values()) {
            if (id.equals(e.id)) return e.name();
        }
        return null;
    }
}
