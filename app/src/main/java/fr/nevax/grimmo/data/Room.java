package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.UUID;

public class Room {

    @NonNull
    private String mId;

    private String title;

    @Nullable
    private String mDescription;

    private int mSize;

    @Nullable
    private RoomType roomType;

    private List<Picture> pictures;

    private List<Room> nearbyRooms;

    public Room(@Nullable String id, String title, String mDescription, Integer mSize, RoomType roomType, List<Picture> pictures, List<Room> nearbyRooms) {

        if (id == null)
            this.mId = UUID.randomUUID().toString();
        else
            this.mId = id;

        this.title = title;
        this.mDescription = mDescription;
        this.mSize = mSize;
        this.roomType = roomType;
        this.pictures = pictures;
        this.nearbyRooms = nearbyRooms;
    }

    @NonNull
    public String getId() {
        return mId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    public String getDescription() {
        return mDescription;
    }

    public void setDescription(@Nullable String mDescription) {
        this.mDescription = mDescription;
    }

    public Integer getSize() {
        return mSize;
    }

    public void setSize(@Nullable Integer mSize) {
        this.mSize = mSize;
    }

    @Nullable
    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(@Nullable RoomType roomType) {
        this.roomType = roomType;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public void addRoomPictures(Picture picture) {
        this.pictures.add(picture);
    }

    public List<Room> getNearbyRooms() {
        return nearbyRooms;
    }

    public void setNearbyRooms(List<Room> nearbyRooms) {
        this.nearbyRooms = nearbyRooms;
    }

    public void addNearbyRooms(Room nearbyRoom) {
        this.nearbyRooms.add(nearbyRoom);
    }
}
