/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static fr.nevax.grimmo.data.source.local.PersistenceContract.PictureEntry;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.PropertyEntry;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.RoomEntry;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.TaskEntry;

public class BaseDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "database.db";

    private static final String PK = " PRIMARY KEY";

    private static final String FK = " FOREIGN KEY";

    private static final String REF = " REFERENCES ";

    private static final String TEXT_TYPE = " TEXT";

    private static final String BOOLEAN_TYPE = " INTEGER";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String REAL_TYPE = " REAL";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_TASKS_ENTRIES =
            "CREATE TABLE " + TaskEntry.TABLE_NAME + " (" +
                    TaskEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PK + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_COMPLETED + BOOLEAN_TYPE +
                    " )";

    private static final String SQL_CREATE_PROPERTIES_ENTRIES =
            "CREATE TABLE " + PropertyEntry.TABLE_NAME + " (" +
                    PropertyEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PK + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_PRICE + REAL_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_SIZE + INTEGER_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_TYPE + INTEGER_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                    PropertyEntry.COLUMN_NAME_COMPLETED + BOOLEAN_TYPE +
                    " )";

    private static final String SQL_CREATE_ROOMS_ENTRIES =
            "CREATE TABLE " + RoomEntry.TABLE_NAME + " (" +
                    RoomEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PK + COMMA_SEP +
                    RoomEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    RoomEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    RoomEntry.COLUMN_NAME_SIZE + INTEGER_TYPE + COMMA_SEP +
                    RoomEntry.COLUMN_NAME_TYPE + INTEGER_TYPE + COMMA_SEP +
                    RoomEntry.COLUMN_NAME_PROPERTY_ID + TEXT_TYPE + COMMA_SEP +
                    FK + "(" + RoomEntry.COLUMN_NAME_PROPERTY_ID + ")" +
                    REF + PropertyEntry.TABLE_NAME + "(" + PropertyEntry.COLUMN_NAME_ENTRY_ID + ")" +
                    " )";

    private static final String SQL_CREATE_PICTURES_ENTRIES =
            "CREATE TABLE " + PictureEntry.TABLE_NAME + " (" +
                    PictureEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PK + COMMA_SEP +
                    PictureEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PictureEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    PictureEntry.COLUMN_NAME_FILENAME + TEXT_TYPE + COMMA_SEP +
                    PictureEntry.COLUMN_NAME_PROPERTY_ID + TEXT_TYPE + COMMA_SEP +
                    FK + "(" + PictureEntry.COLUMN_NAME_PROPERTY_ID + ")" +
                    REF + PropertyEntry.TABLE_NAME + "(" + PropertyEntry.COLUMN_NAME_ENTRY_ID + ")" +
                    " )";

    public BaseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TASKS_ENTRIES);
        db.execSQL(SQL_CREATE_PROPERTIES_ENTRIES);
        db.execSQL(SQL_CREATE_ROOMS_ENTRIES);
        db.execSQL(SQL_CREATE_PICTURES_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }
}
