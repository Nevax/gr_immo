/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.remote;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.data.source.RoomsDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of the data source that adds a latency simulating network.
 */
public class RoomsRemoteDataSource implements RoomsDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 5000;
    private final static Map<String, Property> TASKS_SERVICE_DATA;
    private static RoomsRemoteDataSource INSTANCE;

    static {
        TASKS_SERVICE_DATA = new LinkedHashMap<>(2);
        addProperty("Build tower in Pisa", "Ground looks good, no foundation work required.", 1000.22f);
        addProperty("Finish bridge in Tacoma", "Found awesome girders at half the cost!", 50000.52f);
    }

    // Prevent direct instantiation.
    private RoomsRemoteDataSource() {
    }

    public static RoomsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RoomsRemoteDataSource();
        }
        return INSTANCE;
    }

    private static void addProperty(String title, String description, Float price) {
        Property newProperty = new Property(title, description, price);
        TASKS_SERVICE_DATA.put(newProperty.getId(), newProperty);
    }

    @Override
    public Flowable<List<Room>> getRooms() {
        return null;
    }

    @Override
    public Flowable<Optional<Room>> getRoom(@NonNull String roomId) {
        return null;
    }

    @Override
    public void saveRoom(@NonNull Room room) {

    }

    @Override
    public void refreshRooms() {

    }

    @Override
    public void deleteAllRooms() {

    }

    @Override
    public void deleteRoom(@NonNull String roomId) {

    }

    @Override
    public void saveRoom(List<Room> rooms) {

    }
}
