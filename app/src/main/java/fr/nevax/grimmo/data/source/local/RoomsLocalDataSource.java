/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.common.base.Optional;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import java.util.List;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.Room;
import fr.nevax.grimmo.data.RoomType;
import fr.nevax.grimmo.data.source.RoomsDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.RoomEntry;


/**
 * Concrete implementation of a data source as a db.
 */
public class RoomsLocalDataSource implements RoomsDataSource {

    @Nullable
    private static RoomsLocalDataSource INSTANCE;

    @NonNull
    private final BriteDatabase mDatabaseHelper;

    @NonNull
    private Function<Cursor, Room> mRoomMapperFunction;

    // Prevent direct instantiation.
    private RoomsLocalDataSource(@NonNull Context context,
                                 @NonNull BaseSchedulerProvider schedulerProvider) {
        checkNotNull(context, "context cannot be null");
        checkNotNull(schedulerProvider, "scheduleProvider cannot be null");
        BaseDbHelper dbHelper = new BaseDbHelper(context);
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, schedulerProvider.io());
        mRoomMapperFunction = this::getRoom;
    }

    public static RoomsLocalDataSource getInstance(
            @NonNull Context context,
            @NonNull BaseSchedulerProvider schedulerProvider) {
        if (INSTANCE == null) {
            INSTANCE = new RoomsLocalDataSource(context, schedulerProvider);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @NonNull
    private Room getRoom(@NonNull Cursor c) {
        String itemId = c.getString(c.getColumnIndexOrThrow(RoomEntry.COLUMN_NAME_ENTRY_ID));
        String title = c.getString(c.getColumnIndexOrThrow(RoomEntry.COLUMN_NAME_TITLE));
        String description = c.getString(c.getColumnIndexOrThrow(RoomEntry.COLUMN_NAME_DESCRIPTION));
        Integer size = c.getInt(c.getColumnIndexOrThrow(RoomEntry.COLUMN_NAME_SIZE));
        RoomType roomType = null;
        List<Picture> pictures = null;
        List<Room> nearbyRoom = null;

        return new Room(itemId, title, description, size, roomType, pictures, nearbyRoom);
    }

    @Override
    public Flowable<List<Room>> getRooms() {
        String[] projection = {
                RoomEntry.COLUMN_NAME_ENTRY_ID,
                RoomEntry.COLUMN_NAME_TITLE,
                RoomEntry.COLUMN_NAME_DESCRIPTION,
                RoomEntry.COLUMN_NAME_SIZE,
                RoomEntry.COLUMN_NAME_TYPE
//                PersistenceContract.RoomEntry.COLUMN_NAME_COMPLETED
        };
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), RoomEntry.TABLE_NAME);
        return mDatabaseHelper.createQuery(RoomEntry.TABLE_NAME, sql)
                .mapToList(mRoomMapperFunction)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public Flowable<Optional<Room>> getRoom(@NonNull String roomId) {
        String[] projection = {
                RoomEntry.COLUMN_NAME_ENTRY_ID,
                RoomEntry.COLUMN_NAME_TITLE,
                RoomEntry.COLUMN_NAME_DESCRIPTION,
                RoomEntry.COLUMN_NAME_SIZE,
                RoomEntry.COLUMN_NAME_TYPE,
                RoomEntry.COLUMN_NAME_PROPERTY_ID
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), RoomEntry.TABLE_NAME, RoomEntry.COLUMN_NAME_ENTRY_ID);
        return mDatabaseHelper.createQuery(RoomEntry.TABLE_NAME, sql, roomId)
                .mapToOneOrDefault(cursor -> Optional.of(mRoomMapperFunction.apply(cursor)), Optional.<Room>absent())
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public void saveRoom(@NonNull Room room) {
        checkNotNull(room);
        ContentValues values = new ContentValues();
        values.put(RoomEntry.COLUMN_NAME_ENTRY_ID, room.getId());
        values.put(RoomEntry.COLUMN_NAME_TITLE, room.getTitle());
        values.put(RoomEntry.COLUMN_NAME_DESCRIPTION, room.getDescription());
        values.put(RoomEntry.COLUMN_NAME_SIZE, room.getSize());
//        values.put(PersistenceContract.RoomEntry.COLUMN_NAME_TYPE, room.getRoomType());
        mDatabaseHelper.insert(RoomEntry.TABLE_NAME, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void refreshRooms() {
        // Not required because the {@link RoomsRepository} handles the logic of refreshing the
        // rooms from all the available data sources.
    }

    @Override
    public void deleteAllRooms() {
        mDatabaseHelper.delete(RoomEntry.TABLE_NAME, null);
    }

    @Override
    public void deleteRoom(@NonNull String roomId) {
        String selection = RoomEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {roomId};
        mDatabaseHelper.delete(RoomEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public void saveRoom(List<Room> rooms) {
        for (Room room : rooms) {
            this.saveRoom(room);
        }
    }
}
