/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.common.base.Optional;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import java.util.List;

import fr.nevax.grimmo.data.Picture;
import fr.nevax.grimmo.data.source.PicturesDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static fr.nevax.grimmo.data.source.local.PersistenceContract.PictureEntry;


/**
 * Concrete implementation of a data source as a db.
 */
public class PicturesLocalDataSource implements PicturesDataSource {

    @Nullable
    private static PicturesLocalDataSource INSTANCE;

    @NonNull
    private final BriteDatabase mDatabaseHelper;

    @NonNull
    private Function<Cursor, Picture> mPictureMapperFunction;

    // Prevent direct instantiation.
    private PicturesLocalDataSource(@NonNull Context context,
                                    @NonNull BaseSchedulerProvider schedulerProvider) {
        checkNotNull(context, "context cannot be null");
        checkNotNull(schedulerProvider, "scheduleProvider cannot be null");
        BaseDbHelper dbHelper = new BaseDbHelper(context);
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, schedulerProvider.io());
        mPictureMapperFunction = this::getPicture;
    }

    public static PicturesLocalDataSource getInstance(
            @NonNull Context context,
            @NonNull BaseSchedulerProvider schedulerProvider) {
        if (INSTANCE == null) {
            INSTANCE = new PicturesLocalDataSource(context, schedulerProvider);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @NonNull
    private Picture getPicture(@NonNull Cursor c) {
        String itemId = c.getString(c.getColumnIndexOrThrow(PictureEntry.COLUMN_NAME_ENTRY_ID));
        String title = c.getString(c.getColumnIndexOrThrow(PictureEntry.COLUMN_NAME_TITLE));
        String description = c.getString(c.getColumnIndexOrThrow(PictureEntry.COLUMN_NAME_DESCRIPTION));
        String filename = c.getString(c.getColumnIndexOrThrow(PictureEntry.COLUMN_NAME_FILENAME));
        String propertyId = c.getString(c.getColumnIndexOrThrow(PictureEntry.COLUMN_NAME_PROPERTY_ID));

        //TODO Get property with id
        return new Picture(itemId, title, description, filename);
    }

    @Override
    public Flowable<List<Picture>> getPictures() {
        String[] projection = {
                PictureEntry.COLUMN_NAME_ENTRY_ID,
                PictureEntry.COLUMN_NAME_TITLE,
                PictureEntry.COLUMN_NAME_DESCRIPTION,
                PictureEntry.COLUMN_NAME_FILENAME,
                PictureEntry.COLUMN_NAME_PROPERTY_ID
        };
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), PictureEntry.TABLE_NAME);
        return mDatabaseHelper.createQuery(PictureEntry.TABLE_NAME, sql)
                .mapToList(mPictureMapperFunction)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public Flowable<Optional<Picture>> getPicture(@NonNull String pictureId) {
        String[] projection = {
                PictureEntry.COLUMN_NAME_ENTRY_ID,
                PictureEntry.COLUMN_NAME_TITLE,
                PictureEntry.COLUMN_NAME_DESCRIPTION,
                PictureEntry.COLUMN_NAME_FILENAME,
                PictureEntry.COLUMN_NAME_PROPERTY_ID
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), PictureEntry.TABLE_NAME, PictureEntry.COLUMN_NAME_ENTRY_ID);
        return mDatabaseHelper.createQuery(PictureEntry.TABLE_NAME, sql, pictureId)
                .mapToOneOrDefault(cursor -> Optional.of(mPictureMapperFunction.apply(cursor)), Optional.<Picture>absent())
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public void savePicture(@NonNull Picture picture) {
        checkNotNull(picture);
        ContentValues values = new ContentValues();
        values.put(PictureEntry.COLUMN_NAME_ENTRY_ID, picture.getId());
        values.put(PictureEntry.COLUMN_NAME_TITLE, picture.getDescription());
        values.put(PictureEntry.COLUMN_NAME_DESCRIPTION, picture.getDescription());
        values.put(PictureEntry.COLUMN_NAME_FILENAME, picture.getImageFile().getName());
        values.put(PictureEntry.COLUMN_NAME_PROPERTY_ID, picture.getProperty().getId());

        mDatabaseHelper.insert(PictureEntry.TABLE_NAME, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void refreshPictures() {
        // Not required because the {@link PicturesRepository} handles the logic of refreshing the
        // properties from all the available data sources.
    }

    @Override
    public void deleteAllPictures() {
        mDatabaseHelper.delete(PictureEntry.TABLE_NAME, null);
    }

    @Override
    public void deletePicture(@NonNull String pictureId) {
        String selection = PictureEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {pictureId};
        mDatabaseHelper.delete(PictureEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public void savePicture(List<Picture> pictures) {
        for (Picture picture : pictures) {
            this.savePicture(picture);
        }
    }
}
