/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.data.source.remote;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.PropertyType;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of the data source that adds a latency simulating network.
 */
public class PropertiesRemoteDataSource implements PropertiesDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 5000;
    private final static Map<String, Property> PROPERTIES_SERVICE_DATA;
    private static PropertiesRemoteDataSource INSTANCE;

    static {
        PROPERTIES_SERVICE_DATA = new LinkedHashMap<>(3);
        addProperty("Maison sur la mer", "Grande villa avec vue sur la mer", 1000000f, 43.544856, 7.032079, PropertyType.HOUSE);
        addProperty("Garage en centre ville", "Petit garage en plein centre de ville de Paris", 5000.52f, 48.854371, 2.359732, PropertyType.GARAGE);
        addProperty("Maison sur Valence", "Proche des écoles", 50000.52f, 44.9227286, 4.9087235, PropertyType.HOUSE);
    }

    // Prevent direct instantiation.
    private PropertiesRemoteDataSource() {
    }

    public static PropertiesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PropertiesRemoteDataSource();
        }
        return INSTANCE;
    }

    private static void addProperty(String title, String description, Float price, Double latitude, Double longitude, PropertyType propertyType) {
        Property newProperty = new Property(title, description, price);
        newProperty.setLatitude(latitude);
        newProperty.setLongitude(longitude);
        newProperty.setPropertyType(propertyType);
        PROPERTIES_SERVICE_DATA.put(newProperty.getId(), newProperty);
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        return Flowable
                .fromIterable(PROPERTIES_SERVICE_DATA.values())
                .delay(SERVICE_LATENCY_IN_MILLIS, TimeUnit.MILLISECONDS)
                .toList()
                .toFlowable();
    }

    @Override
    public Flowable<Optional<Property>> getProperty(@NonNull String propertyId) {
        final Property property = PROPERTIES_SERVICE_DATA.get(propertyId);
        if (property != null) {
            return Flowable.just(Optional.of(property)).delay(SERVICE_LATENCY_IN_MILLIS, TimeUnit.MILLISECONDS);
        } else {
            return Flowable.empty();
        }
    }

    @Override
    public void saveProperty(@NonNull Property property) {
        PROPERTIES_SERVICE_DATA.put(property.getId(), property);
    }

    @Override
    public void completeProperty(@NonNull Property property) {
        Property completedProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId(), true);
        PROPERTIES_SERVICE_DATA.put(property.getId(), completedProperty);
    }

    @Override
    public void completeProperty(@NonNull String propertyId) {
        // Not required for the remote data source because the {@link PropertiesRepository} handles
        // converting from a {@code propertyId} to a {@link property} using its cached data.
    }

    @Override
    public void activateProperty(@NonNull Property property) {
        Property activeProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId());
        PROPERTIES_SERVICE_DATA.put(property.getId(), activeProperty);
    }

    @Override
    public void activateProperty(@NonNull String propertyId) {
        // Not required for the remote data source because the {@link PropertiesRepository} handles
        // converting from a {@code propertyId} to a {@link property} using its cached data.
    }

    @Override
    public void clearCompletedProperties() {
        Iterator<Map.Entry<String, Property>> it = PROPERTIES_SERVICE_DATA.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Property> entry = it.next();
            if (entry.getValue().isCompleted()) {
                it.remove();
            }
        }
    }

    @Override
    public void refreshProperties() {
        // Not required because the {@link PropertiesRepository} handles the logic of refreshing the
        // properties from all the available data sources.
    }

    @Override
    public void deleteAllProperties() {
        PROPERTIES_SERVICE_DATA.clear();
    }

    @Override
    public void deleteProperty(@NonNull String propertyId) {
        PROPERTIES_SERVICE_DATA.remove(propertyId);
    }
}
