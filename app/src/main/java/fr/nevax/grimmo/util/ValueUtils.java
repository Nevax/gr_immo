/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class ValueUtils {

    public static final String DECIMAL_FORMAT = "###,###.#";

    public static Integer parseIntegerOrZero(String integerString) {
        try {
            return Integer.parseInt(integerString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static Float parseFloatOrZero(String floatString) {
        try {
            return Float.parseFloat(floatString);
        } catch (NumberFormatException e) {
            return 0f;
        }
    }

    public static Double parseDoubleOrZero(String doubleString) {
        try {
            return Double.parseDouble(doubleString);
        } catch (NumberFormatException e) {
            return 0d;
        }
    }

    public static String formatPriceValue(Number value, String formatString) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        formatSymbols.setDecimalSeparator('.');
        formatSymbols.setGroupingSeparator(' ');
        DecimalFormat formatter = new DecimalFormat(formatString, formatSymbols);
        return formatter.format(value);
    }
}
