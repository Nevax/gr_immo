/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:29
 */

package fr.nevax.grimmo;

import android.content.Context;
import android.support.annotation.NonNull;

import fr.nevax.grimmo.data.source.PicturesRepository;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.data.source.RoomsRepository;
import fr.nevax.grimmo.data.source.TasksDataSource;
import fr.nevax.grimmo.data.source.TasksRepository;
import fr.nevax.grimmo.data.source.local.PicturesLocalDataSource;
import fr.nevax.grimmo.data.source.local.PropertiesLocalDataSource;
import fr.nevax.grimmo.data.source.local.RoomsLocalDataSource;
import fr.nevax.grimmo.data.source.local.TasksLocalDataSource;
import fr.nevax.grimmo.data.source.remote.PicturesRemoteDataSource;
import fr.nevax.grimmo.data.source.remote.PropertiesRemoteDataSource;
import fr.nevax.grimmo.data.source.remote.RoomsRemoteDataSource;
import fr.nevax.grimmo.data.source.remote.TasksRemoteDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.SchedulerProvider;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Enables injection of production implementations for
 * {@link TasksDataSource} and {@link PropertiesDataSource} at compile time.
 */
public class Injection {

    public static TasksRepository provideTasksRepository(@NonNull Context context) {
        checkNotNull(context);
        return TasksRepository.getInstance(TasksRemoteDataSource.getInstance(),
                TasksLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static PropertiesRepository providePropertiesRepository(@NonNull Context context) {
        checkNotNull(context);
        return PropertiesRepository.getInstance(PropertiesRemoteDataSource.getInstance(),
                PropertiesLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static RoomsRepository provideRoomsRepository(@NonNull Context context) {
        checkNotNull(context);
        return RoomsRepository.getInstance(RoomsRemoteDataSource.getInstance(),
                RoomsLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static PicturesRepository providePicturesRepository(@NonNull Context context) {
        checkNotNull(context);
        return PicturesRepository.getInstance(PicturesRemoteDataSource.getInstance(),
                PicturesLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static BaseSchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }
}
