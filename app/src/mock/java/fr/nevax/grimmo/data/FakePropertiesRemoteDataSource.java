/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.source.PropertiesDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakePropertiesRemoteDataSource implements PropertiesDataSource {

    private static final Map<String, Property> TASKS_SERVICE_DATA = new LinkedHashMap<>();
    private static FakePropertiesRemoteDataSource INSTANCE;

    // Prevent direct instantiation.
    private FakePropertiesRemoteDataSource() {
    }

    public static FakePropertiesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakePropertiesRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        Collection<Property> values = TASKS_SERVICE_DATA.values();
        return Flowable.fromIterable(values).toList().toFlowable();
    }

    @Override
    public Flowable<Optional<Property>> getProperty(@NonNull String propertyId) {
        Property property = TASKS_SERVICE_DATA.get(propertyId);
        return Flowable.just(Optional.of(property));
    }

    @Override
    public void saveProperty(@NonNull Property property) {
        TASKS_SERVICE_DATA.put(property.getId(), property);
    }

    @Override
    public void completeProperty(@NonNull Property property) {
        Property completedProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId(), true);
        TASKS_SERVICE_DATA.put(property.getId(), completedProperty);
    }

    @Override
    public void completeProperty(@NonNull String propertyId) {
        Property property = TASKS_SERVICE_DATA.get(propertyId);
        Property completedProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId(), true);
        TASKS_SERVICE_DATA.put(propertyId, completedProperty);
    }

    @Override
    public void activateProperty(@NonNull Property property) {
        Property activeProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId());
        TASKS_SERVICE_DATA.put(property.getId(), activeProperty);
    }

    @Override
    public void activateProperty(@NonNull String propertyId) {
        Property property = TASKS_SERVICE_DATA.get(propertyId);
        Property activeProperty = new Property(property.getTitle(), property.getDescription(), property.getPrice(), property.getId());
        TASKS_SERVICE_DATA.put(propertyId, activeProperty);
    }

    @Override
    public void clearCompletedProperties() {
        Iterator<Map.Entry<String, Property>> it = TASKS_SERVICE_DATA.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Property> entry = it.next();
            if (entry.getValue().isCompleted()) {
                it.remove();
            }
        }
    }

    public void refreshProperties() {
        // Not required because the {@link PropertiesRepository} handles the logic of refreshing the
        // properties from all the available data sources.
    }

    @Override
    public void deleteProperty(@NonNull String propertyId) {
        TASKS_SERVICE_DATA.remove(propertyId);
    }

    @Override
    public void deleteAllProperties() {
        TASKS_SERVICE_DATA.clear();
    }

    @VisibleForTesting
    public void addProperties(Property... properties) {
        for (Property property : properties) {
            TASKS_SERVICE_DATA.put(property.getId(), property);
        }
    }
}
