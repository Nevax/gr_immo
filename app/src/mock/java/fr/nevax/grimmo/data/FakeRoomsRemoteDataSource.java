/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.source.RoomsDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakeRoomsRemoteDataSource implements RoomsDataSource {

    private static final Map<String, Room> TASKS_SERVICE_DATA = new LinkedHashMap<>();
    private static FakeRoomsRemoteDataSource INSTANCE;

    // Prevent direct instantiation.
    private FakeRoomsRemoteDataSource() {
    }

    public static FakeRoomsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeRoomsRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<Room>> getRooms() {
        Collection<Room> values = TASKS_SERVICE_DATA.values();
        return Flowable.fromIterable(values).toList().toFlowable();
    }

    @Override
    public Flowable<Optional<Room>> getRoom(@NonNull String roomId) {
        Room room = TASKS_SERVICE_DATA.get(roomId);
        return Flowable.just(Optional.of(room));
    }

    @Override
    public void saveRoom(@NonNull Room room) {
        TASKS_SERVICE_DATA.put(room.getId(), room);
    }

    public void refreshRooms() {
        // Not required because the {@link RoomsRepository} handles the logic of refreshing the
        // rooms from all the available data sources.
    }

    @Override
    public void deleteRoom(@NonNull String roomId) {
        TASKS_SERVICE_DATA.remove(roomId);
    }

    @Override
    public void saveRoom(List<Room> rooms) {
        Room[] roomsArray = rooms.toArray(new Room[rooms.size()]);
        for (Room room : roomsArray) {
            this.saveRoom(room);
        }
    }

    @Override
    public void deleteAllRooms() {
        TASKS_SERVICE_DATA.clear();
    }

    @VisibleForTesting
    public void addRooms(Room... rooms) {
        for (Room room : rooms) {
            TASKS_SERVICE_DATA.put(room.getId(), room);
        }
    }
}
