/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.nevax.grimmo.data;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Optional;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.nevax.grimmo.data.source.PicturesDataSource;
import io.reactivex.Flowable;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakePicturesRemoteDataSource implements PicturesDataSource {

    private static final Map<String, Picture> TASKS_SERVICE_DATA = new LinkedHashMap<>();
    private static FakePicturesRemoteDataSource INSTANCE;

    // Prevent direct instantiation.
    private FakePicturesRemoteDataSource() {
    }

    public static FakePicturesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakePicturesRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<Picture>> getPictures() {
        Collection<Picture> values = TASKS_SERVICE_DATA.values();
        return Flowable.fromIterable(values).toList().toFlowable();
    }

    @Override
    public Flowable<Optional<Picture>> getPicture(@NonNull String propertyId) {
        Picture property = TASKS_SERVICE_DATA.get(propertyId);
        return Flowable.just(Optional.of(property));
    }

    @Override
    public void savePicture(@NonNull Picture property) {
        TASKS_SERVICE_DATA.put(property.getId(), property);
    }

    public void refreshPictures() {
        // Not required because the {@link PicturesRepository} handles the logic of refreshing the
        // pictures from all the available data sources.
    }

    @Override
    public void deletePicture(@NonNull String propertyId) {
        TASKS_SERVICE_DATA.remove(propertyId);
    }

    @Override
    public void savePicture(List<Picture> pictures) {
        for (Picture picture : pictures) {
            this.savePicture(picture);
        }
    }

    @Override
    public void deleteAllPictures() {
        TASKS_SERVICE_DATA.clear();
    }

    @VisibleForTesting
    public void addPictures(Picture... pictures) {
        for (Picture property : pictures) {
            TASKS_SERVICE_DATA.put(property.getId(), property);
        }
    }
}
