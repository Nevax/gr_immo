package fr.nevax.grimmo;

import android.content.Context;
import android.support.annotation.NonNull;

import fr.nevax.grimmo.data.FakePicturesRemoteDataSource;
import fr.nevax.grimmo.data.FakePropertiesRemoteDataSource;
import fr.nevax.grimmo.data.FakeRoomsRemoteDataSource;
import fr.nevax.grimmo.data.FakeTasksRemoteDataSource;
import fr.nevax.grimmo.data.source.PicturesRepository;
import fr.nevax.grimmo.data.source.PropertiesRepository;
import fr.nevax.grimmo.data.source.RoomsRepository;
import fr.nevax.grimmo.data.source.TasksDataSource;
import fr.nevax.grimmo.data.source.TasksRepository;
import fr.nevax.grimmo.data.source.local.PicturesLocalDataSource;
import fr.nevax.grimmo.data.source.local.PropertiesLocalDataSource;
import fr.nevax.grimmo.data.source.local.RoomsLocalDataSource;
import fr.nevax.grimmo.data.source.local.TasksLocalDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.SchedulerProvider;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Enables injection of mock implementations for
 * {@link TasksDataSource} at compile time. This is useful for testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

    public static TasksRepository provideTasksRepository(@NonNull Context context) {
        checkNotNull(context);
        return TasksRepository.getInstance(FakeTasksRemoteDataSource.getInstance(),
                TasksLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static PropertiesRepository providePropertiesRepository(@NonNull Context context) {
        checkNotNull(context);
        return PropertiesRepository.getInstance(FakePropertiesRemoteDataSource.getInstance(),
                PropertiesLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static RoomsRepository provideRoomsRepository(@NonNull Context context) {
        checkNotNull(context);
        return RoomsRepository.getInstance(FakeRoomsRemoteDataSource.getInstance(),
                RoomsLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static PicturesRepository providePicturesRepository(@NonNull Context context) {
        checkNotNull(context);
        return PicturesRepository.getInstance(FakePicturesRemoteDataSource.getInstance(),
                PicturesLocalDataSource.getInstance(context, provideSchedulerProvider()));
    }

    public static BaseSchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }
}
