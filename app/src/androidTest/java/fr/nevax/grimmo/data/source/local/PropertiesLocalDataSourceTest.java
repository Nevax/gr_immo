/*
 * Created by Antoine GRAVELOT on 16/02/18 16:05
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:31
 */

package fr.nevax.grimmo.data.source.local;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.google.common.base.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import fr.nevax.grimmo.data.Property;
import fr.nevax.grimmo.data.source.PropertiesDataSource;
import fr.nevax.grimmo.util.schedulers.BaseSchedulerProvider;
import fr.nevax.grimmo.util.schedulers.ImmediateSchedulerProvider;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Integration test for the {@link PropertiesDataSource}, which uses the {@link BaseDbHelper}.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class PropertiesLocalDataSourceTest {

    private final static String TITLE = "title";

    private final static String TITLE2 = "title2";

    private final static String TITLE3 = "title3";

    private BaseSchedulerProvider mSchedulerProvider;

    private PropertiesLocalDataSource mLocalDataSource;

    @Before
    public void setup() {
        PropertiesLocalDataSource.destroyInstance();
        mSchedulerProvider = new ImmediateSchedulerProvider();

        mLocalDataSource = PropertiesLocalDataSource.getInstance(
                InstrumentationRegistry.getTargetContext(), mSchedulerProvider);
    }

    @After
    public void cleanUp() {
        mLocalDataSource.deleteAllProperties();
    }

    @Test
    public void testPreConditions() {
        assertNotNull(mLocalDataSource);
    }

    @Test
    public void saveProperty_retrievesProperty() {
        // Given a new property
        final Property newProperty = new Property(TITLE, "", 10f);
        final Optional<Property> newPropertyOptional = Optional.of(newProperty);

        // When saved into the persistent repository
        mLocalDataSource.saveProperty(newProperty);

        // Then the property can be retrieved from the persistent repository
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperty(newProperty.getId()).subscribe(testSubscriber);
        testSubscriber.assertValue(newPropertyOptional);
    }

    @Test
    public void completeProperty_retrievedPropertyIsComplete() {
        // Given a new property in the persistent repository
        final Property newProperty = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty);

        // When completed in the persistent repository
        mLocalDataSource.completeProperty(newProperty);

        // Then the property can be retrieved from the persistent repository and is complete
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperty(newProperty.getId()).subscribe(testSubscriber);
        testSubscriber.assertValueCount(1);
        Property result = testSubscriber.values().get(0).get();
        assertThat(result.isCompleted(), is(true));
    }

    @Test
    public void activateProperty_retrievedPropertyIsActive() {
        // Given a new completed property in the persistent repository
        final Property newProperty = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty);
        mLocalDataSource.completeProperty(newProperty);

        // When activated in the persistent repository
        mLocalDataSource.activateProperty(newProperty);

        // Then the property can be retrieved from the persistent repository and is active
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperty(newProperty.getId()).subscribe(testSubscriber);
        testSubscriber.assertValueCount(1);
        Property result = testSubscriber.values().get(0).get();
        assertThat(result.isActive(), is(true));
        assertThat(result.isCompleted(), is(false));
    }

    @Test
    public void clearCompletedProperty_propertyNotRetrievable() {
        // Given 2 new completed properties and 1 active property in the persistent repository
        final Property newProperty1 = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty1);
        mLocalDataSource.completeProperty(newProperty1);
        final Property newProperty2 = new Property(TITLE2, "", 10f);
        mLocalDataSource.saveProperty(newProperty2);
        mLocalDataSource.completeProperty(newProperty2);
        final Property newProperty3 = new Property(TITLE3, "", 10f);
        mLocalDataSource.saveProperty(newProperty3);

        // When completed properties are cleared in the repository
        mLocalDataSource.clearCompletedProperties();

        // Then the completed properties cannot be retrieved and the active one can
        TestSubscriber<List<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperties().subscribe(testSubscriber);
        List<Property> result = testSubscriber.values().get(0);
        assertThat(result, not(hasItems(newProperty1, newProperty2)));
    }

    @Test
    public void deleteAllProperties_emptyListOfRetrievedProperty() {
        // Given a new property in the persistent repository and a mocked callback
        Property newProperty = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty);

        // When all properties are deleted
        mLocalDataSource.deleteAllProperties();

        // Then the retrieved properties is an empty list
        TestSubscriber<List<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperties().subscribe(testSubscriber);
        List<Property> result = testSubscriber.values().get(0);
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    public void getProperties_retrieveSavedProperties() {
        // Given 2 new properties in the persistent repository
        final Property newProperty1 = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty1);
        final Property newProperty2 = new Property(TITLE, "", 10f);
        mLocalDataSource.saveProperty(newProperty2);

        // Then the properties can be retrieved from the persistent repository
        TestSubscriber<List<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperties().subscribe(testSubscriber);
        List<Property> result = testSubscriber.values().get(0);
        assertThat(result, hasItems(newProperty1, newProperty2));
    }

    @Test
    public void getProperty_whenPropertyNotSaved() {
        //Given that no property has been saved
        //When querying for a property, null is returned.
        TestSubscriber<Optional<Property>> testSubscriber = new TestSubscriber<>();
        mLocalDataSource.getProperty("1").subscribe(testSubscriber);
        testSubscriber.assertValue(Optional.absent());
    }
}
