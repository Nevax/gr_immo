/*
 * Created by Antoine GRAVELOT on 15/02/18 23:31
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:30
 */

package fr.nevax.grimmo.property.properties;

import android.support.test.espresso.NoActivityResumedException;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.widget.DrawerLayout;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.Gravity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.nevax.grimmo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.DrawerMatchers.isOpen;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static fr.nevax.grimmo.TestUtils.getToolbarNavigationContentDescription;
import static fr.nevax.grimmo.custom.action.NavigationViewActions.navigateTo;
import static junit.framework.Assert.fail;

/**
 * Tests for the {@link DrawerLayout} layout component in {@link PropertiesActivity} which manages
 * navigation within the app.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class AppNavigationTest {

    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     * <p>
     * <p>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public ActivityTestRule<PropertiesActivity> mActivityTestRule =
            new ActivityTestRule<>(PropertiesActivity.class);

    @Test
    public void clickOnStatisticsNavigationItem_ShowsStatisticsScreen() {
        openStatisticsScreen();

        // Check that statistics Activity was opened.
        onView(withId(R.id.property_statistics)).check(matches(isDisplayed()));
    }

    @Test
    public void clickOnListNavigationItem_ShowsListScreenFromDrawer() {
        openStatisticsScreen();

        // Check that Properties Activity was opened.
        onView(withId(R.id.propertiesContainer)).check(doesNotExist());
    }

    @Test
    public void clickOnListNavigationItem_ShowsListScreenWithBack() {
        openStatisticsScreen();

        pressBack();

        // Check that Properties Activity was opened.
        onView(withId(R.id.propertiesContainer)).check(matches(isDisplayed()));
    }

    @Test
    public void clickOnAndroidHomeIcon_OpensNavigation() {
        // Check that left drawer is closed at startup
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))); // Left Drawer should be closed.

        // Open Drawer
        onView(withContentDescription(getToolbarNavigationContentDescription(
                mActivityTestRule.getActivity(), R.id.toolbar))).perform(click());

        // Check if drawer is open
        onView(withId(R.id.drawer_layout))
                .check(matches(isOpen(Gravity.LEFT))); // Left drawer is open open.
    }

    @Test
    public void Statistics_backNavigatesToProperties() {
        openStatisticsScreen();

        // Press back to go back to the properties list
        pressBack();

        // Check that Properties Activity was restored.
        onView(withId(R.id.propertiesContainer)).check(matches(isDisplayed()));
    }

    @Test
    public void backFromPropertiesScreen_ExitsApp() {
        // From the properties screen, press back should exit the app.
        assertPressingBackExitsApp();
    }

    @Test
    public void backFromPropertiesScreenAfterStats_ExitsApp() {
        // This test checks that PropertiesActivity is a parent of StatisticsActivity

        // Open the stats screen
        openStatisticsScreen();

        // Return to the properties screen
        pressBack();

        // Pressing back should exit app
        assertPressingBackExitsApp();
    }

    private void assertPressingBackExitsApp() {
        try {
            pressBack();
            fail("Should kill the app and throw an exception");
        } catch (NoActivityResumedException e) {
            // Test OK
        }
    }

    private void openPropertiesScreen() {
        // Open Drawer to click on navigation item.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left Drawer should be closed.
                .perform(open()); // Open Drawer

        // Start properties list screen.
        onView(withId(R.id.nav_view))
                .perform(navigateTo(R.id.property_navigation_menu_item));
    }

    private void openStatisticsScreen() {
        onView(withId(R.id.menu_statistics)).perform(click());
    }
}
