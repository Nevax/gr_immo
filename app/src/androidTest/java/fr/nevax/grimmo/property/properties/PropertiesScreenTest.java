/*
 * Created by Antoine GRAVELOT on 16/02/18 14:01
 * Copyright (c) 2018. All rights reserved.
 *
 * Last modified 15/02/18 23:31
 */

package fr.nevax.grimmo.property.properties;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.filters.SdkSuppress;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.nevax.grimmo.Injection;
import fr.nevax.grimmo.R;
import fr.nevax.grimmo.TestUtils;
import fr.nevax.grimmo.data.source.PropertiesDataSource;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.google.common.base.Preconditions.checkArgument;
import static fr.nevax.grimmo.TestUtils.getCurrentActivity;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;

/**
 * Tests for the properties screen, the main screen which contains a list of all properties.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class PropertiesScreenTest {

    private final static String TITLE1 = "TITLE1";

    private final static String TITLE2 = "TITLE2";

    private final static String LATITUDE1 = "5.215421";

    private final static String LATITUDE2 = "6.1215";

    private final static String LONGITUDE1 = "6.23212";

    private final static String LONGITUDE2 = "-0.54521";

    private final static String DESCRIPTION1 = "DESCR1";

    private final static String DESCRIPTION2 = "DESCR2";


    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     * <p>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public ActivityTestRule<PropertiesActivity> mPropertiesActivityTestRule =
            new ActivityTestRule<PropertiesActivity>(PropertiesActivity.class) {

                /**
                 * To avoid a long list of properties and the need to scroll through the list to find a
                 * property, we call {@link PropertiesDataSource#deleteAllProperties()} before each test.
                 */
                @Override
                protected void beforeActivityLaunched() {
                    super.beforeActivityLaunched();
                    // Doing this in @Before generates a race condition.
                    Injection.providePropertiesRepository(InstrumentationRegistry.getTargetContext())
                            .deleteAllProperties();
                }
            };

    /**
     * Prepare your test fixture for this test. In this case we register an IdlingResources with
     * Espresso. IdlingResource resource is a great way to tell Espresso when your app is in an
     * idle state. This helps Espresso to synchronize your test actions, which makes tests significantly
     * more reliable.
     */
    @Before
    public void setUp() {
        IdlingRegistry.getInstance().register(
                mPropertiesActivityTestRule.getActivity().getCountingIdlingResource());
    }

    /**
     * Unregister your Idling Resource so it can be garbage collected and does not leak any memory.
     */
    @After
    public void tearDown() {
        IdlingRegistry.getInstance().unregister(
                mPropertiesActivityTestRule.getActivity().getCountingIdlingResource());
    }

    /**
     * A custom {@link Matcher} which matches an item in a {@link ListView} by its text.
     * <p>
     * View constraints:
     * <ul>
     * <li>View must be a child of a {@link ListView}
     * <ul>
     *
     * @param itemText the text to match
     * @return Matcher that matches text in the given view
     */
    private Matcher<View> withItemText(final String itemText) {
        checkArgument(!TextUtils.isEmpty(itemText), "itemText cannot be null or empty");
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View item) {
                return allOf(
                        isDescendantOfA(isAssignableFrom(ListView.class)),
                        withText(itemText)).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is isDescendantOfA LV with text " + itemText);
            }
        };
    }

    @Test
    public void clickAddPropertyButton_opensAddPropertyUi() {
        // Click on the add property button
        onView(withId(R.id.fab_add_property)).perform(click());

        // Check if the add property screen is displayed
        onView(withId(R.id.add_property_title)).check(matches(isDisplayed()));
    }

    @Test
    public void editProperty() {
        // First add a property
        createProperty(TITLE1, DESCRIPTION1);

        // Click on the property on the list
        onView(withText(TITLE1)).perform(click());

        // Click on the edit property button
        onView(withId(R.id.fab_edit_property)).perform(click());

        String editPropertyTitle = TITLE2;
        String editPropertyDescription = "New Description";

        // Edit property title and description
        onView(withId(R.id.add_property_title))
                .perform(replaceText(editPropertyTitle), closeSoftKeyboard()); // Type new property title
        onView(withId(R.id.add_property_description)).perform(replaceText(editPropertyDescription),
                closeSoftKeyboard()); // Type new property description and close the keyboard

        // Save the property
        onView(withId(R.id.fab_edit_property_done)).perform(click());

        // Verify property is displayed on screen in the property list.
        onView(withItemText(editPropertyTitle)).check(matches(isDisplayed()));

        // Verify previous property is not displayed
        onView(withItemText(TITLE1)).check(doesNotExist());
    }

    @Test
    public void addPropertyToPropertiesList() {
        createProperty(TITLE1, DESCRIPTION1);

        // Verify property is displayed on screen
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
    }

    @Test
    public void markPropertyAsComplete() {
        viewAllProperties();

        // Add active property
        createProperty(TITLE1, DESCRIPTION1);

        // Mark the property as complete
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // Verify property is shown as complete
        viewAllProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        viewActiveProperties();
        onView(withItemText(TITLE1)).check(matches(not(isDisplayed())));
        viewCompletedProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
    }

    @Test
    public void markPropertyAsActive() {
        viewAllProperties();

        // Add completed property
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // Mark the property as active
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // Verify property is shown as active
        viewAllProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        viewActiveProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        viewCompletedProperties();
        onView(withItemText(TITLE1)).check(matches(not(isDisplayed())));
    }

    @Test
    public void showAllProperties() {
        // Add 2 active properties
        createProperty(TITLE1, DESCRIPTION1);
        createProperty(TITLE2, DESCRIPTION1);

        //Verify that all our properties are shown
        viewAllProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        onView(withItemText(TITLE2)).check(matches(isDisplayed()));
    }

    @Test
    public void showActiveProperties() {
        // Add 2 active properties
        createProperty(TITLE1, DESCRIPTION1);
        createProperty(TITLE2, DESCRIPTION1);

        //Verify that all our properties are shown
        viewActiveProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        onView(withItemText(TITLE2)).check(matches(isDisplayed()));
    }

    @Test
    public void showCompletedProperties() {
        // Add 2 completed properties
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);
        createProperty(TITLE2, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE2);

        // Verify that all our properties are shown
        viewCompletedProperties();
        onView(withItemText(TITLE1)).check(matches(isDisplayed()));
        onView(withItemText(TITLE2)).check(matches(isDisplayed()));
    }

    @Test
    public void clearCompletedProperties() {
        viewAllProperties();

        // Add 2 complete properties
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);
        createProperty(TITLE2, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE2);

        // Click clear completed in menu
        openActionBarOverflowOrOptionsMenu(getTargetContext());
        onView(withText(R.string.menu_clear)).perform(click());

        //Verify that completed properties are not shown
        onView(withItemText(TITLE1)).check(matches(not(isDisplayed())));
        onView(withItemText(TITLE2)).check(matches(not(isDisplayed())));
    }

    @Test
    public void createOneProperty_deleteProperty() {
        viewAllProperties();

        // Add active property
        createProperty(TITLE1, DESCRIPTION1);

        // Open it in details view
        onView(withText(TITLE1)).perform(click());

        // Click delete property in menu
        onView(withId(R.id.menu_delete)).perform(click());

        // Verify it was deleted
        viewAllProperties();
        onView(withText(TITLE1)).check(matches(not(isDisplayed())));
    }

    @Test
    public void createTwoProperties_deleteOneProperty() {
        // Add 2 active properties
        createProperty(TITLE1, DESCRIPTION1);
        createProperty(TITLE2, DESCRIPTION1);

        // Open the second property in details view
        onView(withText(TITLE2)).perform(click());

        // Click delete property in menu
        onView(withId(R.id.menu_delete)).perform(click());

        // Verify only one property was deleted
        viewAllProperties();
        onView(withText(TITLE1)).check(matches(isDisplayed()));
        onView(withText(TITLE2)).check(doesNotExist());
    }

    @Test
    public void markPropertyAsCompleteOnDetailScreen_propertyIsCompleteInList() {
        viewAllProperties();

        // Add 1 active property
        createProperty(TITLE1, DESCRIPTION1);

        // Click on the property on the list
        onView(withText(TITLE1)).perform(click());

        // Click on the checkbox in property details screen
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click on the navigation up button to go back to the list
        onView(withContentDescription(getToolbarNavigationContentDescription())).perform(click());

        // Check that the property is marked as completed
        onView(allOf(withId(R.id.complete),
                hasSibling(withText(TITLE1)))).check(matches(isChecked()));
    }

    @Test
    public void markPropertyAsActiveOnDetailScreen_propertyIsActiveInList() {
        viewAllProperties();

        // Add 1 completed property
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // Click on the property on the list
        onView(withText(TITLE1)).perform(click());

        // Click on the checkbox in property details screen
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click on the navigation up button to go back to the list
        onView(withContentDescription(getToolbarNavigationContentDescription())).perform(click());

        // Check that the property is marked as active
        onView(allOf(withId(R.id.complete),
                hasSibling(withText(TITLE1)))).check(matches(not(isChecked())));
    }

    @Test
    public void markPropertyAsAcompleteAndActiveOnDetailScreen_propertyIsActiveInList() {
        viewAllProperties();

        // Add 1 active property
        createProperty(TITLE1, DESCRIPTION1);

        // Click on the property on the list
        onView(withText(TITLE1)).perform(click());

        // Click on the checkbox in property details screen
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click again to restore it to original state
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click on the navigation up button to go back to the list
        onView(withContentDescription(getToolbarNavigationContentDescription())).perform(click());

        // Check that the property is marked as active
        onView(allOf(withId(R.id.complete),
                hasSibling(withText(TITLE1)))).check(matches(not(isChecked())));
    }

    @Test
    public void markPropertyAsActiveAndCompleteOnDetailScreen_propertyIsCompleteInList() {
        viewAllProperties();

        // Add 1 completed property
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // Click on the property on the list
        onView(withText(TITLE1)).perform(click());

        // Click on the checkbox in property details screen
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click again to restore it to original state
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click on the navigation up button to go back to the list
        onView(withContentDescription(getToolbarNavigationContentDescription())).perform(click());

        // Check that the property is marked as active
        onView(allOf(withId(R.id.complete),
                hasSibling(withText(TITLE1)))).check(matches(isChecked()));
    }

    @Test
    public void showPropertyOnMap_WithEmptyLatitudeAndLongitude() {

        // Add a property without latitude and longitude
        createProperty(TITLE1, DESCRIPTION1);

        // open the property in details view
        onView(withText(TITLE1)).perform(click());

        // when open map on property coordinates
        onView(withId(R.id.map_fab)).perform(click());

        // then show show a error message
        onView(withText(R.string.property_coordinate_undefined)).check(matches(isDisplayed()));
    }


    @Test
    public void orientationChange_FilterActivePersists() {

        // Add a completed property
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // when switching to active properties
        viewActiveProperties();

        // then no properties should appear
        onView(withText(TITLE1)).check(matches(not(isDisplayed())));

        // when rotating the screen
        TestUtils.rotateOrientation(mPropertiesActivityTestRule.getActivity());

        // then nothing changes
        onView(withText(TITLE1)).check(doesNotExist());
    }

    @Test
    public void orientationChange_FilterCompletedPersists() {

        // Add a completed property
        createProperty(TITLE1, DESCRIPTION1);
        clickCheckBoxInPropertyDetailForProperty(TITLE1);

        // when switching to completed properties
        viewCompletedProperties();

        // the completed property should be displayed
        onView(withText(TITLE1)).check(matches(isDisplayed()));

        // when rotating the screen
        TestUtils.rotateOrientation(mPropertiesActivityTestRule.getActivity());

        // then nothing changes
        onView(withText(TITLE1)).check(matches(isDisplayed()));
        onView(withText(R.string.label_completed_properties)).check(matches(isDisplayed()));
    }

    @Test
    @SdkSuppress(minSdkVersion = 21) // Blinking cursor after rotation breaks this in API 19
    public void orientationChange_DuringEdit_Title_ChangePersists() {
        // Add a completed property
        createProperty(TITLE1, DESCRIPTION1);

        // Open the property in details view
        onView(withText(TITLE1)).perform(click());

        // Click on the edit property button
        onView(withId(R.id.fab_edit_property)).perform(click());

        // Change property title (but don't save)
        onView(withId(R.id.add_property_title))
                .perform(replaceText(TITLE2), closeSoftKeyboard()); // Type new property title

        // Rotate the screen
        TestUtils.rotateOrientation(getCurrentActivity());

        // Verify property title is restored
        onView(withId(R.id.add_property_title)).check(matches(withText(TITLE2)));
    }


    @Test
    @SdkSuppress(minSdkVersion = 21) // Blinking cursor after rotation breaks this in API 19
    public void orientationChange_DuringEdit_Description_ChangePersists() {
        // Add a completed property
        createProperty(TITLE1, DESCRIPTION1);

        // Open the property in details view
        onView(withText(TITLE1)).perform(click());

        // Click on the edit property button
        onView(withId(R.id.fab_edit_property)).perform(click());

        // Change property description (but don't save)
        onView(withId(R.id.add_property_description))
                .perform(replaceText(DESCRIPTION2), closeSoftKeyboard()); // Type new property title

        // Rotate the screen
        TestUtils.rotateOrientation(getCurrentActivity());

        // Verify property title is restored
        onView(withId(R.id.add_property_description)).check(matches(withText(DESCRIPTION2)));
    }

    @Test
    @SdkSuppress(minSdkVersion = 21) // Blinking cursor after rotation breaks this in API 19
    public void orientationChange_DuringEdit_NoDuplicate() throws IllegalStateException {
        // Add a completed property
        createProperty(TITLE1, DESCRIPTION1);

        // Open the property in details view
        onView(withText(TITLE1)).perform(click());

        // Click on the edit property button
        onView(withId(R.id.fab_edit_property)).perform(click());

        // Rotate the screen
        TestUtils.rotateOrientation(getCurrentActivity());

        // Edit property title and description
        onView(withId(R.id.add_property_title))
                .perform(replaceText(TITLE2), closeSoftKeyboard()); // Type new property title
        onView(withId(R.id.add_property_description)).perform(replaceText(DESCRIPTION1),
                closeSoftKeyboard()); // Type new property description and close the keyboard

        // Save the property
        onView(withId(R.id.fab_edit_property_done)).perform(click());

        // Verify property is displayed on screen in the property list.
        onView(withItemText(TITLE2)).check(matches(isDisplayed()));

        // Verify previous property is not displayed
        onView(withItemText(TITLE1)).check(doesNotExist());
    }

    private void viewAllProperties() {
        onView(withId(R.id.menu_filter)).perform(click());
        onView(withText(R.string.nav_all)).perform(click());
    }

    private void viewActiveProperties() {
        onView(withId(R.id.menu_filter)).perform(click());
        onView(withText(R.string.nav_active)).perform(click());
    }

    private void viewCompletedProperties() {
        onView(withId(R.id.menu_filter)).perform(click());
        onView(withText(R.string.nav_completed)).perform(click());
    }

    private void createProperty(String title, String description) {
        createProperty(title, "", "", "", "", "", description);
    }


    private void createProperty(String title,
                                String price,
                                String size,
                                String propertyType,
                                String latitude,
                                String longitude,
                                String description) {
        // Click on the add property button
        onView(withId(R.id.fab_add_property)).perform(click());

        // Add property title and description
        onView(withId(R.id.add_property_title)).perform(typeText(title),
                closeSoftKeyboard()); // Type new property title

        onView(withId(R.id.add_property_price)).perform(typeText(price),
                closeSoftKeyboard()); // Type new property price and close the keyboard

        onView(withId(R.id.add_property_size)).perform(typeText(size),
                closeSoftKeyboard()); // Type new property size and close the keyboard

//        //TODO propertyType
//
//
//        onView(withId(R.id.add_property_latitude)).perform(typeText(latitude),
//                closeSoftKeyboard()); // Type new property latitude and close the keyboard
//
//        onView(withId(R.id.add_property_longitude)).perform(typeText(longitude),
//                closeSoftKeyboard()); // Type new property longitude and close the keyboard


        onView(withId(R.id.add_property_description)).perform(typeText(description),
                closeSoftKeyboard()); // Type new property description and close the keyboard

        // Save the property
        onView(withId(R.id.fab_edit_property_done)).perform(click());
    }

    private void clickCheckBoxInPropertyDetailForProperty(String title) {
        // Click on the property on the list
        onView(withText(title)).perform(click());

        // Click on the checkbox in property details screen
        onView(withId(R.id.property_detail_complete)).perform(click());

        // Click on the navigation up button to go back to the list
        onView(withContentDescription(getToolbarNavigationContentDescription())).perform(click());
    }

    private String getText(int stringId) {
        return mPropertiesActivityTestRule.getActivity().getResources().getString(stringId);
    }

    private String getToolbarNavigationContentDescription() {
        return TestUtils.getToolbarNavigationContentDescription(
                mPropertiesActivityTestRule.getActivity(), R.id.toolbar);
    }
}
